/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2015 Stefan Hanenberg (stefan.hanenberg@gmail.com)
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this 
 * software and associated documentation files (the "Software"), to deal in the Software 
 * without restriction, including without limitation the rights to use, copy, modify, 
 * merge, publish, distribute, sublicense, and/or sell copies of the Software, and to 
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:

 * The above copyright notice and this permission notice shall be included in all copies or 
 * substantial portions of the Software.

 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
 * INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR 
 * PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE 
 * FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, 
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 **/

package slowscane.lib;

import slowscane.Scanner;
import slowscane.Tokens;
import slowscane.annotations.Separator;
import slowscane.annotations.Token;
import slowscane.scanners.TokenReader;
import slowscane.streams.StringBufferStream;
import slowscane.streams.TokenStream;

/**
 * Defines the scanner for a simple lamda-language, consisting of: ' ', '.',
 * 'λ', <identifier>, '(', (')
 * 
 * Example 1: "λ abc (λx.x)"
 * Example 2: "(λx.x)"
 * 
 * @author Stefan Hanenberg (stefan.hanenberg@gmail.com)
 *
 */
public class Dots01 extends ExamplesTestCase {

	public static class DotsNWhiteSpaces extends Tokens {
		@Separator public TokenReader<?> WHITESPACE() {	return lib.WHITESPACE; 	}
		@Token public TokenReader<?> DOT() { return lib.DOT; }
    }

	public void test01() {
		StringBufferStream stream = new StringBufferStream("...  . . . ....");

		Scanner<DotsNWhiteSpaces, StringBufferStream> 
			scanner = new Scanner<DotsNWhiteSpaces, StringBufferStream>(new DotsNWhiteSpaces(), stream);
		
		TokenStream<?> res = scanner.scan();
		assertEquals(10, res.length());
		
		
	}
	
}
