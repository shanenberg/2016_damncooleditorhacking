/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2015 Stefan Hanenberg (stefan.hanenberg@gmail.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this
 * software and associated documentation files (the "Software"), to deal in the Software
 * without restriction, including without limitation the rights to use, copy, modify,
 * merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:

 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.

 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
 * INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
 * PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
 * FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 **/

package slope.lang.NFOLD;

import slope.ElementParser;
import slope.PC;
import slope.Parser_1;
import slope.TreeNode;
import slowscane.streams.TokenStream;

public class NFOLDParser extends Parser_1<NFOLDTreeNode, PC<?>> {

	public NFOLDParser(PC pc) {
		super("NFOLD", pc);
	}

	@Override
	public NFOLDTreeNode parse(TokenStream<?> tokenStream) {
		boolean withoutError = true;
		NFOLDTreeNode ret = new NFOLDTreeNode();

        while (withoutError) {
			int setBackPoint = tokenStream.currentPosition;
			try {
                ElementParser p = parserConstructor.createParser();
                TreeNode n = p.parse(tokenStream);
				p.addTreeNodeToTreeNodeN(n, ret);
			} catch (Exception ex) { // just a parse Exception....does not matter...
				tokenStream.currentPosition = setBackPoint;
				withoutError = false;
			}
		}
        if (ret.size() == 0) {
            throw this.createParseException(tokenStream, "Error reading NFOLD: ");
		}
		return ret;
	}
	

}
