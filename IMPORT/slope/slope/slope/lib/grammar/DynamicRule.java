package slope.lib.grammar;

import slope.Rule;

public abstract class DynamicRule extends Rule {

    public DynamicRule(String ruleName) {
        super(ruleName);
    }

}
