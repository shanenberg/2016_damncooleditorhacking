package slope.lib.grammar.parsetree;

import slope.lib.grammar.parsetree.visitors.GrammarRuleVisitor;

public class BracketExpression extends UnaryOperator {
    public void printTo(StringBuffer buffer) {
        buffer.append("(");
        expression.printTo(buffer);
        buffer.append(")");
    }

    public <RETURN_TYPE> RETURN_TYPE acceptVisitor(GrammarRuleVisitor<RETURN_TYPE> visitor) {
        return visitor.visit(this);
    }
}
