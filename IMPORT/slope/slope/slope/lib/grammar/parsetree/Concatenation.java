package slope.lib.grammar.parsetree;

import slope.lib.grammar.parsetree.visitors.GrammarRuleVisitor;

public class Concatenation extends NAryExpression {

    @Override
    public void printTo(StringBuffer buffer) {
        for (Expression expression : expressions) {
            expression.printTo(buffer);
            if (!(expressions.get(expressions.size() - 1) == expression)) {
                buffer.append(" ");
            }
        }

    }

    public <RETURN_TYPE> RETURN_TYPE acceptVisitor(GrammarRuleVisitor<RETURN_TYPE> visitor) {
        return visitor.visit(this);
    }

}
