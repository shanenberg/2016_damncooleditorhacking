package slope.lib.grammar.parsetree;

import slope.lib.grammar.DynamicGrammarParser;
import slope.lib.grammar.parsetree.visitors.RegisterTerminalVisitor;

import java.util.ArrayList;

public class Grammar {
    public String name;
    public String startRuleName;
    public ArrayList<Token> tokens = new ArrayList<Token>();
    public ArrayList<GrammarRule> grammarRules = new ArrayList<GrammarRule>();

    public DynamicGrammarParser createParser() {

        DynamicGrammarParser parser = new DynamicGrammarParser(this);

        for (Token token : tokens) {
            if (!(token instanceof TokenSkipped)) {
                parser.addTokenIfNotKnownBack(token.name, token.createTokenReader());
            } else {
                parser.addSkippedToken(token.name, token.createTokenReader());
            }
        }

        for (GrammarRule grammarRule : grammarRules) {
            parser.addRule(grammarRule.ruleName, grammarRule);
            grammarRule.accept(new RegisterTerminalVisitor(parser));
        }

        return parser;
    }

    public Token getTokenNamed(String tokenName) {
        for (Token token : tokens) {
            if (token.name.equals(tokenName))
                return token;
        }
        throw new RuntimeException("Unknown token in Grammar " + name + "..." + tokenName);
    }


}
