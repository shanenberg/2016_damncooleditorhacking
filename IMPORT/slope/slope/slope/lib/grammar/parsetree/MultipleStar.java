package slope.lib.grammar.parsetree;

import slope.lib.grammar.parsetree.visitors.GrammarRuleVisitor;

import java.util.ArrayList;

public class MultipleStar extends UnaryOperator {

    public ArrayList<Expression> values = new ArrayList<Expression>();

    @Override
    public void printTo(StringBuffer buffer) {
        expression.printTo(buffer);
        buffer.append("*");
    }

    public <RETURN_TYPE> RETURN_TYPE acceptVisitor(GrammarRuleVisitor<RETURN_TYPE> visitor) {
        return visitor.visit(this);
    }
}
