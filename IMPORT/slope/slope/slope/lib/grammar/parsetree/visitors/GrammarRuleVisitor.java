package slope.lib.grammar.parsetree.visitors;

import slope.lib.grammar.parsetree.*;

public interface GrammarRuleVisitor<RETURN_TYPE> {
    public RETURN_TYPE visit(Alternative a);

    public RETURN_TYPE visit(BuiltInTerminal a);

    public RETURN_TYPE visit(BracketExpression e);

    public RETURN_TYPE visit(Concatenation e);

    public RETURN_TYPE visit(LibraryTerminal e);

    public RETURN_TYPE visit(MultiplePlus e);

    public RETURN_TYPE visit(MultipleStar e);

    public RETURN_TYPE visit(NonTerminal e);

    public RETURN_TYPE visit(Optional e);

    //	public RETURN_TYPE visit(ReturnCode e);
    public RETURN_TYPE visit(StringTerminal e);

    public RETURN_TYPE visit(TokenRefTerminal e);
}
