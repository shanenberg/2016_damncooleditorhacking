package slope.lib.grammar.parsetree.visitors;

import slope.lib.grammar.parsetree.*;

public interface GrammarRuleWalker {
    public void visit(Alternative a);

    public void visit(BracketExpression e);

    public void visit(Concatenation e);

    public void visit(LibraryTerminal e);

    public void visit(MultiplePlus e);

    public void visit(MultipleStar e);

    public void visit(NonTerminal e);

    public void visit(Optional e);

    //	public void visit(ReturnCode e);
    public void visit(StringTerminal e);

    public void visit(TokenRefTerminal e);
}
