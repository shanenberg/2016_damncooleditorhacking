package slope.lib.grammar.parsetree.visitors;

import slope.Rule;
import slope.lib.grammar.parsetree.*;

public class Walker implements GrammarRuleVisitor<Rule> {

    public Rule visit(final Alternative e) {
//System.out.println("ALTERNATIVE");
        return null;
    }

    public Rule visit(final BracketExpression e) {
//System.out.println("BRACKETEXPR");
        return null;
    }

    public Rule visit(final Concatenation e) {
        return null;
    }

    public Rule visit(final MultiplePlus e) {
        return null;
    }

    public Rule visit(final MultipleStar e) {
        return null;
    }

    public Rule visit(final NonTerminal e) {
        return null;
    }

    public Rule visit(final Optional e) {
        return null;
    }

    //	public Rule visit(ReturnCode e) {
//		return null;
//	}
//	
    public Rule visit(final LibraryTerminal e) {
        return null;
    }

    public Rule visit(final StringTerminal e) {
        return null;
    }


    public Rule visit(final TokenRefTerminal e) {
        return null;
    }

    public Rule visit(final BuiltInTerminal e) {
        return null;
    }

}
