package slope.lib.regex;

import slope.PC;
import slope.Parser;
import slope.Rule;
import slope.SetCmd;
import slope.lib.lambda.v01.parsetree.LambdaExpression;
import slowscane.Tokens;

/*
   RegEx:
   
     RegEx -> (BracketExpression | Literal) ('|'? RegEx)*.
     BracketExpression -> '(' RegEx ')'.
     Literal -> javaEscapedChar.
   
 */

public class RegExParserTokens extends Tokens {

    public class RegExParser extends Parser<RegExParserTokens> {
        public Rule RegEx(final SetCmd<LambdaExpression> resultSetter) {
            return new Rule() {
                public PC pc() {
                    return
                            AND(
                                    OR(
                                            BracketExpression(null), Literal(null)
                                    ),
                                    NFOLD(
                                            OR(
                                                    AND(TOKEN('|'), RegEx(null)),
                                                    RegEx(null)
                                            )
                                    )
                            );
                }
            };
        }

        public Rule BracketExpression(final SetCmd<LambdaExpression> resultSetter) {
            return new Rule() {
                public PC pc() {
                    return AND(TOKEN('('), RegEx(null), TOKEN(')'));
                }
            };
        }

        public Rule Literal(final SetCmd<LambdaExpression> resultSetter) {
            return new Rule() {
                public PC pc() {
                    return null;
                }
            };
        }

    }

}


