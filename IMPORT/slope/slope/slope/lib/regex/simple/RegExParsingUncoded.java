package slope.lib.regex.simple;

import slowscane.annotations.TokenLevel;
import slowscane.scanners.TokenReader;

/*
   The RegExParsingUncoded translates a RegEx in an uncoded way.
   For example, the token "\n" is not translated into a newLine, but into the
   Token "\\n"
 */

public class RegExParsingUncoded extends RegExParsing {
    @TokenLevel(50)
    public TokenReader<String> JAVA_CHARACTER_LITERAL() {
        return lib.JAVA_CHARACTER_LITERAL_UNCODED;
    }
}


