package damncooleditorhackers;

import damncooleditorhackers.view.EditorView;

/**
 * Visuelle Oberfläche gestalten und Logik
 **/
public class Main {
    public static void main(String[] args) {
        (new EditorView()).start(EditorGrammar.Example);
    }
}

