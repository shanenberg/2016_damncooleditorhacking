package damncooleditorhackers.controller;

import damncooleditorhackers.EditorGrammar;
import damncooleditorhackers.controller.commands.*;
import damncooleditorhackers.controller.commands.helper.ImExHelper;
import damncooleditorhackers.controller.commands.helper.LoadingHelper;
import damncooleditorhackers.model.EditorModel;
import damncooleditorhackers.model.ElementTree;
import damncooleditorhackers.model.ModelController;
import damncooleditorhackers.model.grammarElements.*;
import damncooleditorhackers.view.EditorView;
import damncooleditorhackers.view.ViewUpdater;
import damncooleditorhackers.view.view_elements.ElementButton;

import java.util.ArrayList;

/**
 * Created by Liron on 15.06.2016.
 */
public class EditorController implements ViewController {

    private ViewUpdater view;
    private ModelController model;
    private CommandController commandController;
    private Element copy;
    private EditorGrammar startGrammar;

    public EditorController(EditorView view) {
        copy = null;
        this.view = view;
        model = new EditorModel(this);
    }

    public void loadGrammar(EditorGrammar startGrammar) {
        this.startGrammar = startGrammar;
        model.resetGrammar(startGrammar);
        updateView();
    }

    public CommandController setController(ElementTree elementTree, EditorModel model) {
        commandController = new CommandController(this, model, elementTree);
        return commandController;
    }

    public void updateView() {
        view.updateSelectionDialog(model.getSelectionList(), model.getLastButtonLocation());
        view.updateElements(model.getElements());
    }

    public void clickedResetGrammar() {
        view.deregisterKeyListener();
        if (view.showConfirmationDialog("Grammatik zurücksetzen")) {
            model.resetGrammar(startGrammar);
            updateView();
        }
        view.reregisterKeyListener();
    }

    public void clickedDeleteElement(ArrayList<ElementButton> buttons) {
        ArrayList<Element> temp = new ArrayList<Element>();
        for (ElementButton b : buttons) temp.add(b.getElement());
        commandController.doCommand(new DeleteCommand(temp, commandController.getElementTree(), buttons.get(0)));
        model.getSelectionList().clear();
        updateView();
    }

    public void clickedMenuLoad() {
        LoadingHelper loadingHelper;
        loadingHelper = new LoadingHelper(view, commandController.getElementTree(), commandController);
        loadingHelper.displayLoadChooser(view.getFrame());
        updateView();
    }

    public void clickedDuplicate(ArrayList<ElementButton> buttons) {
        ArrayList<Element> temp = new ArrayList<Element>();
        for (ElementButton b : buttons) temp.add(b.getElement());
        commandController.doCommand(new DuplicateCommand(temp, commandController.getElementTree(), buttons.get(0)));
        updateView();
    }

    public void clickedReplace(ArrayList<ElementButton> buttons) {
        ArrayList<Element> temp = new ArrayList<Element>();
        for (ElementButton b : buttons) temp.add(b.getElement());
        commandController.doCommand(new ReplacementCommand(temp, commandController.getElementTree(), buttons.get(0)));
        updateView();
    }

    public void clickedElement(ElementButton selected) {
        model.getSelectionList().clear();
        switchType(selected);
        updateView();
    }

    public void clickedCut(ArrayList<ElementButton> buttons) {
        ArrayList<Element> temp = new ArrayList<Element>();
        for (ElementButton b : buttons) temp.add(b.getElement());
        commandController.doCommand(new CutCommand(temp, commandController.getElementTree(), buttons.get(0)));
        updateView();

    }

    public void clickedPaste(ElementButton selected) {
        if (copy != null)
            commandController.doCommand(new PasteCommand(selected.getElement(), commandController.getElementTree(), selected, copy));
        updateView();
    }

    public void clickedCopy(ArrayList<ElementButton> buttons) {
        ArrayList<Element> temp = new ArrayList<Element>();
        for (ElementButton b : buttons) temp.add(b.getElement());
        commandController.doCommand(new CopyCommand(temp, commandController.getElementTree(), buttons.get(0)));
        updateView();
    }

    public void clickedUndo() {
        commandController.undoCommand();
    }

    public void clickedRedo() {
        commandController.redoCommand();
    }

    public void editedIdentifier(String before, ElementButton elementButton) {
        model.getSelectionList().clear();
        commandController.doCommand(new TokenCommand(before, view, commandController.getElementTree(), elementButton));
        updateView();
    }

    private Element findChildrenByName(ElementButton elementButton, String assumption) {
        ArrayList<Element> children = (elementButton.getElement().getType() == Type.OR) ? ((ORElement) elementButton.getElement()).getChoices() : elementButton.getElement().getOperator() == Op.OR ? ((ORElement) elementButton.getElement().getParent()).getChoices() : elementButton.getElement().getChildren();

        for (Element child : children) {
            if (child.getType() != Type.OR) while (child.getChildren().size() > 0) child = child.getChildren().get(0);
            else {
                for (Element choice : ((ORElement) child).getChoices()) {
                    if (choice.toString().equals(assumption)) {
                        child = choice;
                        break;
                    }
                }
            }
            if (child.toString().equals(assumption)) {
                return child;
            }
        }
        return null;
    }

    public void assumedElement(ElementButton elementButton, String assumption) {
        Element child = findChildrenByName(elementButton, assumption);
        if (child != null) {
                elementButton.setElement(child);
                commandController.doCommand(new AssumeCommand(child, commandController.getElementTree(), elementButton));
                clickedSelectionElement(elementButton);
        } else {
            model.getSelectionList().clear();
            updateView();
            }
    }

    public void assumedSelectionElement(ElementButton elementButton, String assumption) {
        for (Element selection : model.getSelectionList()) {
            if (selection.getText().equals(assumption)) {
                elementButton.setElement(selection);
                clickedSelectionElement(elementButton);
                return;
            }
        }
        model.getSelectionList().clear();
        updateView();
    }

    public void clickedSelectionElement(ElementButton selected) {
        commandController.doCommand(new SelectionCommand(selected.getElement(), commandController.getElementTree(), selected));
        model.getSelectionList().clear();
        updateView();
    }

    private void switchType(ElementButton selected) {
        switch (selected.getElement().getType()) {
            case OR:
                commandController.doCommand(new ORCommand((ORElement) selected.getElement(), commandController.getElementTree(), selected));
                break;
            case NFOLD:
                commandController.doCommand(new NFOLDCommand(model.getElements(), (NFoldElement) selected.getElement(), view, commandController.getElementTree(), selected));
                break;
            case OPTIONAL:
                commandController.doCommand(new OptionalCommand((OptionalElement) selected.getElement(), commandController.getElementTree(), selected));
                break;
            case ADD_RIGHT:
                commandController.doCommand(new AddRightCommand(selected.getElement(), commandController.getElementTree(), selected));
                break;
            case RULE:
                commandController.doCommand(new RuleCommand((RuleElement) selected.getElement(), commandController.getElementTree(), selected));
                break;
            default:
                System.out.println("Missing handle method for " + selected.getElement().getType());
                break;
        }
    }

    public void setCopy(Element copy) {
        this.copy = copy;
    }

    public void clickedExport() {
        ImExHelper.exportGrammer(model.getElements(), view.getUserName(), view.getCurrentGrammar());
    }

    public String getElementsAsString() {
        return ImExHelper.grammarToText(commandController.getElementTree());
    }

    public String getUserName() {
        return view.getUserName();
    }

    public EditorGrammar getCurrentGrammar() {
        return view.getCurrentGrammar();
    }

    public ViewUpdater getView() {
        return view;
    }
}