package damncooleditorhackers.controller;

import damncooleditorhackers.EditorGrammar;

import java.io.File;
import java.io.FileWriter;
import java.text.SimpleDateFormat;

/**
 * Created by Niklas on 23.09.2016.
 */
public final class Logger {
    private static long startTime = -1;

    private Logger() {
    }

    private static void start(String username, EditorGrammar currentGrammar) {
        File dir = new File("logs");
        dir.mkdir();
        File user = new File("logs/" + username);
        user.mkdir();
        File grammar = new File("logs/" + username + "/" + currentGrammar.toString());
        grammar.mkdir();
        startTime = System.currentTimeMillis();
    }

    public static void log(String operation, String timer, String username, EditorGrammar currentgrammar) {
        username = username.replace(" ", "");
        if (startTime == -1) start(username, currentgrammar);
        String filename = "logs/" + username + "/" + currentgrammar.toString() + "/editor-log-" + username + "-" + (new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss").format(startTime)).replace(".", "") + ".txt";
        File file = new File(filename);
        try {
            FileWriter writer = new FileWriter(file, true);
            writer.write(new SimpleDateFormat("dd.MM.yyyy-HH:mm:ss").format(System.currentTimeMillis()) + ", " + timer + ", " + operation + "\n");
            writer.flush();
            writer.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
