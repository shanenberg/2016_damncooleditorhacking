package damncooleditorhackers.controller;

import damncooleditorhackers.view.view_elements.TimingPanel;

import javax.swing.*;
import java.awt.*;
import java.text.SimpleDateFormat;
import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by Niklas on 23.09.2016.
 */
public class StopWatch {
    protected final TimingPanel timingPanel;
    protected long startTime = 0;
    private long pauseTime = 0;
    protected boolean running = false;
    protected boolean started = false;
    protected static final long TOTAL_DURATION = 45 * 60 * 1000; // 45 Minutes

    public StopWatch(TimingPanel timingPanel) {
        this.timingPanel = timingPanel;
    }

    public void start(final JLabel target) {
        started = true;
        startTime = System.currentTimeMillis();
        Timer timer = new Timer();
        timer.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                if (running) {
                    long elapsed = (System.currentTimeMillis() - startTime);
                    target.setText(new SimpleDateFormat("mm:ss").format(elapsed));
                    timingPanel.getParent().repaint();
                    if (elapsed >= TOTAL_DURATION) target.setForeground(Color.red);
                }
            }
        }, 0, 1000);
        this.running = true;
    }

    public void pause() {
        pauseTime = System.currentTimeMillis();
        running = false;
    }

    public void restart() {
        startTime = startTime + (System.currentTimeMillis() - pauseTime);
        running = true;
    }

    public boolean isPaused() {
        return !running && started;
    }

    public void setStartTime(long time) {
        startTime = time;
        pauseTime = System.currentTimeMillis();
    }
}
