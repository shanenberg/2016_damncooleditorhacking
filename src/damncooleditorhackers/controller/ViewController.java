package damncooleditorhackers.controller;


import damncooleditorhackers.EditorGrammar;
import damncooleditorhackers.controller.commands.CommandController;
import damncooleditorhackers.model.EditorModel;
import damncooleditorhackers.model.ElementTree;
import damncooleditorhackers.view.view_elements.ElementButton;

import java.util.ArrayList;

/**
 * Created by Thorben on 16.06.2016.
 */

public interface ViewController {
    // All the different Actions that could happen in the View

    void clickedResetGrammar();

    void clickedMenuLoad();

    void clickedElement(ElementButton selected);

    void clickedSelectionElement(ElementButton selected);

    CommandController setController(ElementTree elementTree, EditorModel model);

    void clickedUndo();

    void clickedRedo();

    void clickedCut(ArrayList<ElementButton> buttons);

    void clickedPaste(ElementButton selected);

    void clickedCopy(ArrayList<ElementButton> buttons);

    void clickedReplace(ArrayList<ElementButton> buttons);

    void clickedDuplicate(ArrayList<ElementButton> buttons);

    void editedIdentifier(String before, ElementButton elementButton);

    void assumedElement(ElementButton elementButton, String assumption);

    void clickedDeleteElement(ArrayList<ElementButton> buttons);

    void clickedExport();

    String getElementsAsString();

    void assumedSelectionElement(ElementButton elementButton, String assumption);

    void loadGrammar(EditorGrammar startGrammar);
}
