package damncooleditorhackers.controller.commands;

import damncooleditorhackers.controller.EditorController;
import damncooleditorhackers.model.ElementTree;
import damncooleditorhackers.model.ModelController;
import damncooleditorhackers.model.grammarElements.*;
import damncooleditorhackers.view.view_elements.ElementButton;

/**
 * Created by Nick on 28.06.2016.
 */
public class AddRightCommand extends Command {

    public AddRightCommand(Element selected, ElementTree rule, ElementButton button) {
        super(selected, rule, button);
        this.selected = selected;
    }

    public AddRightCommand(String id, ElementTree rule) {
        super(id, rule);
        exchangeElement();
    }

    boolean execute(ModelController model, EditorController controller) {
        if (!loaded) {
            if (getOriginalButton().isShowing()) setLastButtonLocation(getOriginalButton().getLocationOnScreen());
            model.setLastButtonLocation(getLastButtonLocation());
        }
        model.getSelectionList().clear();
        model.getSelectionList().addAll(((AddRightElement) selected).getOptionals(selected.getParent()));
        for (Element e : model.getSelectionList())
            if (e.getType() == Type.NFOLD) ((NFoldElement) e).setSelectionFromChild(selected.getParent());
        if (model.getSelectionList().size() == 1) new SelectionCommand("0", elementTree).execute(model, controller);
        return true;
    }

    @Override
    public void exchangeElement() {
        if (selected != null) {
            selected.setParent(getEqualElement(originalID, elementTree.getRoot(), 0));
        } else {
            selected = new AddRightElement(Op.AND, getEqualElement(originalID, elementTree.getRoot(), 0));
        }
        //setElement(selected);
    }

    @Override
    protected void saveID() {
        if (selected != null) {
            originalID.clear();
            originalID.addAll(selected.getParent().getIDs());
        }
    }
}
