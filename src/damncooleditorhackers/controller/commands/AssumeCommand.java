package damncooleditorhackers.controller.commands;

import damncooleditorhackers.controller.EditorController;
import damncooleditorhackers.model.ElementTree;
import damncooleditorhackers.model.ModelController;
import damncooleditorhackers.model.grammarElements.Element;
import damncooleditorhackers.model.grammarElements.ORElement;
import damncooleditorhackers.model.grammarElements.Op;
import damncooleditorhackers.view.view_elements.ElementButton;

/**
 * Created by Niklas on 16.09.2016.
 */
public class AssumeCommand extends Command {

    public AssumeCommand(Element selected, ElementTree rule, ElementButton button) {
        super(selected, rule, button);
    }

    public AssumeCommand(String id, ElementTree rule) {
        super(id, rule);
        exchangeElement();
    }

    public boolean execute(ModelController model, EditorController controller) {
        super.saveID();
        if (!loaded) {
            if (getOriginalButton().isShowing()) setLastButtonLocation(getOriginalButton().getLocationOnScreen());
            model.setLastButtonLocation(getLastButtonLocation());
        }
        model.getSelectionList().clear();
        if (selected.hasParent()) {
            if (selected.getOperator() == Op.OR)
                model.getSelectionList().addAll(((ORElement) selected.getParent()).getChoices());
            model.getSelectionList().addAll(selected.getParent().getChildren());
        } else model.getSelectionList().add(selected);
        return true;
    }
}
