package damncooleditorhackers.controller.commands;

import damncooleditorhackers.controller.EditorController;
import damncooleditorhackers.controller.commands.helper.ImExHelper;
import damncooleditorhackers.model.ElementTree;
import damncooleditorhackers.model.ModelController;
import damncooleditorhackers.model.grammarElements.Element;
import damncooleditorhackers.model.grammarElements.ORElement;
import damncooleditorhackers.model.grammarElements.OptionalElement;
import damncooleditorhackers.model.grammarElements.Type;
import damncooleditorhackers.view.view_elements.ElementButton;

import java.awt.*;
import java.util.ArrayList;

/**
 * Created by Nick on 21.06.2016.
 */
public abstract class Command {
    ArrayList<Integer> originalID;
    protected Element selected;
    protected ElementTree elementTree;
    ElementButton clickedButton;
    private Point lastButtonLocation;
    protected boolean loaded;

    Command(Element selected, ElementTree elementTree, ElementButton button) {
        originalID = new ArrayList<Integer>();
        this.selected = selected;
        this.elementTree = elementTree;
        this.clickedButton = button;
        loaded = false;
        this.saveID();
    }

    Command(String id, ElementTree elementTree) {
        originalID = ImExHelper.StringToID(id);
        this.elementTree = elementTree;
        loaded = true;
    }

    protected void saveID() {
        if (selected != null) {
            originalID.clear();
            originalID.addAll(selected.getIDs());
        }
    }

    abstract boolean execute(ModelController model, EditorController controller);

    void setElement(Element e) {
        selected = e;
        if (clickedButton != null) clickedButton.setElement(selected);
    }

    ElementButton getOriginalButton() {
        return clickedButton;
    }

    protected void setLastButtonLocation(Point lastButtonLocation) {
        this.lastButtonLocation = lastButtonLocation;
    }

    public Point getLastButtonLocation() {
        return lastButtonLocation;
    }

    public String export(ImExHelper helper) {
        String tmp = "";
        String classname = getClass().getName();
        String[] name = classname.split("\\.");
        tmp += name[name.length - 1];
        tmp += ",";
        tmp += ImExHelper.IDToString(originalID);
        tmp += "\n";
        return tmp;
    }

    Element getEqualElement(ArrayList<Integer> iDs, Element e, int depth) {
        if (e.sameID(iDs)) return e;
        if (e.getIDs().size() < iDs.size()) {
            if (e.getType() == Type.OR) {
                for (Element tmp : ((ORElement) e).getChoices())
                    if (tmp.getIDs().get(depth + 1).equals(iDs.get(depth + 1)))
                        return getEqualElement(iDs, tmp, ++depth);
            } else if (e.getType() == Type.OPTIONAL) {
                return getEqualElement(iDs, ((OptionalElement) e).getOptional(), ++depth);
            } else
                for (Element tmp : e.getChildren())
                    if (tmp.getIDs().get(depth + 1).equals(iDs.get(depth + 1)))
                        return getEqualElement(iDs, tmp, ++depth);
        }
        return null;
    }

    protected ArrayList<Element> cleanUp(ArrayList<Element> dirty) {
        ArrayList<Element> tmp = new ArrayList<Element>();
        for (Element e : dirty) if (e != null) tmp.add(e);
        return tmp;
    }

    public void exchangeElement() {
        this.setElement(getEqualElement(originalID, elementTree.getRoot(), 0));
    }


    public static Element findParentOfAll(ArrayList<Element> selected) {
        if (selected.size() == 0) return null;
        if (selected.size() == 1) return selected.get(0);
        if (selected.size() > 1) {
            Element tmp = selected.get(0);
            for (int i = 0; i < selected.size() - 1; i++)
                tmp = tmp.findParent(selected.get(i + 1));
            return tmp;
        }
        return null;
    }
}
