package damncooleditorhackers.controller.commands;

import damncooleditorhackers.controller.EditorController;
import damncooleditorhackers.controller.commands.helper.ImExHelper;
import damncooleditorhackers.model.ElementTree;
import damncooleditorhackers.model.ModelController;

import java.util.ArrayList;

/**
 * Created by Nick on 21.06.2016.
 */
public class CommandController {
    private ArrayList<Command> commands;
    private int nextIndex;
    private EditorController controller;
    private ModelController model;
    private ElementTree elementTree;
    private ImExHelper helper;

    public CommandController(EditorController controller, ModelController model, ElementTree elementTree) {
        this.elementTree = elementTree;
        commands = new ArrayList<Command>();
        nextIndex = 0;
        this.controller = controller;
        this.model = model;
        helper = new ImExHelper(controller.getCurrentGrammar());
    }

    public ElementTree getElementTree() {
        return elementTree;
    }

    public void doCommand(Command command) {
        if (command != null)
        if ((commands.size() == 0) ||
                (!(commands.size() > 0 && nextIndex > 0 && commands.get(nextIndex - 1).selected == command.selected && commands.get(nextIndex - 1).getClass() == command.getClass())) ||
                (command instanceof TokenCommand) ||
                (command instanceof DuplicateCommand)) {
            if (command.execute(model, controller)) {
                addCommand(command);
                helper.writeToFile(command.export(helper), controller.getUserName());
            }
        } else
            commands.get(nextIndex - 1).execute(model, controller);
    }

    private void addCommand(Command command) {
        for (int i = commands.size() - nextIndex; i > 0; i--)
            if (commands.size() > 0) commands.remove(commands.size() - 1);
        commands.add(command);
        nextIndex = commands.size();
    }

    public void undoCommand() {
        if (nextIndex > 0 && !commands.get(nextIndex - 1).loaded) {
            nextIndex--;
            elementTree.reset();
            helper.clearLog(controller.getUserName());
            redoCommands();
            controller.updateView();
        }
    }

    private void redoCommands() {
        model.getSelectionList().clear();
        for (int i = 0; i < nextIndex; i++) { //redo all command from 0 to nextIndex
            Command command = commands.get(i);
            command.exchangeElement();
            command.execute(model, controller); //redo command
            helper.writeToFile(command.export(helper), controller.getUserName());
        }
    }

    public void redoCommand() {
        if (nextIndex < commands.size()) {
            Command command = commands.get(nextIndex);
            nextIndex++;
            command.exchangeElement();
            if (!(command instanceof SelectionCommand)) model.getSelectionList().clear();
            command.execute(model, controller);
            controller.updateView();
        }
    }

}
