package damncooleditorhackers.controller.commands;

import damncooleditorhackers.controller.EditorController;
import damncooleditorhackers.controller.commands.helper.ImExHelper;
import damncooleditorhackers.model.ElementTree;
import damncooleditorhackers.model.ModelController;
import damncooleditorhackers.model.grammarElements.*;
import damncooleditorhackers.view.view_elements.ElementButton;

import java.util.ArrayList;

/**
 * Created by Mark-Desktop on 14.07.2016.
 */
public class CopyCommand extends Command {
    ArrayList<Element> myElements;
    protected Element copy;
    Element newRoot;
    private ArrayList<ArrayList<Integer>> oldIDs;

    public CopyCommand(ArrayList<Element> myElements, ElementTree rule, ElementButton button) {
        super(button.getElement(), rule, button);
        ArrayList<Element> selected = new ArrayList<Element>();
        for (Element e : myElements) if (e.getType() != Type.ADD_RIGHT) selected.add(e);
        this.myElements = selected;
    }

    public CopyCommand(String ids, ElementTree rule) {
        super(ids.split(";")[0], rule);
        oldIDs = new ArrayList<ArrayList<Integer>>();
        String[] myIDs = ids.split(";");
        for (String s : myIDs) oldIDs.add(ImExHelper.StringToID(s));
        exchangeElement();
    }

    public boolean execute(ModelController model, EditorController controller) {
        if (myElements.size() > 0) {
            Element tmp = findParentOfAll(myElements);
            if (tmp == null) return false;
            while (tmp.getParent() != null && tmp.getParent().getChildren().size() == 1) tmp = tmp.getParent();
            Element newOld = CopyHandler.handle(tmp);
            newRoot = newOld;
            if (tmp.getParent() == null) elementTree.setRoot(newOld);
            else {
                int index = tmp.getParent().getChildren().indexOf(tmp);
                tmp.getParent().getChildren().remove(index);
                tmp.getParent().getChildren().add(index, newOld);
            }
            tmp = clearUp(tmp);
            copy = tmp;
            controller.setCopy(tmp);
        }
        return false; //dont save copy Command
    }

    private Element clearUp(Element e) {
        e = clearUpLeft(e);
        e = clearUpRight(e);
        return e;
    }

    private Element clearUpLeft(Element e) {
        Element left = myElements.get(0);
        while (left != e) {
            if (left.getParent() != null &&
                    (left.getParent() instanceof NFoldElement) &&
                    left.getParent().getChildren().indexOf(left) != 0) {
                left.getParent().getChildren().remove(0); //there was a nfold with a unselected statement
            } else { //TODO Remove all Optionals left sided
                left = left.getParent();
            }
        }
        return left;
    }

    private Element clearUpRight(Element e) {
        Element right = myElements.get(myElements.size() - 1);
        while (right != e) {
            if (right.getParent() != null &&
                    (right.getParent() instanceof NFoldElement) &&
                    right.getParent().getChildren().indexOf(right) != right.getParent().getChildren().size() - 1) {
                right.getParent().getChildren().remove(right.getParent().getChildren().size() - 1); //there was a nfold with a unselected statement
            } else {
                if (right.getParent() != null &&
                        right.getParent().getChildren().indexOf(right) != right.getParent().getChildren().size() - 1) {
                    for (int i = right.getParent().getChildren().indexOf(right) + 1; i < right.getParent().getChildren().size(); i++)
                        if (right.getParent().getChildren().get(i).getType() == Type.OPTIONAL)
                            ((OptionalElement) right.getParent().getChildren().get(i)).removeOptional();
                    right = right.getParent();
                } else
                    right = right.getParent();
            }
        }
        return right;
    }

    protected void saveAllIDs() {
        super.saveID();
        oldIDs.clear();
        for (Element e : myElements) {
            ArrayList<Integer> ID = new ArrayList<Integer>();
            ID.addAll(e.getIDs());
            oldIDs.add(ID);
        }
    }

    @Override
    public String export(ImExHelper helper) {
        String tmp = "";
        String classname = getClass().getName();
        String[] name = classname.split("\\.");
        tmp += name[name.length - 1];
        tmp += ",";
        for (int i = 0; i < oldIDs.size(); i++)
            tmp += ImExHelper.IDToString(oldIDs.get(0)) + (i < oldIDs.size() - 1 ? ";" : "");
        tmp += "\n";
        return tmp;
    }

}
