package damncooleditorhackers.controller.commands;

import damncooleditorhackers.controller.EditorController;
import damncooleditorhackers.controller.commands.helper.DeletionHelper;
import damncooleditorhackers.controller.commands.helper.ImExHelper;
import damncooleditorhackers.model.ElementTree;
import damncooleditorhackers.model.ModelController;
import damncooleditorhackers.model.grammarElements.Element;
import damncooleditorhackers.model.grammarElements.Type;
import damncooleditorhackers.view.view_elements.ElementButton;

import java.util.ArrayList;

/**
 * Created by Mark-Desktop on 15.07.2016.
 */
public class CutCommand extends CopyCommand {
    private boolean executed;
    private ArrayList<ArrayList<Integer>> oldIDs;

    public CutCommand(ArrayList<Element> myElements, ElementTree rule, ElementButton button) {
        super(myElements, rule, button);
        ArrayList<Element> selected = new ArrayList<Element>();
        for (Element e : myElements) if (e.getType() != Type.ADD_RIGHT) selected.add(e);
        this.myElements = selected;
        executed = false;
        oldIDs = new ArrayList<ArrayList<Integer>>();
        saveAllIDs();
    }

    public CutCommand(String ids, ElementTree rule) {
        super(ids.split(";")[0], rule);
        oldIDs = new ArrayList<ArrayList<Integer>>();
        String[] myIDs = ids.split(";");
        for (String s : myIDs) oldIDs.add(ImExHelper.StringToID(s));
        exchangeElement();
    }

    @Override
    public boolean execute(ModelController model, EditorController controller) {
        if (myElements.size() > 0) {
            Element tmp = findParentOfAll(myElements);
            DeletionHelper deletionHelper = new DeletionHelper();
            if (tmp == null) return false;
            while (tmp.getParent() != null && tmp.getParent().getChildren().size() == 1) tmp = tmp.getParent();
            deletionHelper.prepareDelete(tmp, myElements, elementTree);
            if (!executed) super.execute(model, controller);
            newRoot = tmp;
            deletionHelper.delete(newRoot);
            deletionHelper.setDeletion(false, copy);
            executed = true;
            model.getSelectionList().clear();
            return true;
        }
        return false;
    }

    protected void saveAllIDs() {
        super.saveID();
        oldIDs.clear();
        for (Element e : myElements) {
            ArrayList<Integer> ID = new ArrayList<Integer>();
            ID.addAll(e.getIDs());
            oldIDs.add(ID);
        }
    }

    @Override
    public void exchangeElement() {
        super.exchangeElement();
        ArrayList<Element> tmp = new ArrayList<Element>();
        for (int i = 0; i < oldIDs.size(); i++) {
            tmp.add(getEqualElement(oldIDs.get(i), elementTree.getRoot(), 0));
        }
        myElements = tmp;
    }

    @Override
    public String export(ImExHelper helper) {
        String tmp = "";
        String classname = getClass().getName();
        String[] name = classname.split("\\.");
        tmp += name[name.length - 1];
        tmp += ",";
        for (int i = 0; i < oldIDs.size(); i++)
            tmp += ImExHelper.IDToString(oldIDs.get(0)) + (i < oldIDs.size() - 1 ? ";" : "");
        tmp += "\n";
        return tmp;
    }
}
