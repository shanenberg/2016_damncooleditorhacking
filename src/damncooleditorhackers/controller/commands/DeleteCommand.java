package damncooleditorhackers.controller.commands;

import damncooleditorhackers.controller.EditorController;
import damncooleditorhackers.controller.commands.helper.DeletionHelper;
import damncooleditorhackers.controller.commands.helper.ImExHelper;
import damncooleditorhackers.model.ElementTree;
import damncooleditorhackers.model.ModelController;
import damncooleditorhackers.model.grammarElements.*;
import damncooleditorhackers.view.view_elements.ElementButton;

import javax.swing.*;
import java.util.ArrayList;

/**
 * Created by Niklas on 18.09.2016.
 */
public class DeleteCommand extends Command {

    private ArrayList<Element> myElements;
    private final ArrayList<ArrayList<Integer>> oldIDs;
    private boolean first;
    private boolean hasOptionals;

    public DeleteCommand(ArrayList<Element> selectedElements, ElementTree rule, ElementButton button) {
        super(button.getElement(), rule, button);
        ArrayList<Element> selected = new ArrayList<Element>();
        for (Element e : selectedElements) if (e.getType() != Type.ADD_RIGHT) selected.add(e);
        this.myElements = selected;
        oldIDs = new ArrayList<ArrayList<Integer>>();
        first = true;
        hasOptionals = button.hasOptionals();
        saveAllIDs();
    }

    public DeleteCommand(String ids, ElementTree rule, String optional) {
        super(ids.split(";")[0], rule);
        oldIDs = new ArrayList<ArrayList<Integer>>();
        String[] myIDs = ids.split(";");
        hasOptionals = optional.equals("1");
        for (String s : myIDs) oldIDs.add(ImExHelper.StringToID(s));
        exchangeElement();
        super.saveID();
        first = false;
    }

    public boolean execute(ModelController model, EditorController controller) {
        myElements = cleanUp(myElements);
        for (Element check : myElements) {
            if (check instanceof ReplacementElement ||
                    check instanceof OptionalContainer ||
                    check instanceof AddRightElement) return false;
        }
        if (myElements.size() < 1) return false;
        if (!loaded && getOriginalButton().isShowing()) {
            setLastButtonLocation(getOriginalButton().getLocationOnScreen());
        }
        if (myElements.size() == 1) {
            if ((myElements.get(0).getType() == Type.OPTIONAL)) {
                ((OptionalElement) myElements.get(0)).setVisible(false);
                return true;
            } else {
                if (myElements.get(0).getType() == Type.TOKEN && hasOptionals) {
                    Element tmp = myElements.get(0);
                    while (tmp.getParent().getType() != Type.NFOLD &&
                            tmp.getParent().getType() != Type.OPTIONAL &&
                            tmp.getParent().getType() != Type.OR) tmp = tmp.getParent();
                    myElements.clear();
                    ArrayList<Element> todelete = tmp.getLeaves();
                    if (todelete.size() > 2) todelete.remove(1);
                    myElements.addAll(todelete);
                }
            }
        }
        Element tmp = findParentOfAll(myElements);
        if (tmp == null) return false;
        DeletionHelper deletionHelper = new DeletionHelper();
        while (tmp.getParent() != null && tmp.getParent().getChildren().size() == 1) tmp = tmp.getParent();
        deletionHelper.prepareDelete(tmp, myElements, elementTree);
        ArrayList<Element> check = new ArrayList<Element>();
        for (Element el : elementTree.getElements()) if (el.isToBeDeleted()) check.add(el);
        if (check.size() != myElements.size()) {
            controller.getView().deregisterKeyListener();
            int result = 0;
            if (first)
                result = JOptionPane.showConfirmDialog(null, "Diese Elemente löschen?\n" + arrayListToString(check), "Löschen bestätigen", JOptionPane.YES_NO_OPTION);
            controller.getView().reregisterKeyListener();
            if (result != 0)
                return false;

        }
        first = false;
        deletionHelper.delete(tmp);
        return true;
    }

    private void saveAllIDs() {
        super.saveID();
        for (Element e : myElements) {
            ArrayList<Integer> ID = new ArrayList<Integer>();
            ID.addAll(e.getIDs());
            oldIDs.add(ID);
        }
    }

    @Override
    public void exchangeElement() {
        super.exchangeElement();
        ArrayList<Element> tmp = new ArrayList<Element>();
        for (int i = 0; i < oldIDs.size(); i++) {
            tmp.add(getEqualElement(oldIDs.get(i), elementTree.getRoot(), 0));
        }
        myElements = tmp;
    }

    @Override
    public String export(ImExHelper helper) {
        String tmp = "";
        String classname = getClass().getName();
        String[] name = classname.split("\\.");
        tmp += name[name.length - 1];
        tmp += ",";
        for (int i = 0; i < oldIDs.size(); i++)
            tmp += ImExHelper.IDToString(oldIDs.get(0)) + (i < oldIDs.size() - 1 ? ";" : "");
        tmp += "," + (hasOptionals ? "1" : "0");
        tmp += "\n";
        return tmp;
    }

    private String arrayListToString(ArrayList<Element> elements) {
        StringBuilder stringBuilder = new StringBuilder();
        for (Element e : elements) {
            stringBuilder.append("[").append(e.toString()).append("] ");
        }
        return stringBuilder.substring(0, stringBuilder.length() - 1);
    }

   /*
    }*/
}
