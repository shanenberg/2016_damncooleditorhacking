package damncooleditorhackers.controller.commands;

import damncooleditorhackers.controller.EditorController;
import damncooleditorhackers.controller.commands.helper.ImExHelper;
import damncooleditorhackers.model.ElementTree;
import damncooleditorhackers.model.ModelController;
import damncooleditorhackers.model.grammarElements.*;
import damncooleditorhackers.view.view_elements.ElementButton;

import java.util.ArrayList;

/**
 * Created by Nick on 12.07.2016.
 */
public class DuplicateCommand extends Command {
    private final ArrayList<ArrayList<Integer>> oldIDs;
    private ArrayList<Element> elements;

    public DuplicateCommand(ArrayList<Element> elements, ElementTree rule, ElementButton button) {
        super(button.getElement(), rule, button);
        this.elements = elements;
        oldIDs = new ArrayList<ArrayList<Integer>>();
        saveAllIDs();
    }

    public DuplicateCommand(String ids, ElementTree rule) {
        super(ids.split(";")[0], rule);
        oldIDs = new ArrayList<ArrayList<Integer>>();
        String[] myIDs = ids.split(";");
        for (String s : myIDs) oldIDs.add(ImExHelper.StringToID(s));
        exchangeElement();
    }

    boolean execute(ModelController model, EditorController controller) {
        elements = cleanUp(elements);
        super.saveID();
        if (!loaded) {
            if (getOriginalButton().isShowing()) setLastButtonLocation(getOriginalButton().getLocationOnScreen());
            model.setLastButtonLocation(getLastButtonLocation());
        }
        Element parent = findParentOfAll(elements);
        while (parent != null && parent.getParent() != null && parent.getType() != Type.NFOLD)
            parent = parent.getParent();
        return parent != null && replaceDUPLICATE(getDuplicateElement(parent));
    }

    private void saveAllIDs() {
        super.saveID();
        oldIDs.clear();
        for (Element e : elements) {
            ArrayList<Integer> iDs = new ArrayList<Integer>();
            iDs.addAll(e.getIDs());
            oldIDs.add(iDs);
        }
    }

    private boolean replaceDUPLICATE(ReplacementElement sel) {
        if (sel.getText().equals("Duplicate Rule")) return false;
        else {
            if (sel.getParent().getType() == Type.NFOLD) {
                int addIndex = sel.getParent().getChildren().size() - 1;
                ArrayList<Element> toCopy = new ArrayList<Element>();
                for (Element e : sel.getParent().getChildren()) {
                    for (Element selected : sel.getChildren()) {
                        if (selected.hasParent(e) && !toCopy.contains(e)) {
                            addIndex = sel.getParent().getChildren().indexOf(e) + 1;
                            toCopy.add(e);
                        }
                    }
                }
                for (Element e : toCopy) {
                    Element copy = CopyHandler.handle(e);
                    copy.setParent(sel.getParent());
                    sel.getParent().addChildren(addIndex, copy);
                    addIndex++;
                }
                for (Element e : sel.getParent().getChildren()) e.updateID();
            } else return false;
            return true;
        }
    }


    private ReplacementElement getDuplicateElement(Element parent) {
        ReplacementElement duplicate = new ReplacementElement(ReplacementType.DUPLICATE, parent, parent);
        duplicate.getChildren().addAll(elements);
        if (parent == elementTree.getRoot()) duplicate.setText("Duplicate Rule");
        else duplicate.setText("Duplicate");
        return duplicate;
    }

    @Override
    public void exchangeElement() {
        super.exchangeElement();
        ArrayList<Element> tmp = new ArrayList<Element>();
        for (int i = 0; i < oldIDs.size(); i++) {
            tmp.add(getEqualElement(oldIDs.get(i), elementTree.getRoot(), 0));
        }
        elements = tmp;
    }

    @Override
    public String export(ImExHelper helper) {
        String tmp = "";
        String classname = getClass().getName();
        String[] name = classname.split("\\.");
        tmp += name[name.length - 1];
        tmp += ",";
        for (int i = 0; i < oldIDs.size(); i++)
            tmp += ImExHelper.IDToString(oldIDs.get(0)) + (i < oldIDs.size() - 1 ? ";" : "");
        tmp += "\n";
        return tmp;
    }
}
