package damncooleditorhackers.controller.commands;

import damncooleditorhackers.controller.EditorController;
import damncooleditorhackers.model.ElementTree;
import damncooleditorhackers.model.ModelController;
import damncooleditorhackers.model.grammarElements.Element;
import damncooleditorhackers.model.grammarElements.NFoldElement;
import damncooleditorhackers.view.ViewUpdater;
import damncooleditorhackers.view.view_elements.ElementButton;

import java.util.ArrayList;

/**
 * Created by Nick on 21.06.2016.
 */
public class NFOLDCommand extends Command {
    private ArrayList<Element> ruleList;

    public NFOLDCommand(ArrayList<Element> ruleList, NFoldElement selected, ViewUpdater view, ElementTree rule, ElementButton button) {
        super(selected, rule, button);
        this.ruleList = ruleList;
    }

    public NFOLDCommand(String id, ElementTree rule, ViewUpdater view) {
        super(id, rule);
        exchangeElement();
    }

    public boolean execute(ModelController model, EditorController controller) {
        super.saveID();
        if (!loaded) {
            if (getOriginalButton().isShowing()) setLastButtonLocation(getOriginalButton().getLocationOnScreen());
            model.setLastButtonLocation(getLastButtonLocation());
        }
        ruleList = elementTree.getElements();
        NFoldElement tmp = (NFoldElement) selected;
        int removedIndex = ruleList.indexOf(tmp);
        if (tmp.getChildren().size() == 0) tmp.traverseChildren();
        if (removedIndex != -1) ruleList.add(removedIndex, tmp.getChildren().get(0));
        return true;
    }

}
