package damncooleditorhackers.controller.commands;

import damncooleditorhackers.controller.EditorController;
import damncooleditorhackers.model.ElementTree;
import damncooleditorhackers.model.ModelController;
import damncooleditorhackers.model.grammarElements.ORElement;
import damncooleditorhackers.view.view_elements.ElementButton;

/**
 * Created by Nick on 21.06.2016.
 */
public class ORCommand extends Command {

    public ORCommand(ORElement selected, ElementTree rule, ElementButton button) {
        super(selected, rule, button);
    }

    public ORCommand(String id, ElementTree rule) {
        super(id, rule);
        exchangeElement();
    }

    public boolean execute(ModelController model, EditorController controller) {
        super.saveID();
        if (!loaded) {
            if (getOriginalButton().isShowing()) setLastButtonLocation(getOriginalButton().getLocationOnScreen());
            model.setLastButtonLocation(getLastButtonLocation());
        }
        model.getSelectionList().clear();
        model.getSelectionList().addAll(((ORElement) selected).getChoices());
        return true;
    }
}
