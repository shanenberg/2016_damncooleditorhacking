package damncooleditorhackers.controller.commands;

import damncooleditorhackers.controller.EditorController;
import damncooleditorhackers.model.ElementTree;
import damncooleditorhackers.model.ModelController;
import damncooleditorhackers.model.grammarElements.OptionalContainer;
import damncooleditorhackers.model.grammarElements.OptionalElement;
import damncooleditorhackers.model.grammarElements.ReplacementElement;
import damncooleditorhackers.model.grammarElements.ReplacementType;
import damncooleditorhackers.view.view_elements.ElementButton;

/**
 * Created by Nick on 10.09.2016.
 */
public class OptionalCommand extends Command {

    public OptionalCommand(OptionalElement selected, ElementTree rule, ElementButton button) {
        super(selected, rule, button);
    }

    public OptionalCommand(String id, ElementTree rule) {
        super(id, rule);
        exchangeElement();
    }

    public boolean execute(ModelController model, EditorController controller) {
        super.saveID();
        if (!loaded) {
            if (getOriginalButton().isShowing()) setLastButtonLocation(getOriginalButton().getLocationOnScreen());
            model.setLastButtonLocation(getLastButtonLocation());
        }
        model.getSelectionList().clear();
        if (((OptionalElement) selected).isVisible()) model.getSelectionList().add(selected);
        else
        model.getSelectionList().add(new OptionalContainer((OptionalElement) selected));
        if (((OptionalElement) selected).isVisible())
            model.getSelectionList().add(new ReplacementElement(ReplacementType.DELETEOPTIONAL, selected, selected));
        return true;
    }
}
