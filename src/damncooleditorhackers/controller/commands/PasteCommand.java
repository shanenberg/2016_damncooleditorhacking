package damncooleditorhackers.controller.commands;

import damncooleditorhackers.controller.EditorController;
import damncooleditorhackers.model.ElementTree;
import damncooleditorhackers.model.ModelController;
import damncooleditorhackers.model.grammarElements.CopyHandler;
import damncooleditorhackers.model.grammarElements.Element;
import damncooleditorhackers.model.grammarElements.ORElement;
import damncooleditorhackers.model.grammarElements.Type;
import damncooleditorhackers.view.view_elements.ElementButton;

import java.util.ArrayList;

/**
 * Created by Mark-Desktop on 14.07.2016.
 */
public class PasteCommand extends Command {
    Element copy;

    public PasteCommand(Element position, ElementTree rule, ElementButton button, Element copy) {
        super(position, rule, button);
        this.copy = copy;
    }

    public PasteCommand(String id, ElementTree rule, Element copy) {
        super(id, rule);
        this.copy = copy;
        exchangeElement();
    }


    boolean execute(ModelController model, EditorController controller) {
        if (selected == null || copy == null) return false;
        Element toInsert = CopyHandler.handle(copy);
        ArrayList<Element> test = getPossiblePos(selected);
        boolean ready = false;
        for (Element pos : test) {
            Element tmp = toInsert;
            boolean last = false;
            while (!last) {
                if (tmp.getChildren().size() != 1) last = true;
                ready = tryInsert(pos, tmp);
                if (ready) break;
                if (!last) tmp = tmp.getChildren().get(0);
            }
            if (ready) break;
        }
        return true;
    }

    private boolean tryInsert(Element pos, Element cop) {
        if (pos.getType() == Type.NFOLD) {
            if (pos.sameStruct(cop)) {//you can combine a NFOLD with same structure
                int addIndex = pos.getChildren().size() - 1;
                for (int i = 0; i < pos.getChildren().size(); i++) {
                    if (selected.hasParent(pos.getChildren().get(i))) addIndex = i;
                }
                for (Element newChild : cop.getChildren()) {
                    pos.getChildren().add(addIndex + 1, newChild);
                    newChild.setParent(pos);
                    newChild.updateID();
                }
                return true;
            } else {
                if (pos.getChildren().get(0).sameStruct(cop)) {
                    int addIndex = pos.getChildren().size() - 1;
                    for (int i = 0; i < pos.getChildren().size(); i++) {
                        if (selected.hasParent(pos.getChildren().get(i))) addIndex = i;
                    }
                    pos.getChildren().add(addIndex + 1, cop);
                    cop.setParent(pos);
                    cop.updateID();
                }
            }
      /*  } else if (pos.getType() == Type.OPTIONAL) { //optional was withoptional
            if (((WithOptionalElement) pos).getOptional().sameStruct(cop)) {
                ((WithOptionalElement) pos).setOptionalElement(cop);
                return true;
            }*/
        } else if (pos.getType() == Type.OR) {
            for (int i = 0; i < ((ORElement) pos).getChoices().size(); i++) {
                if (((ORElement) pos).getChoices().get(i).sameStruct(cop)) {
                    ((ORElement) pos).getChoices().remove(i);
                    ((ORElement) pos).getChoices().add(i, cop);
                    ((ORElement) pos).setRule(cop);
                    cop.setParent(pos);
                    cop.updateID();
                }
            }
        }
        return false;
    }

    private ArrayList<Element> getPossiblePos(Element position) {
        ArrayList<Element> tmp = new ArrayList<Element>();
        Element cur = position;
        while (cur.getParent() != null &&
                ((cur.getParent().getChildren().indexOf(cur) == cur.getParent().getChildren().size() - 1) ||
                        ((cur.getParent().getType() == Type.NFOLD)))) {
            if (cur.getType() == Type.NFOLD || (cur.getType() == Type.OPTIONAL && cur.getChildren().size() == 1) ||
                    (cur.getType() == Type.OR && cur.getChildren().size() == 0)) { //Optional was Withoptional
                tmp.add(cur);
            }
            cur = cur.getParent();
        }
        if (cur.getParent() != null && (cur.getParent().getChildren().indexOf(cur) != cur.getParent().getChildren().size() - 1)) {
            int currendIndex = cur.getParent().getChildren().indexOf(cur);
            tmp.addAll(getAllPos(cur.getParent().getChildren().get(currendIndex + 1)));
        }
        return tmp;
    }

    private ArrayList<Element> getAllPos(Element pos) {
        ArrayList<Element> tmp = new ArrayList<Element>();
        Element cur = pos;
        while (cur.getChildren().size() > 0) {
            if (cur.getType() == Type.NFOLD)
                tmp.add(cur);
            cur = cur.getChildren().get(0);
        }
        return tmp;
    }
}
