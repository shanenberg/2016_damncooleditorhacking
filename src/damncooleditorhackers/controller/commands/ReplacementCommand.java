package damncooleditorhackers.controller.commands;

import damncooleditorhackers.controller.EditorController;
import damncooleditorhackers.controller.commands.helper.ImExHelper;
import damncooleditorhackers.model.ElementTree;
import damncooleditorhackers.model.ModelController;
import damncooleditorhackers.model.grammarElements.*;
import damncooleditorhackers.view.view_elements.ElementButton;

import java.util.ArrayList;

/**
 * Created by Mark-Desktop on 01.07.2016.
 */
public class ReplacementCommand extends Command {
    private ArrayList<Element> myElements;
    private ArrayList<ArrayList<Integer>> oldIDs;

    public ReplacementCommand(ArrayList<Element> myElements, ElementTree rule, ElementButton button) {
        super(button.getElement(), rule, button);
        ArrayList<Element> selected = new ArrayList<Element>();
        for (Element e : myElements) if (e.getType() != Type.ADD_RIGHT) selected.add(e);
        this.myElements = selected;
        oldIDs = new ArrayList<ArrayList<Integer>>();
        saveAllIDs();
    }

    public ReplacementCommand(String ids, ElementTree rule) {
        super(ids.split(";")[0], rule);
        oldIDs = new ArrayList<ArrayList<Integer>>();
        String[] myIDs = ids.split(";");
        for (String s : myIDs) oldIDs.add(ImExHelper.StringToID(s));
        exchangeElement();
    }

    public boolean execute(ModelController model, EditorController controller) {
        myElements = cleanUp(myElements);
        if (myElements.size() < 1) return false;
        if (!loaded) {
            if (getOriginalButton().isShowing()) setLastButtonLocation(getOriginalButton().getLocationOnScreen());
            model.setLastButtonLocation(getLastButtonLocation());
        }
        model.getSelectionList().clear();
        ArrayList<ReplacementElement> toAdd = getReplacementElements();
        if (toAdd == null || toAdd.size() == 0) return false;
        model.getSelectionList().addAll(toAdd);
        if (myElements.size() == 1 && model.getSelectionList().size() == 1)
            return new SelectionCommand("0", elementTree).execute(model, controller);
        return true;
    }


    private void saveAllIDs() {
        super.saveID();
        oldIDs.clear();
        for (Element e : myElements) {
            ArrayList<Integer> ID = new ArrayList<Integer>();
            ID.addAll(e.getIDs());
            oldIDs.add(ID);
        }
    }

    private ArrayList<ReplacementElement> getReplacementElements() {
        Element parent = findParentOfAll(myElements);
        if (parent == null) return null;
        ArrayList<ReplacementElement> tmp = new ArrayList<ReplacementElement>();
        if (myElements.size() == 1 && myElements.get(0).getType() == Type.TOKEN && ((TokenElement) myElements.get(0)).isJavaStringLiteral())
            tmp.add(handleTOKEN(myElements.get(0)));
        while (parent.getParent() != null && parent.getParent().getChildren().size() == 1) { //still right outer child
            if (parent.getType() == Type.OR && ((ORElement) parent).getChoice() != -1)
                tmp.addAll(handleOR(parent));
            parent = parent.getParent();
        }
        if (parent.getType() == Type.OR && ((ORElement) parent).getChoice() != -1)
            tmp.addAll(handleOR(parent));
        return tmp;
    }

    private ArrayList<ReplacementElement> handleOR(Element parent) {
        ArrayList<ReplacementElement> temp = new ArrayList<ReplacementElement>();
        for (Element e : ((ORElement) parent).getChoices()) {
            if (parent.getChildren().get(0) != e) {
                ReplacementElement tmp = new ReplacementElement(ReplacementType.OR, parent, e);
                tmp.setText(tmp.getText());
                temp.add(tmp);
            }
        }
        return temp;
    }

    private ReplacementElement handleTOKEN(Element token) {
        ReplacementElement tmp = new ReplacementElement(ReplacementType.TOKEN, token, token);
        tmp.setButton(clickedButton);
        tmp.setText("Change Text");
        return tmp;
    }

    @Override
    public void exchangeElement() {
        super.exchangeElement();
        ArrayList<Element> tmp = new ArrayList<Element>();
        for (int i = 0; i < oldIDs.size(); i++) {
            tmp.add(getEqualElement(oldIDs.get(i), elementTree.getRoot(), 0));
        }
        myElements = tmp;
    }

    @Override
    public String export(ImExHelper helper) {
        String tmp = "";
        String classname = getClass().getName();
        String[] name = classname.split("\\.");
        tmp += name[name.length - 1];
        tmp += ",";
        for (int i = 0; i < oldIDs.size(); i++)
            tmp += ImExHelper.IDToString(oldIDs.get(0)) + (i < oldIDs.size() - 1 ? ";" : "");
        tmp += "\n";
        return tmp;
    }

}
