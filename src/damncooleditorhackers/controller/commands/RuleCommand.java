package damncooleditorhackers.controller.commands;

import damncooleditorhackers.controller.EditorController;
import damncooleditorhackers.model.ElementTree;
import damncooleditorhackers.model.ModelController;
import damncooleditorhackers.model.grammarElements.ORElement;
import damncooleditorhackers.model.grammarElements.RuleElement;
import damncooleditorhackers.model.grammarElements.Type;
import damncooleditorhackers.view.view_elements.ElementButton;

/**
 * Created by Nick on 21.06.2016.
 */
public class RuleCommand extends Command {

    public RuleCommand(RuleElement selected, ElementTree rule, ElementButton button) {
        super(selected, rule, button);
    }

    public RuleCommand(String id, ElementTree rule) {
        super(id, rule);
        exchangeElement();
    }


    public boolean execute(ModelController model, EditorController controller) {
        super.saveID();
        if (!loaded) {
            if (getOriginalButton().isShowing()) setLastButtonLocation(getOriginalButton().getLocationOnScreen());
            model.setLastButtonLocation(getLastButtonLocation());
        }
        model.getSelectionList().clear();
        if (selected.getChildren().size() > 0) {
            if (selected.getChildren().get(0).getType() == Type.OR)
                model.getSelectionList().addAll(((ORElement) selected.getChildren().get(0)).getChoices());
            else model.getSelectionList().addAll(selected.getChildren());
        }

        return true;
    }
}
