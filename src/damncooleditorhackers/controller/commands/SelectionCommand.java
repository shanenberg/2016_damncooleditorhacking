package damncooleditorhackers.controller.commands;

import damncooleditorhackers.controller.EditorController;
import damncooleditorhackers.controller.commands.helper.ImExHelper;
import damncooleditorhackers.model.ElementTree;
import damncooleditorhackers.model.ModelController;
import damncooleditorhackers.model.grammarElements.*;
import damncooleditorhackers.view.view_elements.ElementButton;

/**
 * Created by Nick on 21.06.2016.
 */
public class SelectionCommand extends Command {
    private ModelController model;
    private int choice;

    public SelectionCommand(Element selected, ElementTree rule, ElementButton button) {
        super(selected, rule, button);
    }

    public SelectionCommand(String choice, ElementTree rule) {
        super("1", rule);
        this.choice = new Integer(choice);
    }

    public boolean execute(ModelController model, EditorController controller) {
        super.saveID();
        this.model = model;
        if (loaded) setElement(model.getSelectionList().get(choice));
        else choice = model.getSelectionList().indexOf(selected);
        if (selected.getOperator() == Op.OR) {
            ((ORElement) selected.getParent()).setRule(selected);
            if (selected.getParent().getLeaves().size() == 1 && selected.getParent().getLeaves().get(0).getType().equals(Type.TOKEN)) {
                Element tmp = selected.getParent();
                while (tmp != null && !tmp.getType().equals(Type.NFOLD)) tmp = tmp.getParent();
                if (tmp != null) {
                    NFoldElement myNfold = (NFoldElement) tmp;
                    if (myNfold.isComplete() && myNfold.isAutoAdd()) {
                        myNfold.setSelectionFromChild(null);
                        myNfold.traverseChildren();
                    }
                }
            }
        } else if (selected.getType() == Type.NFOLD) {
            selected.traverseChildren();
        } else if (selected.getType() == Type.REPLACEMENT) {
            if (((ReplacementElement) selected).getReplacementType() == ReplacementType.DELETEOPTIONAL) {
                ((OptionalElement) selected.getParent()).setVisible(false);
            } else if (((ReplacementElement) selected).getReplacementType() == ReplacementType.OR) {
                replaceOR((ReplacementElement) selected);
            } else if (((ReplacementElement) selected).getReplacementType() == ReplacementType.TOKEN) {
                ((ReplacementElement) selected).getButton().getParentPanel().showInputField();
                model.getSelectionList().clear();
                return false;
            }
        } else if (selected.getType() == Type.OPCONTAINER) {
            ((OptionalContainer) selected).getOriginal().setActive(true);
        } else if (selected.getType() == Type.OPTIONAL) {
            ((OptionalElement) selected).setActive(true);
        }
        model.getSelectionList().clear();
        return true;
    }

    private void replaceOR(ReplacementElement sel) {
        ((ORElement) sel.getParent()).setRule(sel.getChildren().get(0));
    }

    @Override
    public void exchangeElement() {
        if (!loaded) setElement(model.getSelectionList().get(choice));
    }

    @Override
    public String export(ImExHelper helper) {
        String tmp = "";
        String classname = getClass().getName();
        String[] name = classname.split("\\.");
        tmp += name[name.length - 1];
        tmp += ",";
        tmp += choice;
        tmp += "\n";
        return tmp;
    }
}
