package damncooleditorhackers.controller.commands;

import damncooleditorhackers.controller.EditorController;
import damncooleditorhackers.controller.commands.helper.ImExHelper;
import damncooleditorhackers.model.ElementTree;
import damncooleditorhackers.model.ModelController;
import damncooleditorhackers.model.grammarElements.TokenElement;
import damncooleditorhackers.view.ViewUpdater;
import damncooleditorhackers.view.view_elements.ElementButton;

/**
 * Created by Nick on 21.06.2016.
 */
public class TokenCommand extends Command {
    private String textWas;

    public TokenCommand(String before, ViewUpdater view, ElementTree rule, ElementButton button) {
        super(button.getElement(), rule, button);
        this.textWas = before;
    }

    public TokenCommand(String id, String textWas, ElementTree rule, ViewUpdater view) {
        super(id, rule);
        this.textWas = textWas;
        exchangeElement();
    }

    public boolean execute(ModelController model, EditorController controller) {
        super.saveID();
        if (!loaded) {
            if (getOriginalButton().isShowing()) setLastButtonLocation(getOriginalButton().getLocationOnScreen());
            model.setLastButtonLocation(getLastButtonLocation());
        }
        TokenElement temp = (TokenElement) selected;
        if (!loaded && textWas.equals(temp.getText())) return false;
        temp.doSet();
        textWas = temp.getText();
        return true;
    }

    @Override
    public void exchangeElement() {
        TokenElement thisElement = (TokenElement) getEqualElement(originalID, elementTree.getRoot(), 0);
        thisElement.doSet();
        if (selected != null && ((TokenElement) selected).isSet()) thisElement.doSet();
        thisElement.setText(textWas);
        setElement(thisElement);
    }

    @Override
    public String export(ImExHelper helper) {
        String tmp = "";
        String classname = getClass().getName();
        String[] name = classname.split("\\.");
        tmp += name[name.length - 1];
        tmp += ",";
        tmp += ImExHelper.IDToString(originalID);
        tmp += "," + textWas;
        tmp += "\n";
        return tmp;
    }
}
