package damncooleditorhackers.controller.commands.helper;

import damncooleditorhackers.controller.commands.Command;
import damncooleditorhackers.model.ElementTree;
import damncooleditorhackers.model.grammarElements.*;

import java.util.ArrayList;

/**
 * Created by Mark-Desktop on 21.09.2016.
 */
public class DeletionHelper {
    public void prepareDelete(Element parent, ArrayList<Element> toBeDeleted, ElementTree rule) {
        allFIX(rule.getRoot());
        ArrayList<Element> toDelete;
        if (toBeDeleted.size() > 0) {
            toDelete = getRealSelected(parent, toBeDeleted, rule);
            if (toDelete.size() == 0) toDelete = toBeDeleted;
            for (Element e : toDelete)
                markDeletion(e, rule.getRoot(), true);
            ArrayList<Element> allE = rule.getElements();
            if (allE.indexOf(toDelete.get(0)) > 0)
                markDeletion(allE.get(allE.indexOf(toDelete.get(0)) - 1), rule.getRoot(), false);
            if (allE.indexOf(toDelete.get(toDelete.size() - 1)) < allE.size() - 1)
                markDeletion(allE.get(allE.indexOf(toDelete.get(toDelete.size() - 1)) + 1), rule.getRoot(), false);
        }
    }

    private ArrayList<Element> getRealSelected(Element parent, ArrayList<Element> toBeDeleted, ElementTree rule) {
        ArrayList<Element> tmp = new ArrayList<Element>();
        Element realP = Command.findParentOfAll(toBeDeleted);
        for (Element e : toBeDeleted) tmp.add(e);
        if (realP != null && realP.getType() == Type.NFOLD) {
            ArrayList<Element> allE = rule.getElements();
            Element leftBeforeParent = tmp.get(tmp.size() - 1);
            while (leftBeforeParent.getParent() != parent) leftBeforeParent = leftBeforeParent.getParent();
            int index = allE.indexOf(tmp.get(tmp.size() - 1)) + 1;
            while (index < allE.size() && allE.get(index).hasParent(leftBeforeParent))
                tmp.add(allE.get(index++));
            Element rightBeforeParent = tmp.get(tmp.size() - 1);
            while (rightBeforeParent.getParent() != parent) rightBeforeParent = rightBeforeParent.getParent();
            while (index >= 0 && index < allE.size() && allE.get(index).hasParent(rightBeforeParent))
                tmp.add(0, allE.get(index--));
        } else {
            tmp.clear();
            ArrayList<Element> allE = rule.getElements();
            for (Element e : allE) if (e.hasParent(parent)) tmp.add(e);
        }
        return tmp;
    }

    private void markDeletion(Element child, Element parent, boolean type) {
        Element tmp = child;
        while (tmp != null && tmp != parent) {
            tmp.setToBeDeleted(type);
            tmp = tmp.getParent();
        }
    }

    public void delete(Element e) {
        if (e.isToBeDeleted()) deleteFromParent(e);
        else {
            for (int i = e.getChildren().size() - 1; i >= 0; i--)
                if (e.getChildren().get(i).isToBeDeleted())
                    deleteFromParent(e.getChildren().get(i));
                else
                    delete(e.getChildren().get(i));
            if (e.getChildren().size() == 0) e.traverseChildren();
        }
    }

    private void deleteFromParent(Element e) {
        if (e.getParent() instanceof NFoldElement) {
            int pos = e.getParent().getChildren().indexOf(e);
            e.getParent().getChildren().remove(e);
            for (int i = pos; i < e.getParent().getChildren().size(); i++)
                e.getParent().getChildren().get(i).updateID();
            if (e.getParent().getChildren().size() == 0) e.traverseChildren();
            ArrayList<Element> leaves = e.getParent().getLeaves();
            boolean autoAdd = false;
            for (Element ele : leaves) if (ele.getType() != Type.TOKEN) autoAdd = true;
            ((NFoldElement) e.getParent()).setAutoAdd(autoAdd);
        } else if (e.getParent() instanceof OptionalElement) {
            OptionalElement tmp = (OptionalElement) e.getParent();
            tmp.getChildren().clear();
            tmp.setActive(false);
            tmp.setTraversed(false);
            tmp.setVisible(false);
            tmp.traverseChildren();
        } else if (e.getParent() instanceof ORElement) ((ORElement) e.getParent()).setRule(null);
        else {
            int savePos = e.getParent().getChildren().indexOf(e);
            e.getParent().getChildren().remove(e);
            e.getParent().addChildByIndex(savePos);
            e.getParent().getChildren().get(savePos).updateID();
            if (e.getParent().getChildren().get(savePos).getType() == Type.OPTIONAL)
                ((OptionalElement) e.getParent().getChildren().get(savePos)).setVisible(false);
        }
    }

    public void setDeletion(boolean type, Element parent) {
        parent.setToBeDeleted(type);
        for (Element e : parent.getChildren()) setDeletion(type, e);
    }

    private void allFIX(Element root) {
        root.setToBeDeleted(false);
        for (Element e : root.getChildren()) {
            allFIX(e);
        }
    }
}
