package damncooleditorhackers.controller.commands.helper;

import damncooleditorhackers.EditorGrammar;
import damncooleditorhackers.model.ElementTree;
import damncooleditorhackers.model.grammarElements.Element;
import damncooleditorhackers.model.grammarElements.OptionalElement;
import damncooleditorhackers.model.grammarElements.Type;

import java.io.File;
import java.io.FileWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

/**
 * Created by Mark-Desktop on 21.09.2016.
 */
public class ImExHelper {
    private String timestamp;
    private String currentgrammar;

    public ImExHelper(EditorGrammar currentgrammar) {
        String timeStamp = new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss").format(new Date());
        String[] tmp = timeStamp.split("\\.");
        timestamp = "";
        this.currentgrammar = currentgrammar.toString();
        for (String str : tmp) this.timestamp += str;
    }

    public static String IDToString(ArrayList<Integer> id) {
        String tmp = "";
        for (int i = 0; i < id.size(); i++) {
            tmp += id.get(i);
            if (i != id.size() - 1) tmp += ".";
        }
        return tmp;
    }

    public static ArrayList<Integer> StringToID(String sid) {
        ArrayList<Integer> id = new ArrayList<Integer>();
        String[] tmp = sid.split("\\.");
        for (String str : tmp)
            id.add(new Integer(str));
        return id;
    }

    public void writeToFile(String str, String username) {
        File dir = new File("logs");
        dir.mkdir();
        File user = new File("logs/" + username);
        user.mkdir();
        File grammar = new File("export/" + username + "/" + currentgrammar);
        grammar.mkdir();
        String filename = "logs/" + username + "/" + currentgrammar + "/eventlog-" + username + "-" + timestamp + ".txt";
        File file = new File(filename);

        try {
            FileWriter writer = new FileWriter(file, true);
            writer.write(str);
            writer.flush();
            writer.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void exportGrammer(ArrayList<Element> elements, String username, EditorGrammar currentgrammar) {
        File dir = new File("export");
        dir.mkdir();
        File user = new File("export/" + username);
        user.mkdir();
        File grammar = new File("export/" + username + "/" + currentgrammar.toString());
        grammar.mkdir();
        String filename = "export/" + username + "/" + currentgrammar + "/export-" + username + "-" + new SimpleDateFormat("yyyyMMddHHmmss").format(System.currentTimeMillis()) + ".txt";
        File file = new File(filename);
        try {
            FileWriter writer = new FileWriter(file, true);
            for (Element e : elements) {
                if (!(e.getType() == Type.OPTIONAL && !((OptionalElement) e).isActive())) {
                    if (e.getType() == Type.TOKEN) {
                        String str = e.getText();
                        writer.write(str);
                    } else throw (new ExportException("Can't export. Resolve to only Tokens"));
                }
            }
            writer.flush();
            writer.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void clearLog(String username) {
        File dir = new File("logs");
        dir.mkdir();
        File user = new File("logs/" + username);
        user.mkdir();
        File grammar = new File("export/" + username + "/" + currentgrammar);
        grammar.mkdir();
        String filename = "logs/" + username + "/" + currentgrammar + "/eventlog-" + username + "-" + timestamp + ".txt";
        File file = new File(filename);
        try {
            FileWriter writer = new FileWriter(file);
            writer.write("");
            writer.flush();
            writer.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public static String grammarToText(ElementTree rule) {
        ArrayList<Element> elements = rule.getElements();
        String str = "";
        for (Element e : elements)
            if (!(e.getType() == Type.OPTIONAL && !((OptionalElement) e).isActive())) {
                if (e.getType() == Type.TOKEN)
                    str += e.getText();
                else return "";
            }
        return str;
    }
}


class ExportException extends Exception {
    ExportException(String str) {
        super(str);
    }
}
