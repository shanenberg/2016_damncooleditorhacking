package damncooleditorhackers.controller.commands.helper;

import damncooleditorhackers.controller.commands.*;
import damncooleditorhackers.controller.external.FileNameExtensionFilter;
import damncooleditorhackers.model.ElementTree;
import damncooleditorhackers.view.ViewUpdater;

import javax.swing.*;
import javax.swing.filechooser.FileFilter;
import javax.swing.filechooser.FileSystemView;
import java.io.*;


/**
 * Created by Mark-Desktop on 22.09.2016.
 */
public final class LoadingHelper {
    private ViewUpdater view;
    private ElementTree rule;
    private CommandController controller;

    public LoadingHelper(ViewUpdater view, ElementTree rule, CommandController controller) {
        this.view = view;
        this.rule = rule;
        this.controller = controller;
    }

    public Command handle(String line) {
        if (line == null || line.equals("") || line.equals("\n")) return null;
            String[] split = line.split(",");
            if (split[0].equals("AddRightCommand")) return new AddRightCommand(split[1], rule);
            else if (split[0].equals("AssumeCommand")) return new AssumeCommand(split[1], rule);
            else if (split[0].equals("CopyCommand")) return new CopyCommand(split[1], rule);
            else if (split[0].equals("CutCommand")) return new CutCommand(split[1], rule);
            else if (split[0].equals("DeleteCommand")) return new DeleteCommand(split[1], rule, split[2]);
            else if (split[0].equals("DuplicateCommand")) return new DuplicateCommand(split[1], rule);
            else if (split[0].equals("NFOLDCommand")) return new NFOLDCommand(split[1], rule, view);
            else if (split[0].equals("OptionalCommand")) return new OptionalCommand(split[1], rule);
            else if (split[0].equals("ORCommand")) return new ORCommand(split[1], rule);
            else if (split[0].equals("PasteCommand")) return new PasteCommand(split[1], rule, null);
            else if (split[0].equals("ReplacementCommand")) return new ReplacementCommand(split[1], rule);
            else if (split[0].equals("RuleCommand")) return new RuleCommand(split[1], rule);
            else if (split[0].equals("SelectionCommand")) return new SelectionCommand(split[1], rule);
            else if (split[0].equals("TokenCommand")) return new TokenCommand(split[1], split[2], rule, view);
        return null;
    }


    public void displayLoadChooser(JFrame frame) {
        JFileChooser chooser = new JFileChooser();
        chooser.setCurrentDirectory(FileSystemView.getFileSystemView().createFileObject(System.getProperty("user.dir") + "\\logs"));
        FileFilter filter = new FileNameExtensionFilter("*.txt", "txt");
        chooser.setFileFilter(filter);
        if (chooser.showOpenDialog(frame) == JFileChooser.APPROVE_OPTION) {
            try {
                FileInputStream fIn = new FileInputStream(chooser.getSelectedFile());
                BufferedReader reader = new BufferedReader(new InputStreamReader(fIn));
                System.out.println("Reading File");
                String line = reader.readLine();
                int i = 0;
                try {
                    controller.doCommand(handle(line));
                } catch (Exception e) {
                    System.out.println("Exception bei Zeile: " + i + "\n");
                    e.printStackTrace();
                }
                while (line != null) {
                    i++;
                    line = reader.readLine();
                    try {
                        controller.doCommand(handle(line));
                    } catch (Exception e) {
                        System.out.println("Exception bei Zeile: " + i + "\n");
                        e.printStackTrace();
                    }

                }
            } catch (FileNotFoundException e) {
                System.out.println("File not found");
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
