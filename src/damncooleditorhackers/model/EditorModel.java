package damncooleditorhackers.model;

import damncooleditorhackers.EditorGrammar;
import damncooleditorhackers.controller.ViewController;
import damncooleditorhackers.model.grammarElements.Element;

import java.awt.*;
import java.util.ArrayList;

/**
 * Created by Liron on 15.06.2016.
 */
public class EditorModel implements ModelController {
    private ArrayList<Element> selectionList;
    private Point lastButtonLocation;
    private ElementTree elementTree;
    private ViewController controller;

    public EditorModel(ViewController controller) {
        this.controller = controller;
    }

    public void resetGrammar(EditorGrammar startGrammar) {
        selectionList = new ArrayList<Element>();
        elementTree = new ElementTree(this, startGrammar);
    }

    public void setCommandController(ElementTree myRule) {
        controller.setController(myRule, this);
    }

    public ArrayList<Element> getSelectionList() {
        return selectionList;
    }

    public void setLastButtonLocation(Point lastButtonLocation) {
        this.lastButtonLocation = lastButtonLocation;
    }

    public Point getLastButtonLocation() {
        return lastButtonLocation;
    }

    public ArrayList<Element> getElements() {
        return elementTree.getElements();
    }
}
