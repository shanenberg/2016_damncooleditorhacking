package damncooleditorhackers.model;

import damncooleditorhackers.EditorGrammar;
import damncooleditorhackers.model.grammarElements.*;
import slope.Rule;

import java.util.ArrayList;

/**
 * Created by Mark-Desktop on 30.06.2016.
 */
public class ElementTree {
    private final EditorGrammar startGrammar;
    private Element root;

    ElementTree(EditorModel model, EditorGrammar startGrammar) {
        this.startGrammar = startGrammar;
        reset();
        model.setCommandController(this);
    }

    public void reset() {
        StartGrammar start = new StartGrammar();
        Rule slopeGrammar = start.getStart(startGrammar);
        root = new RuleElement(slopeGrammar, Op.NONE, null);
    }


    public ArrayList<Element> getElements() {
        ArrayList<Element> temp = new ArrayList<Element>();
        traverse(temp, root);
        return temp;
    }

    private void traverse(ArrayList<Element> list, Element root) {
        if (root.getChildren().size() == 0 || root.getChildren().size() == 1 && root.getChildren().get(0).getType() == Type.OR && ((ORElement) root.getChildren().get(0)).getChoice() == -1) {//
            list.add(root);
        } else for (Element e : root.getChildren()) traverse(list, e);
    }

    public Element getRoot() {
        return root;
    }

    public void setRoot(Element root) {
        this.root = root;
    }
}
