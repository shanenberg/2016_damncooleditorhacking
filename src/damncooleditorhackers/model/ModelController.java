package damncooleditorhackers.model;

import damncooleditorhackers.EditorGrammar;
import damncooleditorhackers.model.grammarElements.Element;

import java.awt.*;
import java.util.ArrayList;

/**
 * Created by Thorben on 16.06.2016.
 */
public interface ModelController {

    ArrayList<Element> getSelectionList();

    void resetGrammar(EditorGrammar startGrammar);

    void setLastButtonLocation(Point lastButtonLocation);

    Point getLastButtonLocation();

    ArrayList<Element> getElements();
}
