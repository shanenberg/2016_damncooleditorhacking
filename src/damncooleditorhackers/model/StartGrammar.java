package damncooleditorhackers.model;

import damncooleditorhackers.EditorGrammar;
import damncooleditorhackers.model.slopeGrammars.*;
import slope.Rule;

/**
 * Created by Thorben on 20.06.2016.
 */
class StartGrammar {

    public Rule getStart(EditorGrammar startGrammar) {
        switch (startGrammar) {
            case Kassenzettel:
                GrammarKassenzettel tokens_kassenzettel = new GrammarKassenzettel();
                GrammarKassenzettel.Grammar01Parser grammarParser_kassenzettel = tokens_kassenzettel.new Grammar01Parser();
                return grammarParser_kassenzettel.Grammar();
            case Website:
                GrammarWebsite tokens_website = new GrammarWebsite();
                GrammarWebsite.Grammar01Parser grammarParser_website = tokens_website.new Grammar01Parser();
                return grammarParser_website.Grammar();
            case Testgrammatik_DACH:
                GrammarTestDACH tokens_DACH = new GrammarTestDACH();
                GrammarTestDACH.Grammar01Parser grammarParser_DACH = tokens_DACH.new Grammar01Parser();
                return grammarParser_DACH.Grammar();
            case Testgrammatik_Text:
                GrammarTestText tokens_Text = new GrammarTestText();
                GrammarTestText.Grammar01Parser grammarParser_Text = tokens_Text.new Grammar01Parser();
                return grammarParser_Text.Grammar();
            case Example:
                GrammarExample tokens_Example = new GrammarExample();
                GrammarExample.Grammar01Parser grammarParser_Example = tokens_Example.new Grammar01Parser();
                return grammarParser_Example.Grammar();
            default:

        }
        return null;
    }

}
