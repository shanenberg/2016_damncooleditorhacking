package damncooleditorhackers.model.grammarElements;

import slope.ParserConstructor_N;

import java.util.ArrayList;

/**
 * Created by Nick on 15.06.2016.
 */
public class ANDElement extends Element {
    private ParserConstructor_N object;

    public ANDElement(ParserConstructor_N object, Op operator, Element parent) {
        super(operator, parent);
        this.object = object;
        traverseChildren();
    }

    public ANDElement(ANDElement e) {
        super(e);
        this.object = e.getObject();
    }

    public void addChildByIndex(int index) {
        if (index >= 0 && index < object.pcs.length) {
            Element e = createChildElement(object.pcs[index], Op.AND, this);
            children.add(index, e);
        }
    }

    public void traverseChildren() {
        for (int i = 0; i < object.pcs.length; i++)
            addChildren(createChildElement(object.pcs[i], Op.AND, this));
        String text = "";
        for (Element e : getChildren()) text += e.getText() + " ";
        if (!text.equals("")) setText(text.substring(0, text.length() - 1));
        if (hasParent() && getChildren().size() == 1) {
            getParent().removeEmptyAND(this, getOperator());
        }
    }

    public ParserConstructor_N getObject() {
        return object;
    }

    public Type getType() {
        return Type.AND;
    }

    public boolean isSingleToken(int depth) {
        return children.size() == 1 && children.get(1).isSingleToken(depth);
    }

    public boolean sameStruct(Element other) {
        if (other instanceof ANDElement) {
            ANDElement e = (ANDElement) other;
            ArrayList<Element> childs = new ArrayList<Element>();
            ArrayList<Element> childsE = new ArrayList<Element>();
            if (this.getChildren().size() == 0) {
                for (int i = 0; i < object.pcs.length; i++)
                    childs.add(createChildElement(object.pcs[i], getOperator(), this));
                this.children = childs;
            }
            if (e.getChildren().size() == 0) {
                for (int i = 0; i < e.getObject().pcs.length; i++)
                    childsE.add(createChildElement(e.getObject().pcs[i], e.getOperator(), e));
                e.children = childsE;
            }
            boolean result = false;
            if (e.getChildren().size() == this.getChildren().size()) {
                for (int i = 0; i < e.getChildren().size(); i++)
                    if (!this.getChildren().get(i).sameStruct(e.getChildren().get(i))) {
                        if (childs.size() > 0) this.children = new ArrayList<Element>();
                        if (childsE.size() > 0) e.children = new ArrayList<Element>();
                        return false;
                    }
                result = true;
            }
            if (childs.size() > 0) this.children = new ArrayList<Element>();
            if (childsE.size() > 0) e.children = new ArrayList<Element>();
            return result;
        }
        return false;
    }

    @Override
    public int isOuterChild(Element child) {
        int index = children.indexOf(child);
        if (index == children.size() - 1) return 1;
        for (int i = index + 1; i < children.size(); i++) {
            if (children.get(i).getType() != Type.OPTIONAL || (children.get(i).getType() == Type.OPTIONAL && ((OptionalElement) children.get(i)).isActive()))
                return 0;
        }
        return 2;
    }
}
