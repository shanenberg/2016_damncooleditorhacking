package damncooleditorhackers.model.grammarElements;

import slope.PC;

import java.util.ArrayList;


/**
 * Created by Nick on 28.06.2016.
 */
public class AddRightElement extends Element {
    private ArrayList<Element> optionals;
    private Element lastroot;

    public AddRightElement(Op operator, Element parent) {
        super(operator, parent);
        optionals = new ArrayList<Element>();
        lastroot = null;
        setText(parent.getText());
    }

    public void traverseChildren() {
    }

    public void addChildByIndex(int index) {
    }

    public ArrayList<Element> getOptionals(Element element) {
        if (lastroot != element) findOptionals(element, null);
        return optionals;
    }

    private ArrayList<Element> findOptionals(Element element, Element child) {
        ArrayList<Element> alternatives = new ArrayList<Element>();
        if (element == null) return alternatives;
        if (element.getType() == Type.NFOLD) alternatives.add(element);
        if (child == null || element.isOuterChild(child) > 0)
            alternatives.addAll(findOptionals(element.getParent(), element));
        optionals = alternatives;
        return alternatives;
    }

    public boolean hasOptionals(Element element) {
        return getOptionals(element).size() > 0;
    }

    public PC getObject() {
        return null;
    }

    public Type getType() {
        return Type.ADD_RIGHT;
    }

    public boolean isSingleToken(int depth) {
        return false;
    }

    public void add(Element e) {
        children.add(e);
    }

    public boolean sameStruct(Element other) {
        return false;
    }
}
