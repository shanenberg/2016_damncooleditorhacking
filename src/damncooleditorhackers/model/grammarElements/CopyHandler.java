package damncooleditorhackers.model.grammarElements;

import java.lang.reflect.Method;

/**
 * Created by marka on 10.07.2016.
 */
public class CopyHandler {

    public static Element handle(Element e) {
        try {
            Method handler;
            handler = CopyHandler.class.getMethod("handle", e.getClass());
            return (Element) handler.invoke(null, e);
        } catch (Exception ex) {
            System.out.println(e.getClass());
            throw new RuntimeException(ex);
        }
    }

    public static NFoldElement handle(NFoldElement e) {
        return new NFoldElement(e);
    }

    public static ANDElement handle(ANDElement e) {
        return new ANDElement(e);
    }

    public static ORElement handle(ORElement e) {
        return new ORElement(e);
    }

    public static OptionalElement handle(OptionalElement e) {
        return new OptionalElement(e);
    }

    public static RuleElement handle(RuleElement e) {
        return new RuleElement(e);
    }

    public static TokenElement handle(TokenElement e) {
        return new TokenElement(e);
    }


}
