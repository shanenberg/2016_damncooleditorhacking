package damncooleditorhackers.model.grammarElements;

import slope.PC;

import javax.management.InstanceNotFoundException;
import java.util.ArrayList;

/**
 * Created by Nick on 15.06.2016.
 */
public abstract class Element {
    private String text;
    private ArrayList<Integer> ids = new ArrayList<Integer>();
    protected ArrayList<Element> children;
    private Op operator;
    private Element parent;
    private boolean selected;
    private boolean toBeDeleted;

    public Element(Op operator, Element parent) {
        this.operator = operator;
        this.parent = parent;
        children = new ArrayList<Element>();
        toBeDeleted = false;
        ids.add(1);
    }

    public Element(Element e) {
        if (e != null) {
            children = new ArrayList<Element>();
            text = e.getText();
            ids = new ArrayList<Integer>();
            for (Integer i : e.getIDs()) {
                ids.add(i.intValue());
            }
            parent = e.getParent();
            operator = e.getOperator();
            selected = e.isSelected();
            toBeDeleted = e.isToBeDeleted();
            for (Element child : e.children) {
                this.addChildren(getCopyOf(child));
            }
            for (Element child : this.children) {
                child.setParent(this);
            }
        }
    }

    public abstract boolean sameStruct(Element other);
    public abstract void traverseChildren();

    public abstract void addChildByIndex(int index);

    public abstract PC getObject();

    public abstract Type getType();

    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public abstract boolean isSingleToken(int depth);

    public void setID(ArrayList<Integer> nid) {
        ids.clear();
        ids.addAll(nid);
    }

    public ArrayList<Element> getChildren() {
        return children;
    }

    public void addChildren(Element child) {
        if (child != null && !(child.getType() == Type.AND && child.getChildren().size() <= 1)) {
            children.add(child);
            child.updateID();
        }
    }

    public void addChildren(int index, Element child) {
        if (child != null && !(child.getType() == Type.AND && child.getChildren().size() <= 1)) {
            children.add(index, child);
            child.updateID();
        }
    }

    public void updateID() {
        if (parent != null) {
            ArrayList<Integer> myNewID = new ArrayList<Integer>();
            myNewID.addAll(parent.getIDs());
            try {
                myNewID.add(parent.getChildIndex(this) + 1); //add personal ID
            } catch (InstanceNotFoundException e) {
                return;
            }
            this.setID(myNewID);
            for (Element child : children) child.updateID();
        }
    }

    protected int getChildIndex(Element myElement) throws InstanceNotFoundException {
        for (int i = 0; i < children.size(); i++)
            if (children.get(i) == myElement) return i;
        throw new InstanceNotFoundException("Not child of Element");
    }

    public ArrayList<Integer> getIDs() {
        return ids;
    }

    public Element createChildElement(PC object, Op operator, Element parent) {
        return ElementHandler.handle(object, operator, parent);
    }

    public Op getOperator() {
        return operator;
    }

    public void setOperator(Op operator) {
        this.operator = operator;
    }

    public Element getParent() {
        return parent;
    }

    public boolean hasParent() {
        return parent != null;
    }


    public void setParent(Element parent) {
        this.parent = parent;
    }

    public Element getCopyOf(Element e) {
        return CopyHandler.handle(e);
    }

    @Override
    public String toString() {
        return getText();
    }

    public boolean sameID(Element other) {
        if (other != null)
        return sameID(other.getIDs());
        return false;
    }

    public boolean sameID(ArrayList<Integer> ID) {
        if (this.ids == null || ID == null) return false;
        if (ID.size() != this.ids.size()) return false;
        for (int i = 0; i < ids.size(); i++) {
            if (!ids.get(i).equals(ID.get(i))) return false;
        }
        return true;
    }
    public boolean hasParent(Element e) {
        if (sameID(e)) return true;
        if (this.getParent() == null) return false;
        return this.parent.hasParent(e);
    }

    public Element findParent(Element second) {
        if (second == null || second.getIDs() == null) return this;
        if (this.sameID(second)) return this; //rekursion Abbruchbedingung
        if (this.getIDs().size() < second.getIDs().size()) {
            return findParent(second.getParent());
        } else {
            if (this.getIDs().size() > second.getIDs().size()) {
                return this.getParent().findParent(second);
            } else {
                return this.getParent().findParent(second.getParent()); //Beide sind auf derselben "Ebene" haben aber nicht dasselbe Parent
            }
        }
    }

    public boolean isToBeDeleted() {
        return toBeDeleted;
    }

    public void setToBeDeleted(boolean toBeDeleted) {
        this.toBeDeleted = toBeDeleted;
    }

    public void removeEmptyAND(Element andElement, Op operator) {
        getChildren().remove(andElement);
        Element child = andElement.getChildren().get(0);
        child.setParent(this);
        child.setOperator(operator);
        addChildren(child);
    }

    public ArrayList<Element> getLeaves() {
        ArrayList<Element> tmp = new ArrayList<Element>();
        if (getChildren().size() == 0) {
            tmp.add(this);
            return tmp;
        }
        for (Element e : getChildren()) tmp.addAll(e.getLeaves());
        return tmp;
    }

    public int isOuterChild(Element child) {
        return children.indexOf(child) == children.size() - 1 ? 1 : 0;
    }

    @Override
    public int hashCode() {
        return super.hashCode() + 31 * getText().hashCode();
    }
}
