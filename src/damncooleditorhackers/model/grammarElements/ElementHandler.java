package damncooleditorhackers.model.grammarElements;

import slope.PC;
import slope.ParserConstructor_1;
import slope.ParserConstructor_N;
import slope.Rule;
import slope.lang.AND.ANDParser;
import slope.lang.NFOLD.NFOLDParser;
import slope.lang.OPTIONAL.OPTIONALParser;
import slope.lang.OR.ORParser;
import slope.lang.SET.NULLParserConstructor;
import slope.lang.SET.SetTokenValueParserConstructor;
import slope.lang.TOKEN.TokenParserConstructor;

import java.lang.reflect.Method;

/**
 * Created by Thorben on 15.06.2016.
 */

public final class ElementHandler {

    public static Element handle(PC object, Op operator, Element parent) {
        if (object instanceof NULLParserConstructor || object == null) return null;
        try {
            if (object instanceof Rule) {
                if (object.getPCTypeName().equals(Op.OPTIONAL.name()))
                    return new OptionalElement((ParserConstructor_1) ((Rule) object).pc(), operator, parent);
                else return new RuleElement((Rule) object, operator, parent);
            } else {
                Method handler = ElementHandler.class.getMethod("handle", object.getClass(), operator.getClass(), Element.class);
                return (Element) handler.invoke(null, object, operator, parent);
            }
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public static TokenElement handle(SetTokenValueParserConstructor sTVPC, Op operator, Element parent) {
        return new TokenElement(sTVPC.tokenParserConstructor, operator, parent);
    }

    public static TokenElement handle(TokenParserConstructor tPC, Op operator, Element parent) {
        return new TokenElement(tPC, operator, parent);
    }

    public static RuleElement handle(Rule rule, Op operator, Element parent) {
        return new RuleElement(rule, operator, parent);
    }

    public static Element handle(ParserConstructor_1 pc1, Op operator, Element parent) {
        if (pc1.createParser() instanceof NFOLDParser) return new NFoldElement(pc1, operator, parent);
        else if (pc1.createParser() instanceof OPTIONALParser) return new OptionalElement(pc1, operator, parent);
        else return null;
    }

    public static Element handle(ParserConstructor_N pcN, Op operator, Element parent) {
        if (pcN.createParser() instanceof ANDParser) return new ANDElement(pcN, operator, parent);
        else if (pcN.createParser() instanceof ORParser) return new ORElement(pcN, operator, parent);
        else return null;
    }

}