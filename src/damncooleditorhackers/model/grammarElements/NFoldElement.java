package damncooleditorhackers.model.grammarElements;

import slope.PC;
import slope.ParserConstructor_1;

import java.util.ArrayList;

/**
 * Created by Liron on 15.06.2016.
 **/
public class NFoldElement extends Element {
    private ParserConstructor_1 object;
    private int lastSel;
    private boolean textSet;
    private boolean autoAdd;

    public NFoldElement(ParserConstructor_1 object, Op operator, Element parent) {
        super(operator, parent);
        this.object = object;
        lastSel = -1;
        textSet = false;
        if (operator != Op.OR) traverseChildren();
        else
            setText(Op.NFOLD.name());
        autoAdd = true;
    }

    public NFoldElement(NFoldElement e) {
        super(e);
        this.object = e.getObject();
        autoAdd = e.autoAdd;
    }

    public void traverseChildren() {
        if (lastSel < 0 || lastSel >= getChildren().size()) {
            addChildren(createChildElement((PC) object.getPCChildren().get(0), Op.NFOLD, this));
        } else {
            children.add(lastSel, createChildElement((PC) object.getPCChildren().get(0), Op.NFOLD, this));
            for (int i = lastSel; i < children.size(); i++) {
                children.get(i).updateID();
            }
            lastSel = -1;
        }
        if (getChildren().size() > 0 && !textSet) {
            setText(getChildren().get(0).getText());
            textSet = true;
        }
    }

    public void addChildByIndex(int index) {
        if (index >= 0) {
            children.add(index, createChildElement((PC) object.getPCChildren().get(0), Op.NFOLD, this));
        }
        for (int i = index; i < children.size(); i++) updateID();
    }

    public boolean isSingleToken(int depth) {
        return false;
    }

    private boolean TokenContainer(int depth) {
        for (Element e : children) if (!e.isSingleToken(depth)) return false;
        return true;
    }

    public void setSelectionFromChild(Element e) {
        if (e != null && e.getIDs().size() > getIDs().size()) {
            for (int i = 0; i < getIDs().size(); i++)
                if (!getIDs().get(i).equals(e.getIDs().get(i))) {
                    lastSel = 0;
                    return;
                }
            lastSel = e.getIDs().get(getIDs().size());
        } else {
            lastSel = -1;
        }
    }

    public void setChildren(ArrayList<Element> list) {
        children = list;
        for (Element e : children) {
            e.setParent(this);
            e.updateID();
        }
        setText(getChildren().get(0).getText());
    }

    public ParserConstructor_1 getObject() {
        return object;
    }

    public Type getType() {
        return Type.NFOLD;
    }

    public boolean sameStruct(Element other) {
        if (other instanceof NFoldElement) {
            boolean added = false;
            boolean addedThis = false;
            if (other.getChildren().size() < 0) {
                other.addChildren(createChildElement((PC) other.getObject().getPCChildren().get(0), other.getOperator(), other));
                added = true;
            }
            if (this.getChildren().size() < 0) {
                addChildren(createChildElement((PC) object.getPCChildren().get(0), this.getOperator(), this));
                addedThis = true;
            }
            boolean result = this.getChildren().get(0).sameStruct(other.getChildren().get(0));
            if (added) other.getChildren().remove(other.getChildren().size() - 1);
            if (addedThis) this.children.remove(this.children.size() - 1);
            return result;
        }
        return false;
    }

    public boolean isAutoAdd() {
        return autoAdd;
    }

    public boolean isComplete() {
        if (!TokenContainer(1)) return false;
        ArrayList<Element> tmp = this.getLeaves();
        for (Element e : tmp) if (!e.getType().equals(Type.TOKEN)) return false;
        return true;
    }

    public void setAutoAdd(boolean autoAdd) {
        this.autoAdd = autoAdd;
    }
}
