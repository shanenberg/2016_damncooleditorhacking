package damncooleditorhackers.model.grammarElements;

import slope.ParserConstructor_N;

import java.util.ArrayList;

/**
 * Created by Nick on 15.06.2016.
 */
public class ORElement extends Element {
    private ParserConstructor_N object;
    private int choice;
    private boolean traversed;

    public ORElement(ParserConstructor_N object, Op operator, Element parent) {
        super(operator, parent);
        choice = -1;
        this.object = object;
        setText(Op.OR.name());
        traversed = false;
        traverseChildren();
        traversed = true;
    }

    public ORElement(ORElement e) {
        super(e);
        this.object = e.getObject();
        this.choice = e.getChoice();
        this.traversed = e.traversed;
    }

    public int getChoice() {
        return choice;
    }

    public void traverseChildren() {
        if (traversed) return;
        for (int i = 0; i < object.pcs.length; i++)
            addChildren(createChildElement(object.pcs[i], Op.OR, this));
        String text = "";
        for (Element e : super.getChildren()) text += e + " | ";
        if (!text.equals("")) setText(text.substring(0, text.length() - 2));
    }

    public void addChildByIndex(int index) {
    }


    @Override
    public ArrayList<Element> getChildren() {
        ArrayList<Element> tmp = new ArrayList<Element>();
        if (choice >= 0 && choice < super.getChildren().size()) tmp.add(super.getChildren().get(choice));
        return tmp;
    }

    public void setRule(Element e) {
        if (choice == -1) choice = super.getChildren().indexOf(e);
        else {
            if (e != getChildren().get(0)) {
                children.remove(choice);
                children.add(choice, createChildElement(object.pcs[choice], Op.OR, this));
                children.get(choice).updateID();
                choice = super.getChildren().indexOf(e);
            }
        }


    }

    public ArrayList<Element> getChoices() {
        return super.getChildren();
    }

    public ParserConstructor_N getObject() {
        return object;
    }

    public Type getType() {
        return Type.OR;
    }

    public boolean isSingleToken(int depth) {
        if (depth <= 0) return false; //should not search deeper!
        for (Element child : children) {
            if (!child.isSingleToken(depth - 1)) return false;
        }
        return true;
    }

    public boolean sameStruct(Element other) {
        if (other instanceof ORElement) {
            ORElement or = (ORElement) other;
            if (this.getChoices().size() == or.getChoices().size()) {
                for (int i = 0; i < or.getChoices().size(); i++)
                    if (!this.getChoices().get(i).sameStruct(or.getChoices().get(i))) return false;
                return true;
            }
        }
        return false;
    }

    @Override
    public int isOuterChild(Element child) {
        return 1;
    }
}
