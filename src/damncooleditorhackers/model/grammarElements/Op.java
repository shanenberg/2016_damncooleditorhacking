package damncooleditorhackers.model.grammarElements;

/**
 * Created by Nick on 09.06.2016.
 */
public enum Op {
    AND,
    OR,
    NFOLD,
    NONE,
    OPTIONAL;
}
