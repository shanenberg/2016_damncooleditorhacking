package damncooleditorhackers.model.grammarElements;

import slope.PC;

import java.util.ArrayList;

/**
 * Created by Mark-Desktop on 24.09.2016.
 */
public class OptionalContainer extends Element {
    private OptionalElement original;

    public OptionalContainer(OptionalElement e) {
        super(null);
        original = e;
    }

    public boolean sameStruct(Element other) {
        return false;
    }

    public void traverseChildren() {
    }

    public void addChildByIndex(int index) {
    }

    public PC getObject() {
        return null;
    }

    @Override
    public Type getType() {
        return Type.OPCONTAINER;
    }

    @Override
    public String getText() {
        return original.getRealText();
    }

    @Override
    public Element getParent() {
        return original;
    }

    @Override
    public ArrayList<Integer> getIDs() {
        return original.getIDs();
    }

    public boolean isSingleToken(int depth) {
        return false;
    }

    public OptionalElement getOriginal() {
        return original;
    }
}
