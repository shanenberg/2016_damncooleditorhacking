package damncooleditorhackers.model.grammarElements;

import slope.PC;
import slope.ParserConstructor_1;

import java.util.ArrayList;

/**
 * Created by Nick on 14.08.2016.
 */
public class OptionalElement extends Element {
    protected ParserConstructor_1 object;
    private boolean traversed;
    private boolean active;
    private boolean visible;

    public OptionalElement(ParserConstructor_1 object, Op operator, Element parent) {
        super(operator, parent);
        this.object = object;
        setText(object.getPCTypeName());
        traversed = false;
        active = false;
        visible = true;
        if (operator != Op.OR) traverseChildren();
    }

    public OptionalElement(OptionalElement e) {
        super(e);
        this.object = e.getObject();
        this.traversed = e.traversed;
        this.active = e.isActive();
        this.visible = e.visible;
    }

    public boolean isVisible() {
        return visible;
    }

    @Override
    public String getText() {
        return (visible ? super.getText() : "?");
    }

    public String getRealText() {
        return super.getText();
    }

    public void setVisible(boolean visible) {
        this.visible = visible;
    }

    public void addChildByIndex(int index) {
        if (children.size() < 1)
            addChildren(createChildElement((PC) object.getPCChildren().get(0), Op.NONE, this));
    }
    public void traverseChildren() {
        if (!traversed) {
            addChildren(createChildElement((PC) object.getPCChildren().get(0), Op.NONE, this));
            setText(super.getChildren().get(0).getText() + "?");
            traversed = true;
        }
    }

    public void setTraversed(boolean traversed) {
        this.traversed = traversed;
    }

    public ParserConstructor_1 getObject() {
        return object;
    }

    public Type getType() {
        return Type.OPTIONAL;
    }

    public void setActive(boolean opt) {
        active = opt;
    }

    public Element getOptional() {
        return super.getChildren().get(0);
    }

    public void removeOptional() {
        super.getChildren().clear();
        traversed = false;
        active = false;
        traverseChildren();
    }

    public boolean isActive() {
        return active;
    }

    @Override
    public ArrayList<Element> getChildren() {
        return active ? super.getChildren() : new ArrayList<Element>();
    }

    public boolean isSingleToken(int depth) {
        return children.get(0).isSingleToken(depth);
    }

    public boolean sameStruct(Element other) {
        if (other instanceof RuleElement) {
            if (object.getPCTypeName().equals(other.getObject().getPCTypeName())) return true;
        }
        return false;
    }
}
