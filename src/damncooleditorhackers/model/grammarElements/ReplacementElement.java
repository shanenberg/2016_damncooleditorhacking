package damncooleditorhackers.model.grammarElements;

import damncooleditorhackers.view.view_elements.ElementButton;
import slope.PC;

import java.util.ArrayList;

/**
 * Created by Mark-Desktop on 02.07.2016.
 */
public class ReplacementElement extends Element {
    private ArrayList<Element> selected;
    private ArrayList<ArrayList<Integer>> realID;
    private ReplacementType repType;
    private String myText = "";
    private ElementButton button;

    public ReplacementElement(ReplacementType type, Element parent, Element selected) {
        super(Op.NONE, parent);
        this.selected = new ArrayList<Element>();
        this.selected.add(selected);
        repType = type;
        realID = new ArrayList<ArrayList<Integer>>();
        ArrayList<Integer> tmp = new ArrayList<Integer>();
        tmp.addAll(selected.getIDs());
        realID.add(tmp);
    }


    public void traverseChildren() {
    }

    @Override
    public String getText() {
        if (myText.equals(""))
            if (repType == ReplacementType.DELETEOPTIONAL) return "X Löschen";
            else return selected.get(0).getText();
        else
            return myText;
    }

    @Override
    public void setText(String text) {
        myText = text;
    }

    public void addChildByIndex(int index) {
    }

    public ElementButton getButton() {
        return button;
    }

    public void setButton(ElementButton button) {
        this.button = button;
    }

    public boolean isSingleToken(int depth) {
        return false;
    }

    @Override
    public void addChildren(Element child) {
        selected.add(child);
        ArrayList<Integer> tmp = new ArrayList<Integer>();
        tmp.addAll(child.getIDs());
        realID.add(tmp);
    }

    @Override
    public ArrayList<Element> getChildren() {
        return selected;
    }

    public PC getObject() {
        return null;
    }

    @Override
    public ArrayList<Integer> getIDs() {
        return realID.get(0);
    }

    public Type getType() {
        return Type.REPLACEMENT;
    }

    public ReplacementType getReplacementType() {
        return repType;
    }

    public boolean sameStruct(Element other) {
        return false;
    }
}
