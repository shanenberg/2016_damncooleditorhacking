package damncooleditorhackers.model.grammarElements;

/**
 * Created by Mark-Desktop on 03.07.2016.
 */
public enum ReplacementType {
    OR,
    DUPLICATE,
    DELETEOPTIONAL,
    TOKEN
}
