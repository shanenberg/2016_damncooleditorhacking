package damncooleditorhackers.model.grammarElements;

import slope.Rule;

/**
 * Created by Nick on 15.06.2016.
 */
public class RuleElement extends Element {
    private Rule object;
    private boolean traversed;

    public RuleElement(Rule object, Op operator, Element parent) {
        super(operator, parent);
        this.object = object;
        setText(object.getPCTypeName());
        traversed = false;
        if (operator != Op.OR) traverseChildren();
    }

    public RuleElement(RuleElement e) {
        super(e);
        this.object = e.getObject();
        this.traversed = e.traversed;
    }

    public void addChildByIndex(int index) {
        if (children.size() < 1)
            addChildren(createChildElement(object.pc(), Op.NONE, this));
    }

    public void traverseChildren() {
        if (!traversed) {
            addChildren(createChildElement(object.pc(), Op.NONE, this));
            traversed = true;
        }
    }

    public Rule getObject() {
        return object;
    }

    public Type getType() {
        return Type.RULE;
    }

    public boolean isSingleToken(int depth) {
        return children.get(0).isSingleToken(depth);
    }

    public boolean sameStruct(Element other) {
        if (other instanceof RuleElement) {
            if (object.getPCTypeName().equals(other.getObject().getPCTypeName())) return true;
        }
        return false;
    }
}
