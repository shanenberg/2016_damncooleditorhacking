package damncooleditorhackers.model.grammarElements;

import slope.lang.TOKEN.TokenParserConstructor;
import slowscane.scanners.KeywordScanner;
import slowscane.scanners.SingleCharScanner;
import slowscane.scanners.TokenReader;
import slowscane.scanners.lib.JavaIdentifierScanner;
import slowscane.scanners.lib.JavaStringLiteralScanner;

/**
 * Created by Nick on 15.06.2016.
 */
public class TokenElement extends Element {
    private TokenParserConstructor object;
    private boolean set;

    public TokenElement(TokenParserConstructor object, Op operator, Element parent) {
        super(operator, parent);
        set = false;
        this.object = object;
        traverseChildren();
    }

    public TokenElement(TokenElement e) {
        super(e);
        this.object = e.getObject();
        this.set = e.isSet();
    }

    public boolean isJavaScanner() {
        return (object.elementScanner instanceof JavaIdentifierScanner || object.elementScanner instanceof JavaStringLiteralScanner);
    }

    private boolean isJavaIdentifier() {
        return object.elementScanner instanceof JavaIdentifierScanner;
    }

    public boolean isJavaStringLiteral() {
        return object.elementScanner instanceof JavaStringLiteralScanner;
    }

    public TokenReader getJIScanner() {
        return object.elementScanner;
    }

    public void traverseChildren() {
        if (isJavaIdentifier())
            setText(getParent().toString());
        else if (isJavaStringLiteral()) setText("\"" + findParentRule(this) + "\"");
        else if (object.elementScanner instanceof KeywordScanner) {
            setText(String.valueOf(((KeywordScanner) object.elementScanner).keyword));
        } else if (object.elementScanner instanceof SingleCharScanner) {
            setText(String.valueOf(((SingleCharScanner) object.elementScanner).character));
        }
    }

    private String findParentRule(Element element) {
        Element parent = element.getParent();
        while (parent.hasParent() && parent.getType() != Type.RULE) parent = parent.getParent();
        return parent.toString();
    }

    public void addChildByIndex(int index) {
        if (children.size() < 1)
            traverseChildren();
    }

    public TokenParserConstructor getObject() {
        return object;
    }

    public boolean isSet() {
        return set;
    }

    public void doSet() {
        set = true;
    }

    public Type getType() {
        return Type.TOKEN;
    }

    public boolean sameStruct(Element other) {
        if (other instanceof TokenElement) {
            TokenElement token = (TokenElement) other;
            if (this.isJavaScanner() && token.isJavaScanner()) return true;
            else if (!this.isJavaScanner() && !token.isJavaScanner() && this.getText().equals(token.getText()))
                return true;
        }
        return false;
    }

    public boolean isSingleToken(int depth) {
        return true;
    }
}
