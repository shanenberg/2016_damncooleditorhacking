package damncooleditorhackers.model.grammarElements;

/**
 * Created by Nick on 22.06.2016.
 */
public enum Type {
    AND,
    OR,
    NFOLD,
    RULE,
    TOKEN,
    DELETION,
    ADD_RIGHT,
    REPLACEMENT,
    OPTIONAL,
    OPCONTAINER;
}