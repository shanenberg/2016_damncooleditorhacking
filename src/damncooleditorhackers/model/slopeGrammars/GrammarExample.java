package damncooleditorhackers.model.slopeGrammars;


import slope.PC;
import slope.Parser;
import slope.Rule;
import slope.lib.grammar.GrammarParsing;
import slowscane.Tokens;
import slowscane.annotations.TokenLevel;
import slowscane.scanners.KeywordScanner;
import slowscane.scanners.TokenReader;

/**
 * Created by thorb on 30.09.2016.
 */
public class GrammarExample extends Tokens {

    private final TokenReader<String> trA = new KeywordScanner("a");
    private final TokenReader<String> trB = new KeywordScanner("b");
    private final TokenReader<String> trC = new KeywordScanner("c");
    private final TokenReader<String> trD = new KeywordScanner("d");


    @TokenLevel(value = 90)
    public TokenReader<String> A() {
        return trA;
    }

    @TokenLevel(value = 91)
    public TokenReader<String> B() {
        return trB;
    }

    @TokenLevel(value = 92)
    public TokenReader<String> C() {
        return trC;
    }

    @TokenLevel(value = 93)
    public TokenReader<String> D() {
        return trD;
    }


    public class Grammar01Parser extends Parser<GrammarParsing> {


        /*
       Startregel -> a b+ (c|a) d?
        */
        public Rule Grammar() {
            return new Rule() {
                public PC pc() {
                    return
                            AND(
                                    TOKEN("a"),
                                    NFOLD(TOKEN("b")),
                                    OR(
                                            TOKEN("c"),
                                            TOKEN("a")
                                    ),
                                    OPTIONAL(TOKEN("d"))
                            );
                }
            };
        }


    }


}
