package damncooleditorhackers.model.slopeGrammars;

import slope.PC;
import slope.Parser;
import slope.Rule;
import slope.lib.grammar.GrammarParsing;
import slowscane.Tokens;
import slowscane.annotations.Keyword;
import slowscane.annotations.TokenLevel;
import slowscane.scanners.KeywordScanner;
import slowscane.scanners.TokenReader;
import slowscane.scanners.lib.JavaStringLiteralScanner;

public class GrammarKassenzettel extends Tokens {

    public final TokenReader<String> IhrSupermarkt = new KeywordScanner("Ihr Supermarkt");
    public final TokenReader<String> Rueckgabe = new KeywordScanner("R ");
    public final TokenReader<String> komma = new KeywordScanner(",");
    public final TokenReader<String> telNr = new KeywordScanner("Tel.: ");
    public final TokenReader<String> zahl0 = new KeywordScanner("0");
    public final TokenReader<String> zahl1 = new KeywordScanner("1");
    public final TokenReader<String> zahl2 = new KeywordScanner("2");
    public final TokenReader<String> zahl3 = new KeywordScanner("3");
    public final TokenReader<String> zahl4 = new KeywordScanner("4");
    public final TokenReader<String> zahl5 = new KeywordScanner("5");
    public final TokenReader<String> zahl6 = new KeywordScanner("6");
    public final TokenReader<String> zahl7 = new KeywordScanner("7");
    public final TokenReader<String> zahl8 = new KeywordScanner("8");
    public final TokenReader<String> zahl9 = new KeywordScanner("9");
    public final TokenReader<String> sumPreis = new KeywordScanner("=sum(Preis)");
    public final TokenReader<String> bar = new KeywordScanner("Bar");
    public final TokenReader<String> neunzehnProzent = new KeywordScanner("Voll 19,0%");
    public final TokenReader<String> siebenProzent = new KeywordScanner("Erm 7,0%");
    public final TokenReader<String> Voll = new KeywordScanner("(Voll)");
    public final TokenReader<String> ecKarte = new KeywordScanner("EC-Karte");
    public final TokenReader<String> eur = new KeywordScanner("EUR");
    public final TokenReader<String> Erm = new KeywordScanner("(Erm)");
    public final TokenReader<String> steuerart = new KeywordScanner("Steuerart");
    public final TokenReader<String> ohneMwSt = new KeywordScanner("ohne MwSt");
    public final TokenReader<String> mitMwSt = new KeywordScanner("mit MwSt");
    public final TokenReader<String> MwSt = new KeywordScanner("MwSt");
    public final TokenReader<String> sumMitMwSt = new KeywordScanner("=sum(sumMwSt + sumOhneMwSt)");
    public final TokenReader<String> uhrzeit = new KeywordScanner("Uhrzeit: ");
    public final TokenReader<String> ustIdNr = new KeywordScanner("USt-IdNr.: DE ");
    public final TokenReader<String> colon = new KeywordScanner(":");
    public final TokenReader<String> fiveWhites = new KeywordScanner("     ");
    public final TokenReader<String> fourWhites = new KeywordScanner("    ");
    public final TokenReader<String> whiteSpace = new KeywordScanner(" ");
    public final TokenReader<String> period = new KeywordScanner(".");
    public final TokenReader<String> minus = new KeywordScanner("-");

    @Keyword
    public TokenReader<String> ecKarte() {
        return ecKarte;
    }

    @Keyword
    public TokenReader<String> eur() {
        return eur;
    }

    @Keyword
    public TokenReader<String> steuerart() {
        return steuerart;
    }

    @Keyword
    public TokenReader<String> ohneMwSt() {
        return ohneMwSt;
    }

    @Keyword
    public TokenReader<String> mitMwSt() {
        return mitMwSt;
    }

    @Keyword
    public TokenReader<String> MwSt() {
        return MwSt;
    }

    @Keyword
    public TokenReader<String> zahl0() {
        return zahl0;
    }

    @Keyword
    public TokenReader<String> zahl1() {
        return zahl1;
    }

    @Keyword
    public TokenReader<String> zahl2() {
        return zahl2;
    }

    @Keyword
    public TokenReader<String> zahl3() {
        return zahl3;
    }

    @Keyword
    public TokenReader<String> zahl4() {
        return zahl4;
    }

    @Keyword
    public TokenReader<String> zahl5() {
        return zahl5;
    }

    @Keyword
    public TokenReader<String> zahl6() {
        return zahl6;
    }

    @Keyword
    public TokenReader<String> zahl7() {
        return zahl7;
    }

    @Keyword
    public TokenReader<String> zahl8() {
        return zahl8;
    }

    @Keyword
    public TokenReader<String> zahl9() {
        return zahl9;
    }

    @Keyword
    public TokenReader<String> komma() {
        return komma;
    }

    @Keyword
    public TokenReader<String> period() {
        return period;
    }

    @Keyword
    public TokenReader<?> NEWLINE() {
        return lib.NEWLINE;
    }

    @Keyword
    public TokenReader<String> bar() {
        return bar;
    }

    @Keyword
    public TokenReader<String> telNr() {
        return telNr;
    }

    @Keyword
    public TokenReader<String> minus() {
        return minus;
    }

    @TokenLevel(value = 87)
    public TokenReader<String> IhrSupermarkt() {
        return IhrSupermarkt;
    }

    @TokenLevel(value = 89)
    public TokenReader<String> sumPreis() {
        return sumPreis;
    }

    @TokenLevel(value = 90)
    public TokenReader<String> Rueckgabe() {
        return Rueckgabe;
    }

    @TokenLevel(value = 91)
    public TokenReader<String> Voll() {
        return Voll;
    }

    //    @Keyword
    @TokenLevel(value = 92)
    public TokenReader<String> Erm() {
        return Erm;
    }

    //    @Keyword
    @TokenLevel(value = 93)
    public TokenReader<String> neunzehnProzent() {
        return neunzehnProzent;
    }

    //    @Keyword
    @TokenLevel(value = 94)
    public TokenReader<String> siebenProzent() {
        return siebenProzent;
    }

    //    @Keyword
    @TokenLevel(value = 95)
    public TokenReader<String> sumMitMwSt() {
        return sumMitMwSt;
    }

    //    @Keyword
    @TokenLevel(value = 96)
    public TokenReader<String> uhrzeit() {
        return uhrzeit;
    }

    //    @Keyword
    @TokenLevel(value = 97)
    public TokenReader<String> ustIdNr() {
        return ustIdNr;
    }

    @TokenLevel(value = 97)
    public TokenReader<String> fiveWhites() {
        return fiveWhites;
    }

    @TokenLevel(value = 98)
    public TokenReader<String> fourWhites() {
        return fourWhites;
    }

    @TokenLevel(value = 99)
//   @Keyword
    public TokenReader<String> whiteSpace() {
        return whiteSpace;
    }

    @TokenLevel(value = 99)
//   @Keyword
    public TokenReader<String> colon() {
        return colon;
    }

    @TokenLevel(value = 99)
    public TokenReader<String> JAVA_STRING_LITERAL() {
        return new JavaStringLiteralScanner();
    }




    public class Grammar01Parser extends Parser<GrammarParsing> {

        /*
               Start -> Kopf "\n" Liste+ "\n" Bezahlung "\n" Steuer "\n" Zeitstempel "\n" Steuernummer "\n" Extras.
         */
        public Rule Grammar() {
            return new Rule() {
                public PC pc() {

                    return
                            AND(
                                    Kopf(), TOKEN(NEWLINE()),
                                    NFOLD(Liste()), TOKEN(NEWLINE()),
                                    Bezahlung(), TOKEN(NEWLINE()),
                                    Steuer(), TOKEN(NEWLINE()),
                                    Zeitstempel(), TOKEN(NEWLINE()),
                                    Steuernummer(), TOKEN(NEWLINE()),
                                    Extras()
                            );
                }
            };
        }

        /*
             Kopf -> "Ihr Supermarkt" "\n" Adresse.
         */
        public Rule Kopf() {
            return new Rule() {
                public PC pc() {
                    return
                            AND(
                                    TOKEN("Ihr Supermarkt"),
                                    TOKEN(NEWLINE()),
                                    Adresse()
                            );
                }
            };
        }

        /*
             Adresse -> PLZ " " Stadt "\n" Straße "\n" Telefonnummer "\n \n".
         */
        public Rule Adresse() { return new Rule() { public PC pc() {  return
                AND(
                        PLZ(),
                        TOKEN(" "),
                        Stadt(),
                        TOKEN(NEWLINE()),
                        Straße(),
                        TOKEN(NEWLINE()),
                        Telefonnummer(),
                        TOKEN(NEWLINE()),
                        TOKEN(NEWLINE())
                );
        }
        };
        }

        /*
             PLZ -> Zahl Zahl Zahl Zahl Zahl.
         */
        public Rule PLZ() {
            return new Rule() {
                public PC pc() {
                    return
                            AND(
                                    Zahl(), Zahl(), Zahl(), Zahl(), Zahl()
                            );
                }
            };
        }

        /*
             Zahl -> "0"|"1"|"2"|"3"|"4"|"5"|"6"|"7"|"8"|"9"..
         */
        public Rule Zahl() {
            return new Rule() {
                public PC pc() {
                    return
                            OR(
                                    TOKEN("0"), TOKEN("1"), TOKEN("2"), TOKEN("3"), TOKEN("4"), TOKEN("5"),
                                    TOKEN("6"), TOKEN("7"), TOKEN("8"), TOKEN("9")
                            );
                }
            };
        }

        /*
            Stadt -> JAVA_STRING_LITERAL.
        */
        public Rule Stadt() {return new Rule() {public PC pc() {return
                TOKEN(JAVA_STRING_LITERAL());
        }
        };
        }

        /*
            Straße -> JAVA_STRING_LITERAL.
        */
        public Rule Straße() { return new Rule() { public PC pc() { return
                TOKEN(JAVA_STRING_LITERAL());
        }};}

        /*
            Telefonnummer -> "Tel.: " Zahl+.
        */
        public Rule Telefonnummer() { return new Rule() { public PC pc() { return
                AND(
                        TOKEN("Tel.: "),
                        NFOLD(Zahl())
                );
        }
        };
        }

        /*
            Zahlungsart -> "EC-Karte" | "Bar".
        */
        public Rule Zahlungsart() { return new Rule() { public PC pc() { return
                OR(
                        TOKEN("EC-Karte"), TOKEN("Bar")
                );
        }
        };
        }

        /*
            Rueckgabe -> "R ".
        */
        public Rule Rueckgabe() {
            return new Rule() {
                public PC pc() {
                    return
                            TOKEN("R ");
                }
            };
        }

        /*
            Liste -> Rueckgabe? Artikelname " " " " Preis "\n".
        */
        public Rule Liste() { return new Rule() { public PC pc() { return
                AND(
                        OPTIONAL(Rueckgabe()),
                        Artikelname(),
                        TOKEN(" "), TOKEN(" "),
                        Preis(),
                        TOKEN(NEWLINE())
                );
        }
        };
        }

        /*
            Artikelname -> JAVA_STRING_LITERAL.
        */
        public Rule Artikelname() { return new Rule() { public PC pc() {  return
                TOKEN(JAVA_STRING_LITERAL());
        } }; }

        /*
            Stunden -> "0" Zahl|"1" Zahl|"2" ZahlBisDrei.
        */
        public Rule Stunden() {
            return new Rule() {
                public PC pc() {
                    return
                            OR(
                                    AND(TOKEN("0"), Zahl()),
                                    AND(TOKEN("1"), Zahl()),
                                    AND(TOKEN("2"), ZahlBisDrei())
                            );
                }
            };
        }

        /*
            Minuten -> ("0"|"1"|"2"|"3"|"4"|"5") Zahl.
        */
        public Rule Minuten() {
            return new Rule() {
                public PC pc() {
                    return
                            AND(
                                    OR(
                                            TOKEN("0"),
                                            TOKEN("1"),
                                            TOKEN("2"),
                                            TOKEN("3"),
                                            TOKEN("4"),
                                            TOKEN("5")
                                    ),
                                    Zahl()
                            );
                }
            };
        }

        /*
            Tag -> "0" Zahl|"1" Zahl|"2" Zahl|"3""0"|"3""1".
        */
        public Rule Tag() {
            return new Rule() {
                public PC pc() {
                    return
                            OR(
                                    AND(TOKEN("0"), Zahl()),
                                    AND(TOKEN("1"), Zahl()),
                                    AND(TOKEN("2"), Zahl()),
                                    AND(TOKEN("3"), TOKEN("0")),
                                    AND(TOKEN("3"), TOKEN("1"))
                            );
                }
            };
        }

        /*
            Monat -> "0" Zahl|"1"0"|"1""1"|"1""2".
        */
        public Rule Monat() {
            return new Rule() {
                public PC pc() {
                    return
                            OR(
                                    AND(TOKEN("0"), Zahl()),
                                    AND(TOKEN("1"), TOKEN("0")),
                                    AND(TOKEN("1"), TOKEN("1")),
                                    AND(TOKEN("1"), TOKEN("2"))
                            );
                }
            };
        }

        /*
            ZahlBisDrei -> "0" | "1" | "2" | "3".
        */
        public Rule ZahlBisDrei() {
            return new Rule() {
                public PC pc() {
                    return
                            OR(
                                    TOKEN("0"),
                                    TOKEN("1"),
                                    TOKEN("2"),
                                    TOKEN("3")
                            );
                }
            };
        }

        /*
            Steuerart -> "(Voll)" | "(Erm)".
        */
        public Rule Steuerart() {
            return new Rule() {
                public PC pc() {
                    return
                            OR(
                                    TOKEN("(Voll)"),
                                    TOKEN("(Erm)")
                            );
                }
            };
        }

        /*
            Preis -> ("-"|" ") Betrag " " Steuerart.
        */
        public Rule Preis() {
            return new Rule() {
                public PC pc() {
                    return
                            AND(
                                    OR(TOKEN("-"), TOKEN(" ")),
                                    Betrag(),
                                    TOKEN(" "),
                                    Steuerart()
                            );
                }
            };
        }

        /*
            Betrag -> Zahl+ "," Zahl Zahl.
        */
        public Rule Betrag() {
            return new Rule() {
                public PC pc() {
                    return
                            AND(
                                    NFOLD(Zahl()),
                                    TOKEN(","),
                                    Zahl(),
                                    Zahl()
                            );
                }
            };
        }

        /*
            Bezahlung -> Zahlungsart Waehrung " " "=sum(Preis)".
        */
        public Rule Bezahlung() {
            return new Rule() {
                public PC pc() {
                    return
                            AND(
                                    Zahlungsart(),
                                    Waehrung(),
                                    TOKEN(" "),
                                    TOKEN("=sum(Preis)")
                            );
                }
            };
        }

        /*
            Waehrung -> " " EUR".
        */
        public Rule Waehrung() {
            return new Rule() {
                public PC pc() {
                    return
                            AND(TOKEN(" "), TOKEN("EUR"));
                }
            };
        }

        /*
            Steuer -> "Steuerart" "    " "MwSt" "    " "ohne MwSt" "    " "mit MwSt" SteuerListe "\n\n".
        */
        public Rule Steuer() {
            return new Rule() {
                public PC pc() {
                    return
                            AND(
                                    TOKEN("Steuerart"), TOKEN("    "),
                                    TOKEN("MwSt"), TOKEN("    "),
                                    TOKEN("ohne MwSt"), TOKEN("    "),
                                    TOKEN("mit MwSt"),
                                    TOKEN(NEWLINE()),
                                    SteuerListe(),
                                    TOKEN(NEWLINE())
                            );
                }
            };
        }

        /*
            SteuerListe -> "\n"( "Voll 19,0%" "     " SumMwSt "     "SumOhneMwSt "     " SumMitMwSt)? "\n"( "Erm 7,0%" "     " SumMwSt "     "SumOhneMwSt "     " SumMitMwSt)?.
        */
        public Rule SteuerListe() {
            return new Rule() {
                public PC pc() {
                    return
                            AND(
                                    AND(
                                            OPTIONAL(
                                                    AND(
                                                            TOKEN("Voll 19,0%"),
                                                            TOKEN("     "),
                                                            SumMwSt(),
                                                            TOKEN("     "),
                                                            SumOhneMwSt(),
                                                            TOKEN("     "),
                                                            SumMitMwSt()
                                                    )
                                            ),
                                            TOKEN(NEWLINE())
                                    ),
                                    AND(
                                            OPTIONAL(
                                                    AND(
                                                            TOKEN("Erm 7,0%"),
                                                            TOKEN("     "),
                                                            SumMwSt(),
                                                            TOKEN("     "),
                                                            SumOhneMwSt(),
                                                            TOKEN("     "),
                                                            SumMitMwSt()
                                                    )
                                            ),
                                            TOKEN(NEWLINE())
                                    )
                            );
                }
            };
        }

        /*
            SumMwSt -> Betrag.
        */
        public Rule SumMwSt() {
            return new Rule() {
                public PC pc() {
                    return
                            Betrag();
                }
            };
        }

        /*
            SumOhneMwSt -> Betrag.
        */
        public Rule SumOhneMwSt() {
            return new Rule() {
                public PC pc() {
                    return
                            Betrag();
                }
            };
        }

        /*
            SumMitMwSt -> "=sum(sumMwSt + sumOhneMwSt)".
        */
        public Rule SumMitMwSt() {
            return new Rule() {
                public PC pc() {
                    return
                            TOKEN("=sum(sumMwSt + sumOhneMwSt)");
                }
            };
        }

        /*
            Zeitstempel -> Datum "\n" Uhrzeit.
        */
        public Rule Zeitstempel() {
            return new Rule() {
                public PC pc() {
                    return
                            AND(
                                    Datum(),
                                    TOKEN(NEWLINE()),
                                    Uhrzeit()
                            );
                }
            };
        }

        /*
            Datum -> Tag"."Monat".""2""0" Zahl Zahl.
        */
        public Rule Datum() {
            return new Rule() {
                public PC pc() {
                    return
                            AND(
                                    Tag(), TOKEN("."),
                                    Monat(),
                                    TOKEN("."),
                                    TOKEN("2"),
                                    TOKEN("0"),
                                    Zahl(),
                                    Zahl()
                            );
                }
            };
        }

        /*
            Uhrzeit -> "Uhrzeit: "Stunden":"Minuten.
        */
        public Rule Uhrzeit() {
            return new Rule() {
                public PC pc() {
                    return
                            AND(
                                    TOKEN("Uhrzeit: "),
                                    Stunden(),
                                    TOKEN(":"),
                                    Minuten()
                            );
                }
            };
        }

        /*
            Steuernummer -> "USt-IdNr.: DE " Zahl+.
        */
        public Rule Steuernummer() {
            return new Rule() {
                public PC pc() {
                    return
                            AND(
                                    TOKEN("USt-IdNr.: DE "),
                                    NFOLD(Zahl())
                            );
                }
            };
        }

        /*
            Extras -> JAVA_STRING_LITERAL.
        */
        public Rule Extras() {
            return new Rule() {
                public PC pc() {
                    return
                            TOKEN(JAVA_STRING_LITERAL());
                }
            };
        }

    }


}
