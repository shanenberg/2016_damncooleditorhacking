package damncooleditorhackers.model.slopeGrammars;


import slope.PC;
import slope.Parser;
import slope.Rule;
import slope.lib.grammar.GrammarParsing;
import slowscane.Tokens;
import slowscane.annotations.TokenLevel;
import slowscane.scanners.KeywordScanner;
import slowscane.scanners.TokenReader;
import slowscane.scanners.lib.JavaStringLiteralScanner;

/**
 * Created by thorb on 30.09.2016.
 */
public class GrammarTestDACH extends Tokens {

    private final TokenReader<String> trA = new KeywordScanner("A");
    private final TokenReader<String> trB = new KeywordScanner("B");
    private final TokenReader<String> trC = new KeywordScanner("C");
    private final TokenReader<String> trgetrennt = new KeywordScanner("getrennte");
    private final TokenReader<String> trZeile = new KeywordScanner("Zeile");
    private final TokenReader<String> trSirene = new KeywordScanner("tatütata");
    private final TokenReader<String> trda = new KeywordScanner("Lass mich bitte da");
    private final TokenReader<String> trweg = new KeywordScanner("Ich kann aber auch weg");
    private final TokenReader<String> trspace = new KeywordScanner("    ");
    private final TokenReader<String> zahl0 = new KeywordScanner("0");
    private final TokenReader<String> zahl1 = new KeywordScanner("1");
    private final TokenReader<String> zahl2 = new KeywordScanner("2");
    private final TokenReader<String> zahl3 = new KeywordScanner("3");
    private final TokenReader<String> zahl4 = new KeywordScanner("4");
    private final TokenReader<String> zahl5 = new KeywordScanner("5");
    private final TokenReader<String> trdies = new KeywordScanner("Dies ist die ");
    private final TokenReader<String> trpunkt = new KeywordScanner(". ");
    private final TokenReader<String> trstart = new KeywordScanner("Viel Erfolg beim Testen");


    @TokenLevel(value = 76)
    public TokenReader<String> Start() {
        return trstart;
    }

    @TokenLevel(value = 76)
    public TokenReader<String> Dies() {
        return trdies;
    }

    @TokenLevel(value = 77)
    public TokenReader<String> zahl0() {
        return zahl0;
    }

    @TokenLevel(value = 78)
    public TokenReader<String> zahl1() {
        return zahl1;
    }

    @TokenLevel(value = 79)
    public TokenReader<String> zahl2() {
        return zahl2;
    }

    @TokenLevel(value = 80)
    public TokenReader<String> zahl3() {
        return zahl3;
    }

    @TokenLevel(value = 81)
    public TokenReader<String> zahl4() {
        return zahl4;
    }

    @TokenLevel(value = 82)
    public TokenReader<String> zahl5() {
        return zahl5;
    }

    @TokenLevel(value = 89)
    public TokenReader<String> Punkt() {
        return trpunkt;
    }

    @TokenLevel(value = 89)
    public TokenReader<?> NEWLINE() {
        return lib.NEWLINE;
    }

    @TokenLevel(value = 90)
    public TokenReader<String> A() {
        return trA;
    }

    @TokenLevel(value = 91)
    public TokenReader<String> B() {
        return trB;
    }

    @TokenLevel(value = 92)
    public TokenReader<String> C() {
        return trC;
    }

    @TokenLevel(value = 93)
    public TokenReader<String> Getrennt() {
        return trgetrennt;
    }

    @TokenLevel(value = 94)
    public TokenReader<String> Zeile() {
        return trZeile;
    }

    @TokenLevel(value = 95)
    public TokenReader<String> Sirene() {
        return trSirene;
    }

    @TokenLevel(value = 96)
    public TokenReader<String> Da() {
        return trda;
    }

    @TokenLevel(value = 97)
    public TokenReader<String> Weg() {
        return trweg;
    }

    @TokenLevel(value = 98)
    public TokenReader<String> Space() {
        return trspace;
    }

    @TokenLevel(value = 99)
    public TokenReader<String> JAVA_STRING_LITERAL() {
        return new JavaStringLiteralScanner();
    }


    public class Grammar01Parser extends Parser<GrammarParsing> {

        /*
       Startregel -> Texteingabe “\n\n” Logik “\n\n” Newline “\n\n” nFach “\n\n” OptionalesElement  “\n\n” SchnelleEingabeZahl “\n\n” NewlineNFOLD “\n\n” Loeschen.
        */
        public Rule Grammar() {
            return new Rule() {
                public PC pc() {

                    return
                            AND(
                                    TOKEN(Start()), TOKEN(NEWLINE()),
                                    Texteingabe(),
                                    Logik(),
                                    Newline(),
                                    nFach(),
                                    OptionalesElement(),
                                    SchnelleEingabeZahlOptional(),
                                    Newline_nFach(),
                                    Loeschen()

                            );
                }
            };
        }
        /*
        Texteingabe -> JAVA_STRING_LITERAL.
        */

        public Rule Texteingabe() {
            return new Rule() {
                public PC pc() {
                    return
                            AND(
                                    TOKEN(JAVA_STRING_LITERAL()),
                                    TOKEN(NEWLINE()), TOKEN(NEWLINE())
                            );
                }
            };
        }

        public Rule Logik() {
            return new Rule() {
                public PC pc() {
                    return
                            AND(
                                    OR(
                                    TOKEN(A()),
                                    AND(
                                            TOKEN(B()),
                                            TOKEN(C())
                                    )
                                    ),
                                    TOKEN(NEWLINE()), TOKEN(NEWLINE())
                            );
                }
            };
        }

        public Rule Newline() {
            return new Rule() {
                public PC pc() {
                    return
                            AND(
                                    TOKEN(Getrennt()),
                                    TOKEN(NEWLINE()),
                                    TOKEN(Zeile()),
                                    TOKEN(NEWLINE()), TOKEN(NEWLINE())
                            );
                }
            };
        }

        public Rule nFach() {
            return new Rule() {
                public PC pc() {
                    return
                            AND(
                                    NFOLD(TOKEN(Sirene())),
                                    TOKEN(NEWLINE()), TOKEN(NEWLINE())
                            );
                }
            };
        }

        public Rule OptionalesElement() {
            return new Rule() {
                public PC pc() {
                    return
                            AND(
                                    OPTIONAL(TOKEN(Weg())),
                                    TOKEN(Da()),
                                    TOKEN(NEWLINE()), TOKEN(NEWLINE())
                            );
                }
            };
        }


        public Rule SchnelleEingabeZahlOptional() {
            return new Rule() {
                public PC pc() {
                    return
                            AND(
                                    OPTIONAL(
                                            SchnelleEingabeZahl()
                                    ),
                                    TOKEN(NEWLINE()), TOKEN(NEWLINE())
                            );
                }
            };
        }

        public Rule SchnelleEingabeZahl() {
            return new Rule() {
                public PC pc() {
                    return
                            AND(
                                    VierZahlen(),
                                    TOKEN(Space()),
                                    NFachZahl()
                            );
                }
            };
        }

        public Rule VierZahlen() {
            return new Rule() {
                public PC pc() {
                    return
                            AND(
                                    Zahl(),
                                    Zahl(),
                                    Zahl(),
                                    Zahl()
                            );
                }
            };
        }

        public Rule NFachZahl() {
            return new Rule() {
                public PC pc() {
                    return
                            AND(
                                    NFOLD(Zahl())
                            );
                }
            };
        }

        public Rule Zahl() {
            return new Rule() {
                public PC pc() {
                    return
                            OR(
                                    TOKEN(zahl0()),
                                    TOKEN(zahl1()),
                                    TOKEN(zahl2()),
                                    TOKEN(zahl3()),
                                    TOKEN(zahl4()),
                                    TOKEN(zahl5())
                            );
                }
            };
        }

        public Rule Newline_nFach() {
            return new Rule() {
                public PC pc() {
                    return
                            AND(
                                    NFOLD(Liste()),
                                    TOKEN(NEWLINE()), TOKEN(NEWLINE())
                            );
                }
            };
        }

        public Rule Liste() {
            return new Rule() {
                public PC pc() {
                    return
                            AND(
                                    TOKEN(Dies()),
                                    Zahl(),
                                    TOKEN(Punkt()),
                                    TOKEN(Zeile()),
                                    TOKEN(NEWLINE())
                            );
                }
            };
        }

        public Rule Loeschen() {
            return new Rule() {
                public PC pc() {
                    return
                            AND(
                                    Zahl(),
                                    NFOLD(Zahl())
                            );
                }
            };
        }


    }

}
