package damncooleditorhackers.model.slopeGrammars;

import slope.PC;
import slope.Parser;
import slope.Rule;
import slope.lib.grammar.GrammarParsing;
import slowscane.Tokens;
import slowscane.annotations.TokenLevel;
import slowscane.scanners.KeywordScanner;
import slowscane.scanners.TokenReader;
import slowscane.scanners.lib.JavaStringLiteralScanner;

/**
 * Created by thorb on 30.09.2016.
 */
public class GrammarTestText extends Tokens {

    private final TokenReader<String> trA = new KeywordScanner("A");
    private final TokenReader<String> trB = new KeywordScanner("B");
    private final TokenReader<String> trC = new KeywordScanner("C");
    private final TokenReader<String> trgetrennt = new KeywordScanner("Getrennter");
    private final TokenReader<String> trSatz = new KeywordScanner("Satz");
    private final TokenReader<String> trSirene = new KeywordScanner("tatütata");
    private final TokenReader<String> trda = new KeywordScanner("Lass mich da");
    private final TokenReader<String> trweg = new KeywordScanner("Lösch mich");
    //private final TokenReader<String> whiteSpace = new KeywordScanner(" ");


    @TokenLevel(value = 90)
    public TokenReader<?> NEWLINE() {
        return lib.NEWLINE;
    }

    @TokenLevel(value = 91)
    public TokenReader<String> A() {
        return trA;
    }

    @TokenLevel(value = 92)
    public TokenReader<String> B() {
        return trB;
    }

    @TokenLevel(value = 93)
    public TokenReader<String> C() {
        return trC;
    }

    @TokenLevel(value = 94)
    public TokenReader<String> Getrennt() {
        return trgetrennt;
    }

    @TokenLevel(value = 95)
    public TokenReader<String> Zeile() {
        return trSatz;
    }

    @TokenLevel(value = 96)
    public TokenReader<String> Sirene() {
        return trSirene;
    }

    @TokenLevel(value = 97)
    public TokenReader<String> Da() {
        return trda;
    }

    @TokenLevel(value = 98)
    public TokenReader<String> Weg() {
        return trweg;
    }

    @TokenLevel(value = 99)
    public TokenReader<String> JAVA_STRING_LITERAL() {
        return new JavaStringLiteralScanner();
    }


    public class Grammar01Parser extends Parser<GrammarParsing> {


        public Rule Grammar() {
            return new Rule() {
                public PC pc() {

                    return
                            AND(
                                    Texteingabe(),
                                    Logik(),
                                    Klammern(),
                                    NeweLine(),
                                    nFach(),
                                    OptionalesElement()
                            );
                }
            };
        }


        public Rule Texteingabe() {
            return new Rule() {
                public PC pc() {
                    return
                            AND(

                                    TOKEN(JAVA_STRING_LITERAL()),
                                    TOKEN(NEWLINE())
                            );
                }
            };
        }

        public Rule Logik() {
            return new Rule() {
                public PC pc() {
                    return
                            AND(
                                    OR(
                                            TOKEN(A()),
                                            AND(
                                            TOKEN(B()),
                                            TOKEN(C())
                                            )
                                    ),
                                    TOKEN(NEWLINE())
                            );
                }
            };
        }

        public Rule Klammern() {
            return new Rule() {
                public PC pc() {
                    return
                            AND(
                                    OR(
                                            TOKEN(A()),
                                            TOKEN(B())
                                    ),
                                    TOKEN(C()),
                                    TOKEN(NEWLINE())
                            );
                }
            };
        }

        public Rule NeweLine() {
            return new Rule() {
                public PC pc() {
                    return
                            AND(
                                    TOKEN(Getrennt()),
                                    TOKEN(NEWLINE()),
                                    TOKEN(Zeile()),
                                    TOKEN(NEWLINE())
                            );
                }
            };
        }

        public Rule nFach() {
            return new Rule() {
                public PC pc() {
                    return
                            AND(
                                    NFOLD(TOKEN(Sirene())),
                                    TOKEN(NEWLINE())
                            );
                }
            };
        }

        public Rule OptionalesElement() {
            return new Rule() {
                public PC pc() {
                    return
                            AND(
                                    TOKEN(Da()),
                                    OPTIONAL(TOKEN(Weg()))
                            );
                }
            };
        }


    }
}
