package damncooleditorhackers.model.slopeGrammars;

import slope.PC;
import slope.Parser;
import slope.Rule;
import slope.lib.grammar.GrammarParsing;
import slowscane.Tokens;
import slowscane.annotations.TokenLevel;
import slowscane.scanners.KeywordScanner;
import slowscane.scanners.TokenReader;
import slowscane.scanners.lib.JavaStringLiteralScanner;

/**
 * Created by Niklas on 16.09.2016.
 */
public class GrammarWebsite extends Tokens {

    public final TokenReader<String> persoenlWebsite = new KeywordScanner("Persönliche Website von");
    public final TokenReader<String> DisQ = new KeywordScanner("D ");
    public final TokenReader<String> komma = new KeywordScanner(",");
    public final TokenReader<String> telNr = new KeywordScanner("Tel.: ");
    public final TokenReader<String> sumPreisGeld = new KeywordScanner("=sum(Preisgeld)");
    public final TokenReader<String> gesamt = new KeywordScanner("gesamt");
    public final TokenReader<String> letztesJahr = new KeywordScanner("letztes Jahr");
    public final TokenReader<String> threeT = new KeywordScanner("(3T)");
    public final TokenReader<String> eur = new KeywordScanner("EUR");
    public final TokenReader<String> fiveT = new KeywordScanner("(5T)");
    public final TokenReader<String> teamart = new KeywordScanner("Teamart");
    public final TokenReader<String> sponsorengeld = new KeywordScanner("Sponsorengeld");
    public final TokenReader<String> gesamtText = new KeywordScanner("Gesamt");
    public final TokenReader<String> teamanteilText = new KeywordScanner("Teamanteil");
    public final TokenReader<String> fiveTeam = new KeywordScanner("5er-Teams");
    public final TokenReader<String> threeTeam = new KeywordScanner("3er-Teams");
    public final TokenReader<String> geldGesamt = new KeywordScanner("=sum(TeamAnteile + Sponsorengeld)");
    public final TokenReader<String> uhrzeit = new KeywordScanner("Uhrzeit: ");
    public final TokenReader<String> colon = new KeywordScanner(":");
    public final TokenReader<String> eSpIdNr = new KeywordScanner("eSp-IdNr.: DE ");
    public final TokenReader<String> fiveWhites = new KeywordScanner("     ");
    public final TokenReader<String> fourWhites = new KeywordScanner("    ");
    public final TokenReader<String> whiteSpace = new KeywordScanner(" ");
    public final TokenReader<String> minus = new KeywordScanner("-");
    public final TokenReader<String> period = new KeywordScanner(".");

    public final TokenReader<String> zahl0 = new KeywordScanner("0");
    public final TokenReader<String> zahl1 = new KeywordScanner("1");
    public final TokenReader<String> zahl2 = new KeywordScanner("2");
    public final TokenReader<String> zahl3 = new KeywordScanner("3");
    public final TokenReader<String> zahl4 = new KeywordScanner("4");
    public final TokenReader<String> zahl5 = new KeywordScanner("5");
    public final TokenReader<String> zahl6 = new KeywordScanner("6");
    public final TokenReader<String> zahl7 = new KeywordScanner("7");
    public final TokenReader<String> zahl8 = new KeywordScanner("8");
    public final TokenReader<String> zahl9 = new KeywordScanner("9");

    @TokenLevel(value = 66)
    public TokenReader<String> eur() {
        return eur;
    }

    @TokenLevel(value = 67)
    public TokenReader<String> gesamt() {
        return gesamt;
    }

    @TokenLevel(value = 68)
    public TokenReader<String> teamart() {
        return teamart;
    }

    @TokenLevel(value = 69)
    public TokenReader<String> sponsorengeld() {
        return sponsorengeld;
    }

    @TokenLevel(value = 70)
    public TokenReader<String> gesamtText() {
        return gesamtText;
    }

    @TokenLevel(value = 71)
    public TokenReader<String> teamanteilText() {
        return teamanteilText;
    }

    @TokenLevel(value = 72)
    public TokenReader<String> persoenlWebsite() {
        return persoenlWebsite;
    }

    @TokenLevel(value = 73)
    public TokenReader<String> threeT() {
        return threeT;
    }

    @TokenLevel(value = 74)
    public TokenReader<String> fiveT() {
        return fiveT;
    }

    @TokenLevel(value = 75)
    public TokenReader<String> fiveTeam() {
        return fiveTeam;
    }

    @TokenLevel(value = 76)
    public TokenReader<String> threeTeam() {
        return threeTeam;
    }

    @TokenLevel(value = 77)
    public TokenReader<String> zahl0() {
        return zahl0;
    }

    @TokenLevel(value = 78)
    public TokenReader<String> zahl1() {
        return zahl1;
    }

    @TokenLevel(value = 79)
    public TokenReader<String> zahl2() {
        return zahl2;
    }

    @TokenLevel(value = 80)
    public TokenReader<String> zahl3() {
        return zahl3;
    }

    @TokenLevel(value = 81)
    public TokenReader<String> zahl4() {
        return zahl4;
    }

    @TokenLevel(value = 82)
    public TokenReader<String> zahl5() {
        return zahl5;
    }

    @TokenLevel(value = 83)
    public TokenReader<String> zahl6() {
        return zahl6;
    }

    @TokenLevel(value = 84)
    public TokenReader<String> zahl7() {
        return zahl7;
    }

    @TokenLevel(value = 85)
    public TokenReader<String> zahl8() {
        return zahl8;
    }

    @TokenLevel(value = 86)
    public TokenReader<String> zahl9() {
        return zahl9;
    }

    @TokenLevel(value = 87)
    public TokenReader<String> komma() {
        return komma;
    }

    @TokenLevel(value = 88)
    public TokenReader<String> period() {
        return period;
    }

    @TokenLevel(value = 89)
    public TokenReader<?> NEWLINE() {
        return lib.NEWLINE;
    }

    @TokenLevel(value = 90)
    public TokenReader<String> letztesJahr() {
        return letztesJahr;
    }

    @TokenLevel(value = 91)
    public TokenReader<String> telNr() {
        return telNr;
    }

    @TokenLevel(value = 92)
    public TokenReader<String> sumPreisGeld() {
        return sumPreisGeld;
    }

    @TokenLevel(value = 93)
    public TokenReader<String> DisQ() {
        return DisQ;
    }

    @TokenLevel(value = 94)
    public TokenReader<String> geldGesamt() {
        return geldGesamt;
    }

    @TokenLevel(value = 95)
    public TokenReader<String> uhrzeit() {
        return uhrzeit;
    }

    @TokenLevel(value = 96)
    public TokenReader<String> colon() {
        return colon;
    }

    @TokenLevel(value = 97)
    public TokenReader<String> eSpIdNr() {
        return eSpIdNr;
    }

    @TokenLevel(value = 97)
    public TokenReader<String> fiveWhites() {
        return fiveWhites;
    }

    @TokenLevel(value = 98)
    public TokenReader<String> fourWhites() {
        return fourWhites;
    }

    @TokenLevel(value = 99)
    public TokenReader<String> whiteSpace() {
        return whiteSpace;
    }

    @TokenLevel(value = 99)
    public TokenReader<String> minus() {
        return minus;
    }

    @TokenLevel(value = 99)
    public TokenReader<String> JAVA_STRING_LITERAL() {
        return new JavaStringLiteralScanner();
    }

    public class Grammar01Parser extends Parser<GrammarParsing> {

        /*
               Start -> Kopf "\n" Liste+ "\n" Erfolg "\n" Gruppierung "\n" Zeitstempel "\n" ESportsID "\n" Extras.
         */
        public Rule Grammar() {
            return new Rule() {
                public PC pc() {

                    return
                            AND(
                                    Kopf(), TOKEN(NEWLINE()),
                                    NFOLD(Liste()), TOKEN(NEWLINE()),
                                    Erfolg(), TOKEN(NEWLINE()),
                                    Gruppierung(), TOKEN(NEWLINE()),
                                    Zeitstempel(), TOKEN(NEWLINE()),
                                    ESportsID(), TOKEN(NEWLINE()),
                                    Extras()
                            );
                }
            };
        }

        /*
             Kopf -> "Persönliche Website von" "\n" Name.
         */
        public Rule Kopf() {
            return new Rule() {
                public PC pc() {
                    return
                            AND(
                                    TOKEN(persoenlWebsite()),
                                    TOKEN(NEWLINE()),
                                    Name()
                            );
                }
            };
        }

        /*
             Name -> MitgliedsID " " Spitzname "\n" Vollstaendigername "\n" Telefonnummer "\n\n".
         */
        public Rule Name() {
            return new Rule() {
                public PC pc() {
                    return
                            AND(
                                    MitgliedsID(),
                                    TOKEN(" "),
                                    Spitzname(),
                                    TOKEN(NEWLINE()),
                                    Vollstaendigername(),
                                    TOKEN(NEWLINE()),
                                    Telefonnummer(),
                                    TOKEN(NEWLINE()),
                                    TOKEN(NEWLINE())
                            );
                }
            };
        }

        /*
             MitgliedsID -> Zahl Zahl Zahl Zahl Zahl.
         */
        public Rule MitgliedsID() {
            return new Rule() {
                public PC pc() {
                    return
                            AND(
                                    Zahl(),
                                    Zahl(),
                                    Zahl(),
                                    Zahl(),
                                    Zahl()
                            );
                }
            };
        }

        /*
             Zahl -> "0"|"1"|"2"|"3"|"4"|"5"|"6"|"7"|"8"|"9"..
         */
        public Rule Zahl() {
            return new Rule() {
                public PC pc() {
                    return
                            OR(
                                    TOKEN("0"),
                                    TOKEN("1"),
                                    TOKEN("2"),
                                    TOKEN("3"),
                                    TOKEN("4"),
                                    TOKEN("5"),
                                    TOKEN("6"),
                                    TOKEN("7"),
                                    TOKEN("8"),
                                    TOKEN("9")
                            );
                }
            };
        }

        /*
            Spitzname -> JAVA_STRING_LITERAL.
        */
        public Rule Spitzname() {
            return new Rule() {
                public PC pc() {
                    return
                            TOKEN(JAVA_STRING_LITERAL());
                }
            };
        }

        /*
            Vollstaendigername -> JAVA_STRING_LITERAL.
        */
        public Rule Vollstaendigername() {
            return new Rule() {
                public PC pc() {
                    return
                            TOKEN(JAVA_STRING_LITERAL());
                }
            };
        }

        /*
            Telefonnummer -> "Tel.: " Zahl+.
        */
        public Rule Telefonnummer() {
            return new Rule() {
                public PC pc() {
                    return
                            AND(
                                    TOKEN("Tel.: "),
                                    NFOLD(Zahl())
                            );
                }
            };
        }

        /*
            Typ -> "gesamt" | "letztes Jahr".
        */
        public Rule Typ() {
            return new Rule() {
                public PC pc() {
                    return
                            OR(
                                    TOKEN("gesamt"),
                                    TOKEN("letztes Jahr")
                            );
                }
            };
        }

        /*
            Liste -> DisQ? Turniername " " " " Preisgeld "\n".
        */
        public Rule Liste() {
            return new Rule() {
                public PC pc() {
                    return
                            AND(
                                    OPTIONAL(DisQ()),
                                    Turniername(),
                                    TOKEN(" "),
                                    TOKEN(" "),
                                    Preisgeld(),
                                    TOKEN(NEWLINE())
                            );
                }
            };
        }

        /*
            DisQ -> "D ".
        */
        public Rule DisQ() {
            return new Rule() {
                public PC pc() {
                    return
                            TOKEN("D ");
                }
            };
        }

        /*
            Turniername -> JAVA_STRING_LITERAL.
        */
        public Rule Turniername() {
            return new Rule() {
                public PC pc() {
                    return
                            TOKEN(JAVA_STRING_LITERAL());
                }
            };
        }

        /*
            Stunden -> "0" Zahl|"1" Zahl|"2" ZahlBisDrei.
        */
        public Rule Stunden() {
            return new Rule() {
                public PC pc() {
                    return
                            OR(
                                    AND(TOKEN("0"), Zahl()),
                                    AND(TOKEN("1"), Zahl()),
                                    AND(TOKEN("2"), ZahlBisDrei())
                            );
                }
            };
        }

        /*
            Minuten -> ("0"|"1"|"2"|"3"|"4"|"5") Zahl.
        */
        public Rule Minuten() {
            return new Rule() {
                public PC pc() {
                    return
                            AND(
                                    OR(
                                            TOKEN("0"),
                                            TOKEN("1"),
                                            TOKEN("2"),
                                            TOKEN("3"),
                                            TOKEN("4"),
                                            TOKEN("5")
                                    ),
                                    Zahl()
                            );
                }
            };
        }

        /*
            Tag -> "0" Zahl|"1" Zahl|"2" Zahl|"3""0"|"3""1".
        */
        public Rule Tag() {
            return new Rule() {
                public PC pc() {
                    return
                            OR(
                                    AND(TOKEN("0"), Zahl()),
                                    AND(TOKEN("1"), Zahl()),
                                    AND(TOKEN("2"), Zahl()),
                                    AND(TOKEN("3"), TOKEN("0")),
                                    AND(TOKEN("3"), TOKEN("1"))
                            );
                }
            };
        }

        /*
            Monat -> "0" Zahl|"1"0"|"1""1"|"1""2".
        */
        public Rule Monat() {
            return new Rule() {
                public PC pc() {
                    return
                            OR(
                                    AND(TOKEN("0"), Zahl()),
                                    AND(TOKEN("1"), TOKEN("0")),
                                    AND(TOKEN("1"), TOKEN("1")),
                                    AND(TOKEN("1"), TOKEN("2"))
                            );
                }
            };
        }

        /*
            ZahlBisDrei -> "0" | "1" | "2" | "3".
        */
        public Rule ZahlBisDrei() {
            return new Rule() {
                public PC pc() {
                    return
                            OR(
                                    TOKEN("0"),
                                    TOKEN("1"),
                                    TOKEN("2"),
                                    TOKEN("3")
                            );
                }
            };
        }

        /*
            Teamart -> "(5T)" | "(3T)".
        */
        public Rule Teamart() {
            return new Rule() {
                public PC pc() {
                    return
                            OR(
                                    TOKEN("(5T)"),
                                    TOKEN("(3T)")
                            );
                }
            };
        }

        /*
            Preisgeld -> ("-"|" ") Betrag " " Teamart.
        */
        public Rule Preisgeld() {
            return new Rule() {
                public PC pc() {
                    return
                            AND(
                                    OR(TOKEN("-"), TOKEN(" ")),
                                    Betrag(),
                                    TOKEN(" "),
                                    Teamart()
                            );
                }
            };
        }

        /*
            Betrag -> Zahl+ "," Zahl Zahl.
        */
        public Rule Betrag() {
            return new Rule() {
                public PC pc() {
                    return
                            AND(
                                    NFOLD(Zahl()),
                                    TOKEN(","),
                                    Zahl(),
                                    Zahl()
                            );
                }
            };
        }

        /*
            Erfolg -> Typ Waehrung " " =sum(Preisgeld)".
        */
        public Rule Erfolg() {
            return new Rule() {
                public PC pc() {
                    return
                            AND(
                                    Typ(),
                                    Waehrung(),
                                    TOKEN(" "),
                                    TOKEN("=sum(Preisgeld)")
                            );
                }
            };
        }

        /*
            Waehrung -> " " EUR".
        */
        public Rule Waehrung() {
            return new Rule() {
                public PC pc() {
                    return
                            AND(
                                    TOKEN(" "),
                                    TOKEN("EUR")
                            );
                }
            };
        }

        /*
            Gruppierung -> "Teamart" "    " "Teamanteil" "    " "Sponsorengeld" "    " "Gesamt" AnteileListe "\n\n".
        */
        public Rule Gruppierung() {
            return new Rule() {
                public PC pc() {
                    return
                            AND(
                                    TOKEN("Teamart"), TOKEN("    "),
                                    TOKEN("Teamanteil"), TOKEN("    "),
                                    TOKEN("Sponsorengeld"), TOKEN("    "),
                                    TOKEN("Gesamt"),
                                    TOKEN(NEWLINE()),
                                    AnteileListe(),
                                    TOKEN(NEWLINE())
                            );
                }
            };
        }

        /*
            AnteileListe -> "\n"( "5er-Teams" "     " TeamAnteile "     "Sponsorengeld "     " Gesamt)? "\n"( "3er-Teams" "     " TeamAnteile "     "Sponsorengeld "     " Gesamt)?.
        */
        public Rule AnteileListe() {
            return new Rule() {
                public PC pc() {
                    return
                            AND(
                                    AND(
                                            OPTIONAL(
                                                    AND(
                                                            TOKEN("5er-Teams"),
                                                            TOKEN("     "),
                                                            TeamAnteile(),
                                                            TOKEN("     "),
                                                            Sponsorengeld(),
                                                            TOKEN("     "),
                                                            Gesamt()
                                                    )
                                            ),
                                            TOKEN(NEWLINE())
                                    ),
                                    AND(
                                            OPTIONAL(
                                                    AND(
                                                            TOKEN("3er-Teams"),
                                                            TOKEN("     "),
                                                            TeamAnteile(),
                                                            TOKEN("     "),
                                                            Sponsorengeld(),
                                                            TOKEN("     "),
                                                            Gesamt()
                                                    )
                                            ),
                                            TOKEN(NEWLINE())
                                    )
                            );
                }
            };
        }

        /*
            TeamAnteile -> Betrag.
        */
        public Rule TeamAnteile() {
            return new Rule() {
                public PC pc() {
                    return
                            Betrag();
                }
            };
        }

        /*
            Sponsorengeld -> Betrag.
        */
        public Rule Sponsorengeld() {
            return new Rule() {
                public PC pc() {
                    return
                            Betrag();
                }
            };
        }

        /*
            Gesamt -> "=sum(TeamAnteile + Sponsorengeld)".
        */
        public Rule Gesamt() {
            return new Rule() {
                public PC pc() {
                    return
                            TOKEN("=sum(TeamAnteile + Sponsorengeld)");
                }
            };
        }

        /*
            Zeitstempel -> Datum "\n" Uhrzeit.
        */
        public Rule Zeitstempel() {
            return new Rule() {
                public PC pc() {
                    return
                            AND(
                                    Datum(),
                                    TOKEN(NEWLINE()),
                                    Uhrzeit()
                            );
                }
            };
        }

        /*
            Datum -> Tag"."Monat".""2""0" Zahl Zahl.
        */
        public Rule Datum() {
            return new Rule() {
                public PC pc() {
                    return
                            AND(
                                    Tag(),
                                    TOKEN("."),
                                    Monat(),
                                    TOKEN("."),
                                    AND(TOKEN("2"), TOKEN("0")),
                                    Zahl(),
                                    Zahl()
                            );
                }
            };
        }

        /*
            Uhrzeit -> "Uhrzeit: "Stunden":"Minuten.
        */
        public Rule Uhrzeit() {
            return new Rule() {
                public PC pc() {
                    return
                            AND(
                                    TOKEN("Uhrzeit: "),
                                    Stunden(),
                                    TOKEN(":"),
                                    Minuten()
                            );
                }
            };
        }

        /*
            ESportsID -> "eSp-IdNr.: DE " Zahl+.
        */
        public Rule ESportsID() {
            return new Rule() {
                public PC pc() {
                    return
                            AND(
                                    TOKEN("eSp-IdNr.: DE "),
                                    NFOLD(Zahl())
                            );
                }
            };
        }

        /*
            Extras -> JAVA_STRING_LITERAL.
        */
        public Rule Extras() {
            return new Rule() {
                public PC pc() {
                    return
                            TOKEN(JAVA_STRING_LITERAL());
                }
            };
        }

    }


}
