package damncooleditorhackers.view;

import damncooleditorhackers.model.grammarElements.Element;
import damncooleditorhackers.model.grammarElements.Op;
import damncooleditorhackers.model.grammarElements.TokenElement;
import damncooleditorhackers.model.grammarElements.Type;
import damncooleditorhackers.view.view_elements.ElementButton;
import damncooleditorhackers.view.view_elements.ElementTextField;
import damncooleditorhackers.view.view_elements.LineJPanel;

import java.util.ArrayList;
import java.util.HashMap;

import static damncooleditorhackers.view.Cursor.SelectionMode.MULTI;
import static damncooleditorhackers.view.Cursor.SelectionMode.SINGLE;

/**
 * Created by Nick on 30.06.2016.
 */
public class Cursor {
    private final CursorTraversal view;
    private HashMap<Integer, ElementButton> selection;
    private ArrayList<ArrayList<ElementButton>> buttons;
    private int lineIndex, lastSelectedIndex;
    private boolean inSelectionDialog;

    public Cursor(CursorTraversal parent, boolean inSelectionDialog) {
        selection = new HashMap<Integer, ElementButton>();
        this.buttons = new ArrayList<ArrayList<ElementButton>>();
        this.view = parent;
        this.lineIndex = 0;
        this.lastSelectedIndex = 0;
        this.inSelectionDialog = inSelectionDialog;
    }

    public void reselect() {
        if (buttons.size() == 0) return;
        if (inSelectionDialog || selection.size() == 0) {
            selectNearestElement(0);
        } else if (selection.size() >= 1) {
            int first = getFirst();
            deselectAll();
            if (lastSelectedIndex != first) selectNearestElement(first);
            else selectNearestElement(first + 1);
        }
        lastSelectedIndex = getFirst();
    }

    public void deselect(ElementButton button) {
        button.deselect();
        if (buttons.get(lineIndex).contains(button)) selection.remove(buttons.get(lineIndex).indexOf(button));
    }

    private void deselectAll() {
        for (ArrayList<ElementButton> lineButtons : buttons) {
            Integer[] selectedIndices = selection.keySet().toArray(new Integer[selection.keySet().size()]);
            for (int i : selectedIndices) {
                if (i < lineButtons.size() && i >= 0)
                    lineButtons.get(i).deselect();
            }
        }
        selection.clear();
    }

    public void select(ElementButton button) {
        setLineIndex(button);
        deselectAll();
        lastSelectedIndex = buttons.get(lineIndex).indexOf(button);
        selection.put(buttons.get(lineIndex).indexOf(button), button);
        button.select(SINGLE);
        view.scrollToComponent(button);
    }

    public void addSelect(ElementButton button) {
        if (button.getLineIndex() != lineIndex) select(button);
        else {
            if (hasSelection()) {
                int index = getFirst();
                int endIndex = buttons.get(lineIndex).indexOf(button);
                if (index < endIndex) {
                    while (index != endIndex) {
                        if (check(index)) addSelect(index);
                        index++;
                    }
                } else {
                    while (index != endIndex) {
                        if (check(index)) addSelect(index);
                        index--;
                    }
                }
                button.select(MULTI);
                selection.put(endIndex, button);
            } else select(button);
        }
    }

    private void addSelect(int index) {
        if (check(index)) {
            ElementButton button = buttons.get(lineIndex).get(index);
            button.select(MULTI);
            selection.put(index, button);
        } else selection.remove(index);
    }

    public void addRight(int index) {
        if (selection.size() > 0) {
            if (check(index + 1)) addSelect(getButton(index + 1));
            else if (index + 1 < buttons.get(lineIndex).size()) addRight(index + 1);
        }
    }

    public void addLeft(int index) {
        if (selection.size() > 0) {
            if (check(index - 1)) addSelect(getButton(index - 1));
            else if (index - 1 > 0) addLeft(index - 1);
        }
    }

    public void moveRight() {
        if (selection.size() > 0)
            moveRight(getFirst());
    }

    public void moveLeft() {
        if (selection.size() > 0)
            moveLeft(getFirst());
    }

    public void moveToEnd() {
        deselectAll();
        int index = buttons.get(lineIndex).size() - 1;
        while (!check(index) && index > 0) index--;
        select(getButton(index));
    }

    public void moveToFirst() {
        deselectAll();
        select(getButton(0));
    }

    public void moveUp() {
        if (selection.size() > 0) {
            if (lineIndex > 0) {
                lineIndex--;
                selectNearestElementUp(getFirst());
            }
        }
    }

    public void moveDown() {
        if (selection.size() > 0) {
            if (inSelectionDialog) view.closeDialog();
            else if (lineIndex < buttons.size() - 1) {
                lineIndex++;
                selectNearestElementDown(getFirst());
            }
        }
    }

    private void moveRight(int index) {
        if (check(index + 1)) {
            deselectAll();
            select(getButton(index + 1));
        } else if (index + 1 < buttons.get(lineIndex).size() && !getButton(index + 1).isEnabled() || index + 1 < buttons.get(lineIndex).size() && getButton(index + 1).getElement().getType() == Type.ADD_RIGHT)
            moveRight(index + 1);
        else if (index + 1 == buttons.get(lineIndex).size() && lineIndex < buttons.size() - 1) {
            moveToFirst();
            moveDown();
        }
    }

    private void moveLeft(int index) {
        if (check(index - 1)) {
            deselectAll();
            select(getButton(index - 1));
        } else if (index - 1 >= 0 && !getButton(index - 1).isEnabled() || index - 1 >= 0 && getButton(index - 1).getElement().getType() == Type.ADD_RIGHT)
            moveLeft(index - 1);
        else if (index - 1 == -1 && lineIndex != 0) {
            moveUp();
            moveToEnd();
        }
    }

    public void moveToNextEditable() {
        int index = getFirst();
        if (index + 1 < buttons.get(lineIndex).size()) index++;
        else if (lineIndex + 1 < buttons.size()) {
            index = 0;
            lineIndex++;
        } else return;
        while (notRequiresEditing(getButton(index))) {
            if (index + 1 < buttons.get(lineIndex).size()) index++;
            else if (lineIndex + 1 < buttons.size()) {
                index = 0;
                lineIndex++;
            } else break;
        }
        if (lineIndex < buttons.size()) selectNearestElement(index);
    }

    private boolean notRequiresEditing(ElementButton button) {
        return (button.getElement().getType() == Type.TOKEN && !((TokenElement) button.getElement()).isJavaScanner() ||
                button.getElement().getType() == Type.TOKEN && ((TokenElement) button.getElement()).isJavaScanner() && ((TokenElement) button.getElement()).isSet() ||
                button.getElement().getType() == Type.ADD_RIGHT ||
                button.getElement().getType() == Type.OPTIONAL && button.getText().equals("?"));
    }

    private void selectNearestElementUp(int index) {
        if (lineIndex >= buttons.size()) moveUp();
        if (check(index)) {
            deselectAll();
            select(getButton(index));
        } else {
            deselectAll();
            if (check(index - 1)) {
                select(getButton(index - 1));
            } else if (index == 0 && !check(index) && lineIndex - 1 >= 0) {
                lineIndex--;
                selectNearestElementUp(index);
            } else selectNearestElementUp(buttons.get(lineIndex).size() - 1);
        }
    }

    private void selectNearestElementDown(int index) {
        if (lineIndex >= buttons.size()) moveUp();
        if (check(index)) {
            deselectAll();
            select(getButton(index));
        } else {
            deselectAll();
            if (check(index - 1)) {
                select(getButton(index - 1));
            } else if (index == 0 && !check(index) && lineIndex + 1 < buttons.size()) {
                lineIndex++;
                selectNearestElementDown(index);
            } else selectNearestElementDown(buttons.get(lineIndex).size() - 1);
        }
    }

    private void selectNearestElement(int index) {
        if (lineIndex >= buttons.size()) moveUp();
        if (check(index)) {
            deselectAll();
            select(getButton(index));
        } else {
            deselectAll();
            if (check(index - 1)) {
                select(getButton(index - 1));
            } else if (index == 0 && !check(index) && lineIndex + 1 < buttons.size()) {
                lineIndex++;
                selectNearestElement(index);
            } else selectNearestElement(buttons.get(lineIndex).size() - 1);
        }
    }

    private boolean check(int index) {
        return index >= 0 && lineIndex < buttons.size() && index < buttons.get(lineIndex).size() && getButton(index).isEnabled() && getButton(index).getElement().getType() != Type.ADD_RIGHT;
    }

    public void closeDialog() {
        if (inSelectionDialog) view.closeDialog();
    }

    public void updateButtons() {
        buttons.clear();
        for (LineJPanel r : view.getPanels()) {
            buttons.add(new ArrayList<ElementButton>(r.getButtons()));
        }
    }

    public ArrayList<ElementButton> getSelection() {
        ArrayList<ElementButton> temp = new ArrayList<ElementButton>();
        temp.addAll(selection.values());
        return temp;
    }

    private void setLineIndex(ElementButton button) {
        lineIndex = button.getLineIndex();
        if (lineIndex == -1) {
            lineIndex = 0;
            inSelectionDialog = true;
        }
    }

    private ElementButton getButton(int index) {
        return buttons.get(lineIndex).get(index);
    }

    public ArrayList<ElementButton> getUpdatedSelection() {
        int startIndex = getFirst();
        Element element = getButton(startIndex).getElement();
        if (element.getOperator() != Op.OR) {
            ArrayList<Element> children = element.getParent().getChildren();
            if (!getSelection().containsAll(children)) {
                for (int i = 0; i < buttons.get(lineIndex).size(); i++) {
                    if (children.contains(getButton(i).getElement()) && getButton(i).getElement().getType() != Type.ADD_RIGHT)
                        addSelect(i);
                }
            }
        }
        return getSelection();
    }

    public boolean inSelectionDialog() {
        return inSelectionDialog;
    }

    public boolean hasSelection() {
        return selection.size() > 0;
    }

    public boolean isOnButtons() {
        if (view.getFocusedComponent() == null) return false;
        String focusedClass = view.getFocusedComponent().getClass().getName();
        return !(focusedClass.equals(ElementTextField.class.getName()));
    }


    public int getFirst() {
        return selection.keySet().toArray(new Integer[selection.keySet().size()])[0];
    }

    public int getLast() {
        Integer[] integers = selection.keySet().toArray(new Integer[selection.keySet().size()]);
        return integers[integers.length - 1];
    }

    public void decreaseLastIndex() {
        lastSelectedIndex--;
    }

    public enum SelectionMode {
        SINGLE,
        MULTI
    }
}
