package damncooleditorhackers.view;

import damncooleditorhackers.view.view_elements.ElementButton;
import damncooleditorhackers.view.view_elements.LineJPanel;

import java.util.ArrayList;

/**
 * Created by Nick on 01.07.2016.
 */
public interface CursorTraversal {
    ArrayList<LineJPanel> getPanels();

    void closeDialog();

    Object getFocusedComponent();

    void scrollToComponent(ElementButton button);
}
