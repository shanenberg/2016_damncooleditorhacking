package damncooleditorhackers.view;

import damncooleditorhackers.EditorGrammar;
import damncooleditorhackers.controller.Logger;
import damncooleditorhackers.model.grammarElements.Element;
import damncooleditorhackers.view.listener.CursorListener;
import damncooleditorhackers.view.listener.ExperimentListeners;
import damncooleditorhackers.view.listener.ViewElementListeners;
import damncooleditorhackers.view.view_elements.*;

import javax.swing.*;
import java.awt.*;
import java.io.*;
import java.util.ArrayList;

import static damncooleditorhackers.model.grammarElements.Type.TOKEN;

/**
 * Created by Thorben on 15.06.2016.
 */

@SuppressWarnings("unchecked")

public class EditorView implements ViewUpdater, CursorTraversal, ExperimentView {
    private ViewElementListeners viewListeners;
    private JFrame frame;
    private int width, height;
    private ArrayList<LineJPanel> lineJPanels;
    private final GridBagLayout gridBagLayout = new GridBagLayout();
    private JPanel mainContainerJPanel;
    private JScrollPane mainScrollPane;
    private SelectionDialog selectionDialog;
    private Cursor cursor;
    private CursorListener cursorListener;
    private KeyboardFocusManager manager;
    private EditorGrammar currentGrammar;
    private int[] oldElementHashes;
    private OverlayPane overlayPane;
    private TimingPanel timingPanel;
    private String userName = "Proband";
    private String targetInput;

    public void start(EditorGrammar startGrammar) {
        oldElementHashes = new int[0];
        currentGrammar = startGrammar;
        setTargetInput();
        lineJPanels = new ArrayList<LineJPanel>();
        viewListeners = new ViewElementListeners(this);
        overlayPane = new OverlayPane(this);
        setupCursor();
        width = (int) Toolkit.getDefaultToolkit().getScreenSize().getWidth();
        height = (int) Toolkit.getDefaultToolkit().getScreenSize().getHeight() - 40;

        frame = new JFrame("GrammarEditor - " + currentGrammar.name());
        frame.setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);
        frame.setExtendedState(JFrame.MAXIMIZED_BOTH);
        frame.setMinimumSize(new Dimension(800, 600));
        frame.setJMenuBar(getEditorMenuBar());
        frame.addWindowListener(viewListeners.getCloseListener(frame));
        JPanel contentPane;
        contentPane = getMainPane();
        frame.setContentPane(contentPane);
        viewListeners.createTree(startGrammar);
        lineJPanels.get(0).requestFocus();
        contentPane.setFocusable(false);
        frame.setFocusTraversalKeysEnabled(false);
        frame.setGlassPane(overlayPane);
        showOverlayPane("Start", timingPanel.getDescriptionFor("Start"));
        requestUserName();
        frame.setVisible(true);
    }

    private void setTargetInput() {
        InputStream inputStream;
        switch (currentGrammar) {
            case Kassenzettel:
                inputStream = getClass().getResourceAsStream("/damncooleditorhackers/view/view_elements/res/target_kassenzettel.txt");
                break;
            case Website:
                inputStream = getClass().getResourceAsStream("/damncooleditorhackers/view/view_elements/res/target_website.txt");
                break;
            case Testgrammatik_DACH:
                inputStream = getClass().getResourceAsStream("/damncooleditorhackers/view/view_elements/res/target_test_DACH.txt");
                break;
            case Testgrammatik_Text:
                inputStream = getClass().getResourceAsStream("/damncooleditorhackers/view/view_elements/res/target_test_Text.txt");
                break;
            case Example:
                inputStream = getClass().getResourceAsStream("/damncooleditorhackers/view/view_elements/res/target_test_Text.txt");
                break;
            default:
                JOptionPane.showMessageDialog(frame, "Fehler, keine Grammatik angegeben!");
                inputStream = null;
        }

        try {
            InputStreamReader fileInput = new InputStreamReader(inputStream, "UTF-8");
            BufferedReader reader = new BufferedReader(fileInput);
            String line;
            StringBuilder result = new StringBuilder();
            try {
                while ((line = reader.readLine()) != null) {
                    result.append(line).append("\n");
                }
                this.targetInput = result.toString().substring(0, result.lastIndexOf("\n"));
            } catch (IOException e) {
                e.printStackTrace();
            }
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
    }

    private void requestUserName() {
        NameInputDialog nameInputDialog = new NameInputDialog(this);
        nameInputDialog.show();
        setUserName(nameInputDialog.getName());
    }

    private void setupCursor() {
        cursor = new Cursor(this, false);
        cursorListener = new CursorListener(cursor, CursorListener.Mode.EDITORVIEW, viewListeners);
        manager = KeyboardFocusManager.getCurrentKeyboardFocusManager();
        manager.addKeyEventDispatcher(cursorListener);
    }

    public ArrayList<LineJPanel> getPanels() {
        return lineJPanels;
    }

    public void closeDialog() {
        closeDialogs();
    }

    public void updateSelectionDialog(ArrayList<Element> selectionElements, Point lastButtonLocation) {
        closeDialogs();
        if (selectionElements.size() > 0) {
            cursorListener.dialogVisible(true);
            selectionDialog = new SelectionDialog(this, lastButtonLocation, selectionElements, viewListeners);
            selectionDialog.setVisible(true);
        }
    }

    private boolean sameElements(ArrayList<Element> elements) {
        if (elements.size() != oldElementHashes.length) return false;
        for (int i = 0; i < oldElementHashes.length; i++) {
            if (oldElementHashes[i] != elements.get(i).hashCode())
                return false;
        }
        return true;
    }

    private void saveHashes(ArrayList<Element> elements) {
        oldElementHashes = new int[elements.size()];
        for (int i = 0; i < elements.size(); i++) {
            oldElementHashes[i] = elements.get(i).hashCode();
        }
    }

    public void updateElements(ArrayList<Element> ruleElements) {
        if (!sameElements(ruleElements)) {
            saveHashes(ruleElements);
            setupTitle();
            clearMainPane();
            int lines = 0;
            for (int i = 0; i < ruleElements.size(); i++) {
                Element e = ruleElements.get(i);
                LineJPanel panel = lineJPanels.get(lines);
                panel.add(new ElementJPanel(lines, e, viewListeners, cursor));
                if (e.getType() == TOKEN && e.toString().equals("\n") && !e.equals(ruleElements.get(ruleElements.size() - 1))) {
                    lines++;
                    addNewLineJPanel();
                    panel = lineJPanels.get(lines);
                    panel.removeAll();
                }
                lineJPanels.set(lines, panel);
            }
            mainScrollPane.validate();
            mainScrollPane.repaint();
            cursor.updateButtons();
            cursor.reselect();
        }
    }

    public JFrame getFrame() {
        return frame;
    }

    public int getWindowWidth() {
        return width;
    }

    private JMenuBar getEditorMenuBar() {
        JMenuBar menuBar = new JMenuBar();
        menuBar.add(new FileMenu("File", viewListeners));
        return menuBar;
    }

    private JSplitPane getToolbarTimingSplit() {
        JSplitPane splitPane = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT);
        splitPane.setDividerSize(0);
        splitPane.setLeftComponent(getEditorToolbar());
        timingPanel = new EditorTimingPanel(new FlowLayout(FlowLayout.RIGHT), new ExperimentListeners(), viewListeners.getView());
        timingPanel.setBackground(new Color(0, 0, 0, 0));
        splitPane.setRightComponent(timingPanel);
        return splitPane;
    }

    private JToolBar getEditorToolbar() {
        return new EditorToolBar(viewListeners);
    }

    private int[] getHeights() {
        int height_padding = 10;
        int height_ruleList = 0;
        int height_buttons = 50;
        return new int[]{height_buttons, height_padding, height_ruleList};
    }

    private int[] getWidths() {
        int temp_width = width - 110;
        int width_buttons = 40;
        int width_normal = temp_width / 4;
        int width_padding = 30;
        return new int[]{width_buttons, width_normal, width_normal, width_padding, width_normal, width_normal, width_buttons};
    }

    private void setGridBagLayoutContraints() {
        gridBagLayout.columnWidths = getWidths();
        gridBagLayout.rowHeights = getHeights();
        gridBagLayout.columnWeights = new double[]{1.0, 1.0, 1.0, 1.0, 1.0};
        gridBagLayout.rowWeights = new double[]{0.0, 0.0, 0.0, 1.0};
    }

    private JPanel getMainPane() {
        setGridBagLayoutContraints();
        JPanel contentPane = new JPanel();
        contentPane.setLayout(gridBagLayout);

        GridBagConstraints gbc_mainPane = new GridBagConstraints();
        gbc_mainPane.insets = new Insets(5, 5, 5, 5);
        gbc_mainPane.gridwidth = 6;
        gbc_mainPane.gridx = 1;
        gbc_mainPane.gridy = 3;
        gbc_mainPane.fill = GridBagConstraints.BOTH;

        mainContainerJPanel = new JPanel();
        BoxLayout layout = new BoxLayout(mainContainerJPanel, BoxLayout.Y_AXIS);

        mainContainerJPanel.setLayout(layout);
        mainContainerJPanel.setFocusable(true);
        mainContainerJPanel.setBackground(Color.WHITE);
        mainScrollPane = new JScrollPane(mainContainerJPanel, ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED,
                ScrollPaneConstants.HORIZONTAL_SCROLLBAR_ALWAYS);
        mainScrollPane.getVerticalScrollBar().setUnitIncrement(12);
        contentPane.add(mainScrollPane, gbc_mainPane);
        clearMainPane();

        GridBagConstraints gbc_tool_split_timing = new GridBagConstraints();
        gbc_tool_split_timing.gridwidth = 7;
        gbc_tool_split_timing.gridx = 0;
        gbc_tool_split_timing.gridy = 0;
        gbc_tool_split_timing.fill = GridBagConstraints.BOTH;
        contentPane.add(getToolbarTimingSplit(), gbc_tool_split_timing);

        return contentPane;
    }

    public int addNewLineJPanel() {
        LineJPanel addLineJPanel = new LineJPanel();
        addLineJPanel.setLayout(new FlowLayout(FlowLayout.LEFT));
        ((FlowLayout) addLineJPanel.getLayout()).setHgap(0);
        addLineJPanel.setAlignmentX(Component.LEFT_ALIGNMENT);
        addLineJPanel.setAlignmentY(Component.BOTTOM_ALIGNMENT);
        addLineJPanel.setBackground(Color.WHITE);
        lineJPanels.add(addLineJPanel);
        mainContainerJPanel.add(addLineJPanel);
        return lineJPanels.indexOf(addLineJPanel);
    }

    public boolean showConfirmationDialog(String action) {
        return JOptionPane.showOptionDialog(getFrame(), "Bist du sicher, dass du die " + action + " willst?", action.substring(0, 1).toUpperCase() + action.substring(1), JOptionPane.YES_NO_OPTION, JOptionPane.WARNING_MESSAGE, null, new String[]{"Yes", "No"}, "Yes") == 0;
    }

    public void clearMainPane() {
        mainContainerJPanel.removeAll();
        lineJPanels = new ArrayList<LineJPanel>();
        LineJPanel newLineJPanel = new LineJPanel();
        newLineJPanel.setLayout(new FlowLayout(FlowLayout.LEFT));
        newLineJPanel.setAlignmentX(Component.LEFT_ALIGNMENT);
        newLineJPanel.setBackground(Color.WHITE);
        lineJPanels.add(newLineJPanel);
        mainContainerJPanel.add(newLineJPanel);
    }

    public Cursor getViewCursor() {
        return cursor;
    }

    public void closeDialogs() {
        cursorListener.dialogVisible(false);
        if (selectionDialog != null && selectionDialog.isVisible()) {
            selectionDialog.deregisterKeyManager();
            selectionDialog.setVisible(false);
        }
    }

    public Object getFocusedComponent() {
        return manager.getFocusOwner();
    }

    public void scrollToComponent(ElementButton button) {
        button.getParentPanel().scrollRectToVisible(button.getBounds(null));
    }

    public int getWindowHeight() {
        return height;
    }

    public void checkUserInput() {
        overlayPane.setVisible(false);
        frame.requestFocusInWindow();
        Logger.log("OVERLAY ausgeblendet", timingPanel.getTimerString(), userName, currentGrammar);
        manager.addKeyEventDispatcher(cursorListener);
    }

    public void showOverlayPane(String mode, String text) {
        if (mode.equals("Stop")) cursorListener.endRequested();
        manager.removeKeyEventDispatcher(cursorListener);
        overlayPane.setVisible(true);
        overlayPane.showMode(mode, text);
        overlayPane.requestFocusInWindow();
    }

    public void hideOverlayPane() {
        overlayPane.setVisible(false);
        frame.requestFocusInWindow();
        manager.addKeyEventDispatcher(cursorListener);
        timingPanel.start();
    }

    public void restartTimer() {
        if (!timingPanel.isRunning()) {
            Logger.log("ENDE abgelehnt", timingPanel.getTimerString(), userName, currentGrammar);
            timingPanel.start();
        }
    }

    public void end() {
        timingPanel.end();
        Logger.log("ENDE bestätigt", timingPanel.getTimerString(), userName, currentGrammar);
    }

    public void abort() {
        timingPanel.end();
        Logger.log("EDITOR geschlossen", timingPanel.getTimerString(), userName, currentGrammar);
    }

    public TimingPanel getTimingPanel() {
        return timingPanel;
    }

    public void setUserName(String name) {
        userName = name;
        setupTitle();
    }

    public String getCurrentInput() {
        return viewListeners.getElementsAsString();
    }

    public void showInvalidDialog(String message, String title) {
        deregisterKeyListener();
        JOptionPane.showMessageDialog(frame, message, title, JOptionPane.ERROR_MESSAGE);
        reregisterKeyListener();
    }

    public EditorGrammar getCurrentGrammar() {
        return currentGrammar;
    }

    public void deregisterKeyListener() {
        if (selectionDialog != null && selectionDialog.isVisible()) {
            selectionDialog.deregisterKeyManager();
        } else manager.removeKeyEventDispatcher(cursorListener);
    }

    public void reregisterKeyListener() {
        if (selectionDialog != null && selectionDialog.isVisible()) {
            selectionDialog.reregisterKeyManager();
        } else manager.addKeyEventDispatcher(cursorListener);
    }

    private void setupTitle() {
        frame.setTitle("GrammarEditor - " + currentGrammar.name() + " - " + userName);
    }

    public String getUserName() {
        return userName;
    }

    public String getTarget() {
        return targetInput;
    }
}