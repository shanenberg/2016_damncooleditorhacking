package damncooleditorhackers.view;

import damncooleditorhackers.EditorGrammar;
import damncooleditorhackers.view.view_elements.TimingPanel;

import javax.swing.*;

/**
 * Created by Niklas on 23.09.2016.
 */
public interface ExperimentView {

    void checkUserInput();

    void showOverlayPane(String mode, String text);

    void hideOverlayPane();

    void restartTimer();

    void end();

    void abort();

    TimingPanel getTimingPanel();

    JFrame getFrame();

    void setUserName(String name);

    String getCurrentInput();

    void showInvalidDialog(String message, String title);

    EditorGrammar getCurrentGrammar();

    String getUserName();

    String getTarget();
}
