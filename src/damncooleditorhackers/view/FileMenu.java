package damncooleditorhackers.view;

import damncooleditorhackers.view.listener.ViewElementListeners;

import javax.swing.*;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;

/**
 * Created by Nick on 23.06.2016.
 */
public class FileMenu extends JMenu {

    public FileMenu(String name, ViewElementListeners viewListeners) {
        super(name);
        createFileMenu(viewListeners);
    }

    private void createFileMenu(ViewElementListeners viewListeners) {
        JMenuItem menuItemLoad = new JMenuItem("Laden", KeyEvent.VK_O);
        menuItemLoad.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_O, InputEvent.CTRL_MASK));
        add(menuItemLoad);
        menuItemLoad.addActionListener(viewListeners.getLoadListener());
    }
}
