package damncooleditorhackers.view;

import damncooleditorhackers.EditorGrammar;
import damncooleditorhackers.model.grammarElements.Element;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;

/**
 * Created by Thorben on 16.06.2016.
 */
public interface ViewUpdater {

    JFrame getFrame();

    void updateSelectionDialog(ArrayList<Element> selectionElements, Point lastButtonLocation);

    int addNewLineJPanel();

    boolean showConfirmationDialog(String action);

    void clearMainPane();

    Cursor getViewCursor();

    void updateElements(ArrayList<Element> ruleElements);

    String getUserName();

    EditorGrammar getCurrentGrammar();

    void deregisterKeyListener();

    void reregisterKeyListener();
}
