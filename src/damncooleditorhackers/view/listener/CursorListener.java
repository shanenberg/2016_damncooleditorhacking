package damncooleditorhackers.view.listener;

import damncooleditorhackers.view.Cursor;
import damncooleditorhackers.view.view_elements.ElementButton;
import damncooleditorhackers.view.view_elements.TimingDialog;

import java.awt.*;
import java.awt.event.KeyEvent;
import java.util.regex.Pattern;

/**
 * Created by Nick on 01.07.2016.
 */
public class CursorListener implements KeyEventDispatcher {
    private final Cursor cursor;
    private boolean dialogVisible;
    private Mode mode;
    private boolean endRequested;
    private ViewElementListeners viewListeners;
    private static final Pattern directInputRegex = Pattern.compile("^[-a-zA-Z0-9_+*.,;]+$");

    public CursorListener(Cursor cursor, Mode mode, ViewElementListeners viewListeners) {
        this.cursor = cursor;
        this.mode = mode;
        this.viewListeners = viewListeners;
    }

    public boolean dispatchKeyEvent(KeyEvent e) {
        if (!e.isConsumed() && e.getID() == KeyEvent.KEY_PRESSED && cursor.isOnButtons() && cursor.hasSelection()) {
            if (mode == Mode.EDITORVIEW && !dialogVisible || mode == Mode.SELECTIONDIALOG && dialogVisible) {
                if (e.getKeyCode() == KeyEvent.VK_ENTER || e.getKeyCode() == KeyEvent.VK_SPACE && mode == Mode.SELECTIONDIALOG && dialogVisible) {
                    e.consume();
                    cursor.getSelection().get(0).fire();
                } else if (e.getKeyCode() == KeyEvent.VK_SPACE && !(mode == Mode.SELECTIONDIALOG && dialogVisible)) {
                    if (cursor.hasSelection()) {
                        e.consume();
                        viewListeners.addRightEvent(cursor.getSelection().get(0));
                    }
                } else if (e.getKeyCode() == KeyEvent.VK_RIGHT) {
                    if (e.isControlDown())
                        cursor.addRight(cursor.getLast());
                    else cursor.moveRight();
                    e.consume();
                } else if (e.getKeyCode() == KeyEvent.VK_LEFT) {
                    if (e.isControlDown())
                        cursor.addLeft(cursor.getFirst());
                    else cursor.moveLeft();
                    e.consume();
                } else if (e.getKeyCode() == KeyEvent.VK_UP) {
                    cursor.moveUp();
                } else if (e.getKeyCode() == KeyEvent.VK_DOWN) {
                    cursor.moveDown();
                } else if (e.getKeyCode() == KeyEvent.VK_R) {
                    if (!cursor.inSelectionDialog())
                        viewListeners.replaceEvent(cursor.getSelection());
                    //viewListeners.replaceEvent(cursor.getUpdatedSelection());
                } else if (e.getKeyCode() == KeyEvent.VK_DELETE) {
                    viewListeners.deleteEvent(cursor.getSelection());
                } else if (e.getKeyCode() == KeyEvent.VK_ESCAPE) {
                    cursor.closeDialog();
                } else if (e.getKeyCode() == KeyEvent.VK_TAB) {
                    cursor.moveToNextEditable();
                } else if (e.getKeyCode() == KeyEvent.VK_Z && e.isControlDown()) {
                    viewListeners.undoEvent();
                } else if (e.getKeyCode() == KeyEvent.VK_Y && e.isControlDown()) {
                    viewListeners.redoEvent();
                } else if (e.getKeyCode() == KeyEvent.VK_D && e.isControlDown()) {
                    viewListeners.duplicateEvent(cursor.getUpdatedSelection());
                } else if (e.getKeyCode() == KeyEvent.VK_END) {
                    cursor.moveToEnd();
                } else if (e.getKeyCode() == KeyEvent.VK_HOME) {
                    cursor.moveToFirst();
                } else if (endRequested && e.isControlDown() && e.isAltDown() && e.getKeyCode() == KeyEvent.VK_P) {
                    viewListeners.getView().showOverlayPane("Stop", "Vielen Dank für ihre Teilnahme! Das Experiment ist nun beendet.");
                    viewListeners.getView().end();
                } else if (endRequested && e.isControlDown() && e.isAltDown() && e.getKeyCode() == KeyEvent.VK_O) {
                    viewListeners.getView().restartTimer();
                    endRequested = false;
                } else if (directInputRegex.matcher(String.valueOf(e.getKeyChar())).matches() && !e.isControlDown()) {
                    if (mode == Mode.EDITORVIEW && !String.valueOf(e.getKeyChar()).equals(cursor.getSelection().get(0).getText()))
                        viewListeners.editEvent(cursor.getSelection().get(0), String.valueOf(e.getKeyChar()));
                    else {
                        ElementButton button = cursor.getSelection().get(0);
                        button.setElement(button.getElement().getParent());
                        viewListeners.selectionEvent(button, String.valueOf(e.getKeyChar()));
                    }
                } else if (e.isControlDown() && e.isAltDown() && e.getKeyCode() == KeyEvent.VK_E) {
                    viewListeners.exportGrammar();
                } else if (e.isControlDown() && e.isAltDown() && e.getKeyCode() == KeyEvent.VK_T) {
                    viewListeners.getView().getTimingPanel().pause();
                    TimingDialog timingDialog = new TimingDialog(viewListeners.getView());
                    timingDialog.show();
                    viewListeners.getView().getTimingPanel().setTimerStartTime(timingDialog.getNewStartTime());
                }
            }
        }
        return false;
    }

    public void endRequested() {
        endRequested = true;
    }

    public void dialogVisible(boolean visible) {
        dialogVisible = visible;
    }

    public enum Mode {
        EDITORVIEW,
        SELECTIONDIALOG;
    }
}