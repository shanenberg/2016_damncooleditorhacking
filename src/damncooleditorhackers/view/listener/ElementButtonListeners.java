package damncooleditorhackers.view.listener;

import damncooleditorhackers.model.grammarElements.TokenElement;
import damncooleditorhackers.model.grammarElements.Type;
import damncooleditorhackers.view.EditorView;
import damncooleditorhackers.view.view_elements.ElementButton;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

/**
 * Created by Nick on 24.06.2016
 */
public class ElementButtonListeners {
    private EditorView view;
    private ViewElementListeners viewListeners;

    public ElementButtonListeners(ViewElementListeners viewListeners) {
        this.view = viewListeners.getView();
        this.viewListeners = viewListeners;
    }

    public MouseAdapter getButtonClickedListener(final ElementButton button) {
        return new MouseAdapter() {
            @Override
            public void mouseReleased(MouseEvent e) {
                if (e.getSource().equals(button) && button.isEnabled()) {
                    if (!e.isControlDown()) {
                        if (!button.getSelected() && button.getElement().getType() != Type.ADD_RIGHT)
                            button.getButtonCursor().select(button);
                        buttonFired(button);
                    } else {
                        if (!button.getSelected()) button.getButtonCursor().addSelect(button);
                        else if (button.getButtonCursor().getSelection().size() > 1)
                            button.getButtonCursor().deselect(button);
                    }
                }
            }
        };
    }

    public void buttonFired(final ElementButton button) {
        if (button.getLineIndex() != -1) {
            if (button.getElement() instanceof TokenElement && ((TokenElement) button.getElement()).isJavaScanner()) {
                button.getParentPanel().showInputField();
            } else viewListeners.editEvent(button);
        } else viewListeners.selectionEvent(button);
    }

    public EditorView getView() {
        return view;
    }
}
