package damncooleditorhackers.view.listener;

import damncooleditorhackers.view.view_elements.TimingPanel;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created by Niklas on 23.09.2016.
 */
public class ExperimentListeners {

    public ActionListener getStartCaptureListener(final TimingPanel timingPanel) {
        return new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                if (!timingPanel.isRunning()) timingPanel.start();
                else timingPanel.pause();
            }
        };
    }

    public ActionListener getStopCaptureListener(final TimingPanel timingPanel) {
        return new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                timingPanel.stop();
            }
        };
    }
}
