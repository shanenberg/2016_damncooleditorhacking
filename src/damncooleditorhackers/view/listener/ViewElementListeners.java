package damncooleditorhackers.view.listener;

import damncooleditorhackers.EditorGrammar;
import damncooleditorhackers.controller.EditorController;
import damncooleditorhackers.controller.ViewController;
import damncooleditorhackers.view.Cursor;
import damncooleditorhackers.view.EditorView;
import damncooleditorhackers.view.view_elements.ElementButton;

import javax.swing.*;
import java.awt.event.*;
import java.util.ArrayList;

/**
 * Created by Thorben on 16.06.2016.
 */
public class ViewElementListeners {

    private ViewController controller;
    private EditorView view;

    public ViewElementListeners(EditorView view) {
        this.view = view;
        controller = new EditorController(this.view);
    }

    public void createTree(EditorGrammar startGrammar) {
        controller.loadGrammar(startGrammar);
    }


    public ActionListener getLoadListener() {
        return new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                view.deregisterKeyListener();
                controller.clickedMenuLoad();
                view.reregisterKeyListener();
            }
        };
    }


    public ActionListener getUndoListener() {
        return new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                if (view.getViewCursor().hasSelection()) undoEvent();
            }
        };
    }

    public ActionListener getRedoListener() {
        return new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                if (view.getViewCursor().hasSelection()) redoEvent();
            }
        };
    }

    public ActionListener getDuplicateListener() {
        return new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                Cursor cursor = view.getViewCursor();
                if (cursor.hasSelection()) duplicateEvent(cursor.getSelection());
            }
        };
    }

    public ActionListener getReplaceListener() {
        return new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                Cursor cursor = view.getViewCursor();
                if (cursor.hasSelection()) replaceEvent(cursor.getSelection());
            }
        };
    }

    public ActionListener getDeleteListener() {
        return new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                Cursor cursor = view.getViewCursor();
                if (cursor.hasSelection()) deleteEvent(cursor.getSelection());
            }
        };
    }

    public MouseListener getDeleteListener(final ElementButton elementButton) {
        return new MouseAdapter() {
            @Override
            public void mouseReleased(MouseEvent e) {
                Cursor cursor = view.getViewCursor();
                cursor.select(elementButton);
                if (cursor.hasSelection()) deleteEvent(cursor.getSelection());
            }
        };
    }

    public ActionListener getResetGrammarListener() {
        return new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                controller.clickedResetGrammar();
            }
        };
    }

    public WindowListener getCloseListener(final JFrame frame) {
        return new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent windowEvent) {
                view.deregisterKeyListener();
                if (JOptionPane.showConfirmDialog(frame,
                        "Bist du sicher, dass du den Editor schließen möchten?", "Editor schließen?",
                        JOptionPane.YES_NO_OPTION,
                        JOptionPane.QUESTION_MESSAGE) == JOptionPane.YES_OPTION) {
                    view.abort();
                    System.exit(0);
                }
                view.reregisterKeyListener();
            }
        };
    }

    public EditorView getView() {
        return view;
    }

    public void undoEvent() {
        controller.clickedUndo();
    }

    public void redoEvent() {
        controller.clickedRedo();
    }

    public void selectionEvent(ElementButton elementButton) {
        controller.clickedSelectionElement(elementButton);
    }

    public void selectionEvent(ElementButton elementButton, String assumption) {
        controller.assumedSelectionElement(elementButton, assumption);
    }

    public void editEvent(ElementButton elementButton) {
        controller.clickedElement(elementButton);
    }

    public void editEvent(ElementButton elementButton, String assumption) {
        controller.assumedElement(elementButton, assumption);
    }

    public void tokenEvent(String before, ElementButton elementButton) {
        controller.editedIdentifier(before, elementButton);
    }

    public void duplicateEvent(ArrayList<ElementButton> buttons) {
        controller.clickedDuplicate(buttons);
    }

    public void replaceEvent(ArrayList<ElementButton> buttons) {
        controller.clickedReplace(buttons);
    }

    public void deleteEvent(ArrayList<ElementButton> buttons) {
        view.getViewCursor().decreaseLastIndex();
        controller.clickedDeleteElement(buttons);
    }

    public void addRightEvent(ElementButton leftButton) {
        if (leftButton.getParentPanel().getButtons().size() >= 2) {
            ElementButton addRightButton = leftButton.getParentPanel().getButtons().get(1);
            controller.clickedElement(addRightButton);
        }
    }

    public void exportGrammar() {
        controller.clickedExport();
    }

    public String getElementsAsString() {
        return controller.getElementsAsString();
    }
}
