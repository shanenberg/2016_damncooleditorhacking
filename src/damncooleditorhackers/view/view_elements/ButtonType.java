package damncooleditorhackers.view.view_elements;

/**
 * Created by Mark-Desktop on 28.06.2016.
 */
public enum ButtonType {
    DeleteButton,
    AddRightButton,
    Default;
}
