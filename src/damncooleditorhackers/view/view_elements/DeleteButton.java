package damncooleditorhackers.view.view_elements;

import damncooleditorhackers.view.listener.ViewElementListeners;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

/**
 * Created by Nick on 07.07.2016.
 */
public class DeleteButton extends StyledButton {

    public DeleteButton(ViewElementListeners viewListeners, final ElementJPanel elementJPanel) {
        super("");
        setup();
        addMouseListener(viewListeners.getDeleteListener(elementJPanel.getButtons().get(0)));
        addMouseListener(new MouseAdapter() {
            public void mouseEntered(MouseEvent evt) {
                elementJPanel.dispatchEvent(evt);
            }

            public void mouseExited(MouseEvent evt) {
                elementJPanel.dispatchEvent(evt);
            }
        });
    }

    private void setup() {
        type = ButtonType.DeleteButton;
        setIcon(new ImageIcon(getClass().getResource("res/delete-button.png")));
        setText("");
        setBackground(null);
        setBorder(new EmptyBorder(0, 0, 0, 0));
        setFocusPainted(false);
    }
}
