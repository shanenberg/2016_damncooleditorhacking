package damncooleditorhackers.view.view_elements;

import javax.swing.*;
import javax.swing.border.Border;
import java.awt.*;

/**
 * Created by Nick on 07.07.2016.
 */
public class DropShadowPanel extends JPanel {

    private static final long serialVersionUID = 1L;

    private int pixels;

    public DropShadowPanel(int pix, Component contentComponent) {
        this.pixels = pix;
        Border border = BorderFactory.createEmptyBorder(pixels, pixels, pixels, pixels);
        this.setBorder(BorderFactory.createCompoundBorder(this.getBorder(), border));
        this.setLayout(new BorderLayout());
        add(contentComponent);
    }

    @Override
    protected void paintComponent(Graphics g) {
        int shade = 0;
        int topOpacity = 90;
        for (int i = 0; i < pixels; i++) {
            g.setColor(new Color(shade, shade, shade, ((topOpacity / pixels) * i)));
            g.drawRect(i, i, this.getWidth() - ((i * 2) + 1), this.getHeight() - ((i * 2) + 1));
        }
    }
}