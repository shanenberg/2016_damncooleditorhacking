package damncooleditorhackers.view.view_elements;

import damncooleditorhackers.controller.Logger;
import damncooleditorhackers.controller.StopWatch;
import damncooleditorhackers.view.ExperimentView;
import damncooleditorhackers.view.listener.ExperimentListeners;

import java.awt.*;

/**
 * Created by Nick on 29.09.2016.
 */
public class EditorTimingPanel extends TimingPanel {

    public EditorTimingPanel(FlowLayout flowLayout, ExperimentListeners listeners, ExperimentView view) {
        super(flowLayout, listeners, view);
        stopWatch = new StopWatch(this);
    }

    @Override
    public void checkInput(String input) {
        if (input.equals("")) {
            Logger.log("EINGABE WAR UNGÜLTIG", timerLabel.getText(), view.getUserName(), view.getCurrentGrammar());
            view.showInvalidDialog("Bitte arbeite zuerst alle noch offenen Felder ab.", "Eingabe war ungültig");
        } else if (!input.equals(view.getTarget())) {
            Logger.log("EINGABE IST INKORREKT", timerLabel.getText(), view.getUserName(), view.getCurrentGrammar());
            view.showInvalidDialog("Das von dir angegebene Dokument entspricht zwar der Grammatik, ist aber nicht das Wort, welches unseren Vorgaben entspricht.\nBitte schaue dir die Aufgabenstellung nochmal an.", "Wort entspricht nicht den Vorgaben");
        } else {
            Logger.log("EINGABE WAR GÜLTIG", timerLabel.getText(), view.getUserName(), view.getCurrentGrammar());
            view.showOverlayPane("Stop", "Deine Eingabe ist korrekt! Vielen Dank für deine Teilnahme! Das Experiment ist nun beendet.");
            view.end();
        }
    }
}
