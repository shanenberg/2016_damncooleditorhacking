package damncooleditorhackers.view.view_elements;

import damncooleditorhackers.view.listener.ViewElementListeners;

import javax.swing.*;
import java.awt.*;

/**
 * Created by Nick on 03.07.2016.
 */
public class EditorToolBar extends JToolBar {
    private final ViewElementListeners viewListeners;

    public EditorToolBar(ViewElementListeners viewListeners) {
        super("Toolbar");
        this.viewListeners = viewListeners;
        setup();
    }

    private void setup() {
        setFloatable(false);
        setBackground(new Color(0, 0, 0, 0));

        JButton undo = new JButton("\u21b0 Rückgängig");
        undo.setFocusable(false);
        undo.setToolTipText("Rückgängig | STRG+Z");
        undo.addActionListener(viewListeners.getUndoListener());
        add(undo);

        JButton redo = new JButton("\u21b1 Wiederholen");
        redo.setFocusable(false);
        redo.setToolTipText("Wiederholen | STRG+Y");
        redo.addActionListener(viewListeners.getRedoListener());
        add(redo);

        JButton duplicate = new JButton("\u27b3 Duplizieren");
        duplicate.setFocusable(false);
        duplicate.setToolTipText("Ausgewählte Elemente duplizieren | STRG+D");
        duplicate.addActionListener(viewListeners.getDuplicateListener());
        add(duplicate);

        JButton replace = new JButton("\u270e Ersetzen");
        replace.setFocusable(false);
        replace.setToolTipText("Ausgewählte Elemente ersetzen | R");
        replace.addActionListener(viewListeners.getReplaceListener());
        add(replace);

        JButton delete = new JButton("\u2717 Löschen");
        delete.setFocusable(false);
        delete.setToolTipText("Ausgewählte Elemente löschen | ENTF");
        delete.addActionListener(viewListeners.getDeleteListener());
        add(delete);

        JButton reset = new JButton("\u21ba Zurücksetzen");
        reset.setFocusable(false);
        reset.setToolTipText("Editor zurücksetzen");
        reset.addActionListener(viewListeners.getResetGrammarListener());
        add(reset);
    }
}
