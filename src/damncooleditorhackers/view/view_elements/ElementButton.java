package damncooleditorhackers.view.view_elements;

import damncooleditorhackers.model.grammarElements.Element;
import damncooleditorhackers.model.grammarElements.OptionalElement;
import damncooleditorhackers.model.grammarElements.TokenElement;
import damncooleditorhackers.model.grammarElements.Type;
import damncooleditorhackers.view.listener.ElementButtonListeners;

import javax.swing.*;
import javax.swing.border.CompoundBorder;
import javax.swing.border.EmptyBorder;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

/**
 * Created by Liron on 19.06.2016.
 */
public class ElementButton extends StyledButton {
    private final ElementButtonListeners buttonListeners;
    private final ElementJPanel parent;
    private Element element;
    private final int lineIndex;
    private final Color IDENTIFIERCOLOR = new Color(245, 214, 93, 255);
    private final Color IDENTIFIERCOLOR_SET = new Color(246, 246, 246, 255);
    private final Color OPTIONALCOLOR = new Color(165, 209, 255, 255);
    private final Color RULECOLOR = new Color(114, 238, 146, 255);
    private final Color TOKENCOLOR = new Color(255, 255, 255);
    private final Color NFOLDCOLOR = new Color(255, 183, 185);
    private final Color NFOLDTOKENCOLOR = new Color(255, 228, 232);
    private final Color REPLACEMENTCOLOR = new Color(255, 128, 132);
    private final Font buttonFont = new Font(getFont().getName(), Font.TRUETYPE_FONT, 15);
    private damncooleditorhackers.view.Cursor cursor;
    private boolean hasOptionals;

    public ElementButton(ElementJPanel parent, ElementButtonListeners buttonListeners, int lineIndex, Element element, damncooleditorhackers.view.Cursor cursor) {
        super(element.toString().replace("\n", "\u00b6"));
        this.parent = parent;
        this.element = element;
        this.lineIndex = lineIndex;
        this.buttonListeners = buttonListeners;
        this.cursor = cursor;
        setType();
        initDesign();
        addMouseListener(buttonListeners.getButtonClickedListener(this));
        if (!getText().equals("\u00b6")) addMouseListener(new MouseAdapter() {
            public void mouseEntered(MouseEvent evt) {
                getParentPanel().dispatchEvent(evt);
            }

            public void mouseExited(MouseEvent evt) {
                getParentPanel().dispatchEvent(evt);
            }
        });
    }

    private void setType() {
        switch (element.getType()) {
            case DELETION:
                type = ButtonType.DeleteButton;
                break;
            case ADD_RIGHT:
                type = ButtonType.AddRightButton;
                break;
            default:
                type = ButtonType.Default;
                break;
        }
    }

    private void initDesign() {
        if (type != ButtonType.Default) initNonDefault();
        else initDefault();
        setToolTipText(findParentRule(element));
    }

    private String findParentRule(Element element) {
        Element parent = element.getParent();
        while (parent.hasParent() && parent.getType() != Type.RULE) parent = parent.getParent();
        return parent.toString();
    }

    private void initDefault() {
        setBorder(new CompoundBorder(new EmptyBorder(5, 8, 5, 8), new EmptyBorder(0, 8, 0, 8)));
        setFocusPainted(false);
        setFont(buttonFont);
        setBackground(RULECOLOR); //Default Color
        if (element.getType() == Type.TOKEN) {
            if (((TokenElement) element).isJavaScanner()) {
                setBackground(IDENTIFIERCOLOR);
            }
            else {
                if (!getText().equals("\u00b6")) setFont(getFont().deriveFont(Font.BOLD));
                else setForeground(new Color(209, 209, 209, 255));
                if (lineIndex != -1) {
                    setBackground(TOKENCOLOR);
                    setBorder(new EmptyBorder(5, 6, 5, 6));
                }
            }
            if (((TokenElement) element).isSet()) {
                setBackground(IDENTIFIERCOLOR_SET);
                setFont(getFont().deriveFont(Font.BOLD));
            }
            if (hasOptionals) setBackground(NFOLDTOKENCOLOR);
        } else if (element.getType() == Type.REPLACEMENT)
            setBackground(REPLACEMENTCOLOR);
        else if (element.getType() == Type.OPTIONAL)
            if (!((OptionalElement) element).isVisible()) {
                setBackground(Color.WHITE);
                setForeground(Color.lightGray);
            } else setBackground(OPTIONALCOLOR);
        else if (element.getType() == Type.OPCONTAINER)
            setBackground(OPTIONALCOLOR);
        else if (hasOptionals) setBackground(NFOLDCOLOR);
    }

    private void initNonDefault() {
        String path = "";
        if (type == ButtonType.DeleteButton) path = "res/delete-button.png";
        else if (type == ButtonType.AddRightButton) path = "res/start-button.png";
        setIcon(new ImageIcon(getClass().getResource(path)));
        setText("");
        setBackground(null);
        setBorder(new EmptyBorder(0, 0, 0, 0));
    }

    public Element getElement() {
        return element;
    }

    public void setElement(Element element) {
        this.element = element;
    }

    public int getLineIndex() {
        return lineIndex;
    }

    public void select(damncooleditorhackers.view.Cursor.SelectionMode mode) {
        if (!getSelected()) {
            super.select(mode);
            dispatchEvent(new MouseEvent(this, MouseEvent.MOUSE_ENTERED, System.currentTimeMillis(), 0, 0, 0, 0, false, 0));
        }
    }

    public void deselect() {
        if (getSelected()) {
            super.deselect();
            dispatchEvent(new MouseEvent(this, MouseEvent.MOUSE_EXITED, System.currentTimeMillis(), 0, 0, 0, 0, false, 0));
            repaint();
        }
    }

    public damncooleditorhackers.view.Cursor getButtonCursor() {
        return cursor;
    }

    public boolean getSelected() {
        return super.getSelected();
    }

    public void fire() {
        buttonListeners.buttonFired(this);
    }

    public ElementJPanel getParentPanel() {
        return parent;
    }

    @Override
    public void setVisible(boolean to) {
        super.setVisible(to);
        if (to) setText(element.getText());
    }

    public void setHasOptionals(boolean hasOptionals) {
        if (hasOptionals && element.getType() != Type.TOKEN) setBackground(NFOLDCOLOR);
        else setBackground(NFOLDTOKENCOLOR);
        this.hasOptionals = hasOptionals;
    }

    public boolean hasOptionals() {
        return hasOptionals;
    }
}
