package damncooleditorhackers.view.view_elements;

import damncooleditorhackers.model.grammarElements.*;
import damncooleditorhackers.view.Cursor;
import damncooleditorhackers.view.listener.ElementButtonListeners;
import damncooleditorhackers.view.listener.ViewElementListeners;

import javax.swing.*;
import javax.swing.border.CompoundBorder;
import javax.swing.border.EmptyBorder;
import javax.swing.border.TitledBorder;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;

/**
 * Created by Nick on 30.06.2016.
 */
public class ElementJPanel extends JPanel {
    private final Element element;
    private final ViewElementListeners viewListeners;
    private ArrayList<ElementButton> buttons;
    private ElementTextField inputField;
    private int lineIndex;
    private String parentName;
    private JSplitPane controlSplit;
    private boolean isLineBreak;

    public ElementJPanel(int lineIndex, Element element, ViewElementListeners viewListeners, Cursor cursor) {
        buttons = new ArrayList<ElementButton>();
        this.element = element;
        this.lineIndex = lineIndex;
        this.viewListeners = viewListeners;
        parentName = "";
        setLayout(new FlowLayout(FlowLayout.LEFT));
        ((FlowLayout) getLayout()).setHgap(2);
        ((FlowLayout) getLayout()).setVgap(0);
        createButtons(cursor);
        if (controlSplit != null && !isLineBreak) addMouseListener(new MouseAdapter() {
            public void mouseEntered(MouseEvent evt) {
                controlSplit.setVisible(true);
                revalidate();
                repaint();
            }

            public void mouseExited(MouseEvent evt) {
                if (buttons.size() > 0 && !buttons.get(0).getSelected()) controlSplit.setVisible(false);
                revalidate();
                repaint();
            }
        });
    }

    private void createButtons(Cursor cursor) {
        setBackground(null);
        buttons.add(new ElementButton(this, new ElementButtonListeners(viewListeners), lineIndex, element, cursor));
        for (ElementButton eB : buttons) {
            add(eB);
            if (eB.getElement() instanceof TokenElement && ((TokenElement) eB.getElement()).isJavaScanner()) {
                inputField = new ElementTextField((TokenElement) element, this);
                inputField.setVisible(false);
                add(inputField);
            }
        }
        if (buttons.size() > 0) {
            if (lineIndex != -1) addMoreButton(cursor);
            parentName = buttons.get(0).getToolTipText();
            setToolTipText(parentName);
            if (lineIndex != -1 && buttons.get(0).getElement().getChildren().size() > 0 || lineIndex != -1 && buttons.get(0).getElement().getType() == Type.OR) {
                TitledBorder titledBorder = new TitledBorder(BorderFactory.createLineBorder(buttons.get(0).getBackground().darker(), 1), parentName);
                titledBorder.setTitleFont(new Font(getFont().getName(), Font.BOLD, 10));
                setBorder(new CompoundBorder(titledBorder, new EmptyBorder(1, 1, 1, 1)));
            }
        } else if (lineIndex != -1)
            setBorder(new TitledBorder(BorderFactory.createLineBorder(new Color(0, 0, 0, 0), 1), " "));
    }

    private void addMoreButton(Cursor cursor) {
        ElementButton moreButton = new ElementButton(this, new ElementButtonListeners(viewListeners), lineIndex, new AddRightElement(Op.NONE, element), cursor);
        if (((AddRightElement) moreButton.getElement()).hasOptionals(element)) {
            buttons.add(moreButton);
            if (buttons.get(0).getText().equals("\u00b6")) isLineBreak = true;
            controlSplit = new JSplitPane(JSplitPane.VERTICAL_SPLIT);
            controlSplit.setBackground(null);
            controlSplit.setBorder(null);
            controlSplit.setTopComponent(moreButton);
            controlSplit.setBottomComponent(new DeleteButton(viewListeners, this));
            controlSplit.setDividerSize(0);
            add(controlSplit);
            buttons.get(0).setHasOptionals(true);
        }
    }

    public String getParentName() {
        return parentName;
    }

    public void showInputField() {
        if (inputField != null) {
            buttons.get(0).setVisible(false);
            inputField.setVisible(true);
            inputField.selectAll();
            inputField.requestFocus();
        }
    }

    public void hideInputField(String before) {
        if (inputField != null) {
            inputField.setVisible(false);
            buttons.get(0).setVisible(true);
            if (before != null) viewListeners.tokenEvent(before, buttons.get(0));
        }
    }

    public void hideControls() {
        if (controlSplit != null && !isLineBreak) {
            controlSplit.setVisible(false);
            revalidate();
            repaint();
        }
    }

    public ArrayList<ElementButton> getButtons() {
        return buttons;
    }
}
