package damncooleditorhackers.view.view_elements;

import damncooleditorhackers.model.grammarElements.TokenElement;
import slowscane.scanners.TokenReader;
import slowscane.streams.StringBufferStream;

import javax.swing.*;
import javax.swing.border.CompoundBorder;
import javax.swing.border.EmptyBorder;
import javax.swing.border.LineBorder;
import java.awt.*;
import java.awt.event.*;

/**
 * Created by Nick on 13.07.2016.
 */
public class ElementTextField extends JTextField {

    private final TokenElement element;
    private final ElementJPanel parent;
    private final String before;

    public ElementTextField(final TokenElement element, final ElementJPanel parent) {
        super(element.getText(), element.getText().length());
        this.before = element.getText();
        this.element = element;
        this.parent = parent;
        setBackground(parent.getBackground());
        setForeground(getForeground().brighter());
        setFocusTraversalKeysEnabled(false);
        setFont(new Font("Consolas", Font.PLAIN, 15));
        setBorder(new CompoundBorder(new LineBorder(parent.getButtons().get(0).getBackground().darker(), 3), new CompoundBorder(new EmptyBorder(3, 3, 3, 3), new EmptyBorder(0, 2, 0, 2))));
        addFocusListener(new FocusListener() {
            public void focusGained(FocusEvent e) {
            }

            public void focusLost(FocusEvent e) {
                if (!checkInput()) parent.hideInputField(null);
            }
        });

        addKeyListener(new KeyAdapter() {
            @Override
            public void keyPressed(KeyEvent e) {
                if (e.getKeyCode() == KeyEvent.VK_TAB || e.getKeyCode() == KeyEvent.VK_ENTER || e.getKeyCode() == KeyEvent.VK_UP || e.getKeyCode() == KeyEvent.VK_DOWN || getCaretPosition() == getText().length() && e.getKeyCode() == KeyEvent.VK_RIGHT || getCaretPosition() == 0 && e.getKeyCode() == KeyEvent.VK_LEFT) {
                    checkInput();
                } else if (e.isControlDown() && e.getKeyCode() == KeyEvent.VK_Z || e.isControlDown() && e.getKeyCode() == KeyEvent.VK_Y || e.isControlDown() && e.getKeyCode() == KeyEvent.VK_D || e.getKeyCode() == KeyEvent.VK_ESCAPE) {
                    parent.hideInputField(null);
                }
            }
        });
    }

    private boolean checkInput() {
        String enteredIdentifier = getText();
        if (!enteredIdentifier.equals("")) {
            if (!enteredIdentifier.contains("\"")) enteredIdentifier = "\"" + enteredIdentifier + "\"";
            if (!enteredIdentifier.startsWith("\"")) enteredIdentifier = "\"" + enteredIdentifier;
            if (!enteredIdentifier.endsWith("\"")) enteredIdentifier += "\"";
            TokenReader jIScanner = element.getJIScanner();
            try {
                jIScanner.scanNext(new StringBufferStream(enteredIdentifier));
            } catch (Exception sE) {
                Toolkit.getDefaultToolkit().beep();
                showToolTip("Keine gültige Eingabe");
                return false;
            }
            element.setText(enteredIdentifier);
            parent.hideInputField(before);
            return true;
        } else showToolTip("Keine gültige Eingabe");
        return false;
    }

    private void showToolTip(String message) {
        setToolTipText(message);
        try {
            KeyEvent tooltipHack = new KeyEvent(this, KeyEvent.KEY_PRESSED, System.currentTimeMillis(), InputEvent.CTRL_MASK, KeyEvent.VK_F1, KeyEvent.CHAR_UNDEFINED);
            dispatchEvent(tooltipHack);
        } catch (Throwable e1) {
            e1.printStackTrace();
        }
    }

    @Override
    public void setVisible(boolean aFlag) {
        super.setVisible(aFlag);
        if (aFlag) {
            setColumns((int) (parent.getWidth() / 11.15));
        }
    }
}
