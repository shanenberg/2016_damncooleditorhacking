package damncooleditorhackers.view.view_elements;

import damncooleditorhackers.model.grammarElements.Type;

import javax.swing.*;
import javax.swing.border.LineBorder;
import java.awt.*;
import java.util.ArrayList;

/**
 * Created by Nick on 30.06.2016.
 */
public class LineJPanel extends JPanel {
    private ArrayList<ElementButton> buttons;

    public LineJPanel() {
        super();
        buttons = new ArrayList<ElementButton>();
        setBorder(new LineBorder(Color.blue, 0));
    }

    @Override
    public Component add(Component comp) {
        if (comp instanceof ElementJPanel) {
            ElementJPanel elementJPanel = (ElementJPanel) comp;
            buttons.addAll(elementJPanel.getButtons());
            elementJPanel.hideControls();
            setMaximumSize(new Dimension((int) getMaximumSize().getWidth(), 58));
        }
        return super.add(comp);
    }

    public ElementButton get(int index) {
        return buttons.get(index);
    }

    public ArrayList<ElementButton> getButtons() {
        ArrayList<ElementButton> temp = new ArrayList<ElementButton>();
        for (ElementButton button : buttons)
            if (!(button.getElement().getType() == Type.ADD_RIGHT && !button.getElement().getText().equals("\n")))
                temp.add(button);
        return temp;
    }

}
