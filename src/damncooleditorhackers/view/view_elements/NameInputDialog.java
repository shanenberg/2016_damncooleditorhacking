package damncooleditorhackers.view.view_elements;

import damncooleditorhackers.controller.Logger;
import damncooleditorhackers.view.ExperimentView;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.regex.Pattern;

/**
 * Created by Nick on 27.09.2016.
 */
public class NameInputDialog {

    private final ExperimentView view;
    private JDialog dialog;
    private JTextField input;
    private Pattern regexName = Pattern.compile("^[a-zA-Z]+(\\s([a-zA-Z])+)*$");

    public NameInputDialog(final ExperimentView view) {
        this.view = view;
        input = new JTextField("");
        input.setColumns(20);
        JButton confirm = new JButton("Ok");
        confirm.setFocusable(false);
        Object[] options1 = {input, confirm};
        JOptionPane jop = new JOptionPane("Bitte gib deinen Namen ein:", JOptionPane.QUESTION_MESSAGE, JOptionPane.OK_OPTION, null, options1, options1[0]);
        dialog = jop.createDialog(view.getFrame(), "Willkommen zum Experiment!");
        dialog.setPreferredSize(new Dimension(350, 100));
        dialog.setMinimumSize(new Dimension(350, 100));
        confirm.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                if (check()) close();
            }
        });
        input.addKeyListener(new KeyListener() {
            public void keyTyped(KeyEvent e) {
            }

            public void keyPressed(KeyEvent e) {
                if (e.getKeyCode() == KeyEvent.VK_ESCAPE) e.consume();
            }

            public void keyReleased(KeyEvent e) {
                if (e.getKeyCode() == KeyEvent.VK_ENTER && check())
                    close();
            }
        });
        dialog.setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);
        dialog.setFocusable(true);
        dialog.addKeyListener(new KeyListener() {
            public void keyTyped(KeyEvent e) {
            }

            public void keyPressed(KeyEvent e) {
                if (e.getKeyCode() == KeyEvent.VK_ESCAPE) e.consume();
            }

            public void keyReleased(KeyEvent e) {
            }
        });
    }

    private boolean check() {
        return input.getText() != null && regexName.matcher(input.getText()).matches();
    }

    private void close() {
        Logger.log("PROBAND ist " + input.getText(), view.getTimingPanel().getTimerString(), input.getText(), view.getCurrentGrammar());
        dialog.dispose();
    }

    public void show() {
        dialog.setVisible(true);
    }

    public String getName() {
        return input.getText();
    }
}
