package damncooleditorhackers.view.view_elements;

import damncooleditorhackers.view.ExperimentView;

import javax.swing.*;
import java.awt.*;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

/**
 * Created by Nick on 19.09.2016.
 */
public class OverlayPane extends JPanel {
    private ExperimentView view;
    private String lastMode = "";
    private Color OPAQUEBG = new Color(23, 26, 25, 255);

    public OverlayPane(final ExperimentView view) {
        this.view = view;
        setOpaque(false);
        addMouseListener(new MouseListener() {
            public void mouseClicked(MouseEvent e) {
            }

            public void mousePressed(MouseEvent e) {
            }

            public void mouseReleased(MouseEvent e) {
                if (!lastMode.equals("Stop")) view.hideOverlayPane();
            }

            public void mouseEntered(MouseEvent e) {
            }

            public void mouseExited(MouseEvent e) {
            }
        });
        addKeyListener(new KeyListener() {
            public void keyTyped(KeyEvent e) {
            }

            public void keyPressed(KeyEvent e) {
            }

            public void keyReleased(KeyEvent e) {
                if ((lastMode.equals("Stop") || lastMode.equals("Pause")) && e.isControlDown() && e.isAltDown() && e.getKeyCode() == KeyEvent.VK_I) {
                    if (!view.getTimingPanel().isEnded())
                        view.checkUserInput();
                }
            }
        });
        setFocusTraversalKeysEnabled(false);
        setAlignmentX(CENTER_ALIGNMENT);
        setAlignmentY(CENTER_ALIGNMENT);
        setLayout(new GridBagLayout());
    }

    public void showMode(String mode, String text) {
        removeAll();
        lastMode = mode;
        setBackground(OPAQUEBG);
        JLabel title = new JLabel(mode);
        title.setFont(new Font("Consolas", Font.PLAIN, 36));
        title.setForeground(Color.white);
        title.setAlignmentX(CENTER_ALIGNMENT);
        JLabel button = new JLabel(new ImageIcon(getClass().getResource("res/" + mode.toLowerCase() + ".png")), SwingConstants.LEFT);
        button.setAlignmentX(CENTER_ALIGNMENT);
        JLabel description = new JLabel(text);
        description.setFont(new Font("Consolas", Font.PLAIN, 28));
        description.setForeground(Color.white);
        description.setAlignmentX(CENTER_ALIGNMENT);

        Box box = new Box(BoxLayout.Y_AXIS);
        box.setPreferredSize(new Dimension(view.getFrame().getWidth(), view.getFrame().getHeight()));
        box.setMinimumSize(new Dimension(view.getFrame().getWidth(), view.getFrame().getHeight()));
        box.setAlignmentX(JComponent.CENTER_ALIGNMENT);
        box.add(Box.createVerticalGlue());
        box.add(title);
        box.add(button);
        box.add(description);
        box.add(Box.createVerticalGlue());
        add(box, new GridBagConstraints());
        validate();
        repaint();
        requestFocusInWindow();
    }

    @Override
    public void paintComponent(Graphics g) {
        setOpaque(true);
        super.paintComponent(g);
        setOpaque(false);
    }

}