package damncooleditorhackers.view.view_elements;

import damncooleditorhackers.model.grammarElements.Element;
import damncooleditorhackers.view.CursorTraversal;
import damncooleditorhackers.view.EditorView;
import damncooleditorhackers.view.listener.CursorListener;
import damncooleditorhackers.view.listener.ViewElementListeners;

import javax.swing.*;
import javax.swing.border.TitledBorder;
import java.awt.*;
import java.awt.event.WindowEvent;
import java.awt.event.WindowFocusListener;
import java.util.ArrayList;

/**
 * Created by Nick on 23.06.2016.
 */
public class SelectionDialog extends JDialog implements CursorTraversal {
    private final LineJPanel selectionJPanel;
    private final Point parentLocation;
    private final ViewElementListeners viewListeners;
    private final damncooleditorhackers.view.Cursor cursor;
    private final KeyboardFocusManager manager;
    private final EditorView parent;
    private CursorListener cursorListener;
    private String parentName;

    public SelectionDialog(EditorView parent, Point parentLocation, ArrayList<Element> elements, ViewElementListeners viewListeners) {
        super(parent.getFrame(), "Select");
        this.parent = parent;
        this.parentLocation = parentLocation;
        this.viewListeners = viewListeners;
        parentName = "";
        cursor = new damncooleditorhackers.view.Cursor(this, true);
        selectionJPanel = new LineJPanel();
        selectionJPanel.setLayout(new FlowLayout(FlowLayout.CENTER));
        fillJPanel(elements);
        manager = KeyboardFocusManager.getCurrentKeyboardFocusManager();
        cursorListener = new CursorListener(cursor, CursorListener.Mode.SELECTIONDIALOG, viewListeners);
        cursorListener.dialogVisible(true);
        manager.addKeyEventDispatcher(cursorListener);
    }

    private void fillJPanel(ArrayList<Element> elements) {
        Container contentPane = getContentPane();
        for (Element e : elements) {
            ElementJPanel eJPanel = new ElementJPanel(-1, e, viewListeners, cursor);
            parentName = eJPanel.getParentName();
            selectionJPanel.add(eJPanel);
        }
        cursor.updateButtons();
        cursor.reselect();
        setUndecorated(true);
        addWindowFocusListener(new WindowFocusListener() {
            public void windowGainedFocus(WindowEvent e) {
            }

            public void windowLostFocus(WindowEvent e) {
                if (SwingUtilities.isDescendingFrom(parent.getFrame(), SelectionDialog.this)) return;
                closeDialog();
            }
        });
        if (!parentName.equals("")) {
            selectionJPanel.setBorder(new TitledBorder(BorderFactory.createLineBorder(getBackground().darker(), 1), parentName));
            ((TitledBorder) selectionJPanel.getBorder()).setTitleJustification(TitledBorder.CENTER);
            ((TitledBorder) selectionJPanel.getBorder()).setTitleFont(new Font(getFont().getName(), Font.BOLD, 11));
        }
        DropShadowPanel shadowPanel = new DropShadowPanel(6, selectionJPanel);
        contentPane.add(shadowPanel);
        pack();
        if (parentLocation != null) setupLocation(elements);
    }

    private void setupLocation(ArrayList<Element> elements) {
        int x = parentLocation.x - elements.size() * 18;
        int y = parentLocation.y - 84;
        if (x < 0) x = 0;
        if (y < 0) y = 0;
        if (x + getWidth() > parent.getWindowWidth()) x = parent.getWindowWidth() - getWidth();
        if (y + 64 > parent.getWindowHeight()) y = parent.getWindowHeight() + 64;
        setLocation(x, y);
    }

    public void closeDialog() {
        parent.closeDialogs();
    }

    public Object getFocusedComponent() {
        return parent.getFocusedComponent();
    }

    public void scrollToComponent(ElementButton button) {
        button.getParentPanel().scrollRectToVisible(button.getBounds(null));
    }

    public void deregisterKeyManager() {
        manager.removeKeyEventDispatcher(cursorListener);
    }

    public ArrayList<LineJPanel> getPanels() {
        ArrayList<LineJPanel> temp = new ArrayList<LineJPanel>();
        temp.add(selectionJPanel);
        return temp;
    }

    public void reregisterKeyManager() {
        manager.addKeyEventDispatcher(cursorListener);
    }
}
