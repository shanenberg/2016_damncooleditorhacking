package damncooleditorhackers.view.view_elements;

import damncooleditorhackers.view.Cursor;

import javax.swing.*;
import java.awt.*;
import java.awt.geom.Rectangle2D;

/**
 * Created by Nick on 28.06.2016.
 */
public abstract class StyledButton extends JButton {
    private Shape shape;
    private Shape border;
    private Shape base;
    private static final int FOCUS_STROKE = 4;
    protected ButtonType type;
    protected boolean selected;
    private Icon moreIcon, moreSelectedIcon, deleteIcon, deleteSelectedIcon;

    public StyledButton(String text) {
        super(text);
        moreIcon = new ImageIcon(getClass().getResource("res/start-button.png"));
        moreSelectedIcon = new ImageIcon(getClass().getResource("res/start-button-selected.png"));
        deleteIcon = new ImageIcon(getClass().getResource("res/delete-button.png"));
        deleteSelectedIcon = new ImageIcon(getClass().getResource("res/delete-button-selected.png"));
        type = ButtonType.Default;
    }

    @Override
    public void updateUI() {
        super.updateUI();
        setContentAreaFilled(false);
        setFocusPainted(false);
        initShape();
    }

    private void initShape() {
        if (!getBounds().equals(base)) {
            base = getBounds();
            shape = new Rectangle2D.Float(0, 0, getWidth() - 1, getHeight() - 1);
            border = new Rectangle2D.Float(FOCUS_STROKE, FOCUS_STROKE, getWidth() - FOCUS_STROKE * 2, getHeight() - FOCUS_STROKE * 2);
        }
    }

    private void paintNormal(Graphics2D g2) {
        switch (type) {
            case DeleteButton:
                setIcon(deleteIcon);
                break;
            case AddRightButton:
                setIcon(moreIcon);
                break;
            default:
                g2.setColor(getBackground());
                g2.fill(shape);
                break;
        }
    }

    private void paintFocus(Graphics2D g2) {
        switch (type) {
            case DeleteButton:
                setIcon(deleteSelectedIcon);
                break;
            case AddRightButton:
                setIcon(moreSelectedIcon);
                break;
            default:
                g2.setColor(getBackground().darker());
                g2.fill(shape);
                g2.setColor(getBackground());
                g2.fill(border);
                break;
        }
    }

    private void paintRollover(Graphics2D g2) {
        switch (type) {
            case DeleteButton:
                setIcon(deleteSelectedIcon);
                break;
            case AddRightButton:
                setIcon(moreSelectedIcon);
                break;
            default:
                if (selected) {
                    g2.setColor(getBackground().darker());
                    g2.fill(shape);
                    g2.setColor(getBackground().brighter());
                    g2.fill(border);
                } else {
                    g2.setColor(getBackground().brighter());
                    g2.fill(shape);
                }
                break;
        }
    }

    @Override
    protected void paintComponent(Graphics g) {
        initShape();
        Graphics2D g2 = (Graphics2D) g.create();
        g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
                RenderingHints.VALUE_ANTIALIAS_ON);
        if (getModel().isArmed()) {
            g2.setColor(getBackground().darker());
            g2.fill(shape);
        } else if (isRolloverEnabled() && getModel().isRollover()) {
            paintRollover(g2);
        } else if (selected) {
            paintFocus(g2);
        } else if (!isEnabled()) {
            UIManager.put("Button.disabledText", new Color(0, 89, 17));
            g2.setColor(getBackground());
            g2.fill(border);
            g2.fill(shape);
        } else {
            paintNormal(g2);
        }
        g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
                RenderingHints.VALUE_ANTIALIAS_OFF);
        g2.setColor(getBackground());
        super.paintComponent(g2);
        g2.dispose();
    }

    @Override
    protected void paintBorder(Graphics g) {
        initShape();
        Graphics2D g2 = (Graphics2D) g.create();
        g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
                RenderingHints.VALUE_ANTIALIAS_ON);
        g2.setColor(getBackground());
        g2.draw(shape);
        g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
                RenderingHints.VALUE_ANTIALIAS_OFF);
        g2.dispose();
    }

    public void select(Cursor.SelectionMode mode) {
        selected = true;
    }

    public void deselect() {
        selected = false;
    }

    public boolean getSelected() {
        return selected;
    }
}
