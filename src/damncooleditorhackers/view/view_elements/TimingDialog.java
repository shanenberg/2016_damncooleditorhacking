package damncooleditorhackers.view.view_elements;

import damncooleditorhackers.controller.Logger;
import damncooleditorhackers.view.ExperimentView;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

/**
 * Created by Nick on 28.09.2016.
 */
public class TimingDialog {

    private final ExperimentView view;
    private final JTextField minutes;
    private final JDialog dialog;
    private final JTextField seconds;
    private long startTime;

    public TimingDialog(ExperimentView view) {
        this.view = view;
        minutes = new JTextField("");
        minutes.setColumns(2);
        seconds = new JTextField("");
        seconds.setColumns(2);
        JButton confirm = new JButton("Ok");
        Object[] options1 = {minutes, new JLabel(":"), seconds, confirm};
        JOptionPane jop = new JOptionPane("Bitte gib eine Startzeit ein:", JOptionPane.QUESTION_MESSAGE, JOptionPane.OK_OPTION, null, options1, options1[0]);
        dialog = jop.createDialog(view.getFrame(), "Startzeit ändern");
        dialog.setPreferredSize(new Dimension(350, 100));
        dialog.setMinimumSize(new Dimension(350, 100));
        confirm.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                if (!minutes.getText().replace(" ", "").equals("") && !seconds.getText().replace(" ", "").equals("") && areValid(minutes.getText(), seconds.getText())) {
                    close(Integer.valueOf(minutes.getText()), Integer.valueOf(seconds.getText()));
                }
            }
        });
        seconds.addKeyListener(new KeyAdapter() {
            @Override
            public void keyReleased(KeyEvent e) {
                if (e.getKeyCode() == KeyEvent.VK_ENTER && !minutes.getText().replace(" ", "").equals("") && !seconds.getText().replace(" ", "").equals("") && areValid(minutes.getText(), seconds.getText()))
                    close(Integer.valueOf(minutes.getText()), Integer.valueOf(seconds.getText()));
            }
        });
        dialog.setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);
    }

    private boolean areValid(String minutes, String seconds) {
        try {
            Integer.valueOf(minutes);
            Integer.valueOf(seconds);
        } catch (NumberFormatException e) {
            return false;
        }
        return (minutes.length() <= 2 && seconds.length() <= 2);
    }

    public void show() {
        dialog.setVisible(true);
    }

    private void close(int minutes, int seconds) {
        startTime = System.currentTimeMillis() - (minutes * 60 * 1000 + seconds * 1000);
        Logger.log("STARTZEIT GEÄNDERT", minutes + ":" + seconds, view.getUserName(), view.getCurrentGrammar());
        dialog.dispose();
    }

    public long getNewStartTime() {
        return startTime;
    }
}
