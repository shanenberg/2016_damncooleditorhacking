package damncooleditorhackers.view.view_elements;

import damncooleditorhackers.EditorGrammar;
import damncooleditorhackers.controller.Logger;
import damncooleditorhackers.controller.StopWatch;
import damncooleditorhackers.view.ExperimentView;
import damncooleditorhackers.view.listener.ExperimentListeners;

import javax.swing.*;
import java.awt.*;
import java.net.URL;

/**
 * Created by Nick on 21.09.2016.
 */
public abstract class TimingPanel extends JPanel {

    protected final JLabel timerLabel;
    private final JButton startButton, stopButton;
    protected final ExperimentView view;
    private boolean running = true;
    private ImageIcon startIcon;
    private ImageIcon stopIcon;
    private ImageIcon pauseIcon;
    protected StopWatch stopWatch;
    private boolean ended;

    public TimingPanel(FlowLayout flowLayout, ExperimentListeners listeners, ExperimentView view) {
        super(flowLayout);
        this.view = view;
        URL startPath = TimingPanel.class.getClassLoader().getResource("damncooleditorhackers/view/view_elements/res/start-button.png");
        URL stopPath = TimingPanel.class.getClassLoader().getResource("damncooleditorhackers/view/view_elements/res/stop-button.png");
        URL pausePath = TimingPanel.class.getClassLoader().getResource("damncooleditorhackers/view/view_elements/res/pause-button.png");
        if (startPath != null && stopPath != null && pausePath != null) {
            startIcon = new ImageIcon(startPath);
            stopIcon = new ImageIcon(stopPath);
            pauseIcon = new ImageIcon(pausePath);
        }

        timerLabel = new JLabel();
        startButton = new JButton("Start", startIcon);
        startButton.addActionListener(listeners.getStartCaptureListener(this));
        startButton.setBackground(Color.white);
        stopButton = new JButton("Zur Überprüfung abgeben", stopIcon);
        stopButton.addActionListener(listeners.getStopCaptureListener(this));
        stopButton.setBackground(Color.white);
        startButton.setFocusable(false);
        stopButton.setFocusable(false);
        add(timerLabel);
        add(startButton);
        add(stopButton);
        setFocusable(false);
    }

    public void start() {
        if (ended) return;
        running = true;
        if (stopWatch.isPaused()) {
            stopWatch.restart();
            Logger.log("PAUSE verlassen", timerLabel.getText(), view.getUserName(), view.getCurrentGrammar());
        } else {
            stopWatch.start(timerLabel);
            Logger.log("START gedrückt", "00:00", view.getUserName(), view.getCurrentGrammar());
        }
        startButton.setText("Pause");
        startButton.setIcon(pauseIcon);
        stopButton.setEnabled(true);
        startButton.setEnabled(true);
    }

    public void pause() {
        Logger.log("PAUSE gedrückt", timerLabel.getText(), view.getUserName(), view.getCurrentGrammar());
        view.showOverlayPane("Pause", getDescriptionFor("Pause"));
        running = false;
        stopButton.setEnabled(true);
        startButton.setEnabled(true);
        stopWatch.pause();
        startButton.setText("Fortfahren");
        startButton.setIcon(startIcon);
    }

    public void stop() {
        String input = view.getCurrentInput();
        Logger.log("ÜBERPRÜFE:\n\"" + input + "\"", timerLabel.getText(), view.getUserName(), view.getCurrentGrammar());
        checkInput(input);
    }

    public abstract void checkInput(String input);

    public boolean isRunning() {
        return running;
    }

    public String getDescriptionFor(String mode) {
        if (mode.equals("Pause"))
            return "Zeitmessung pausiert. Fordere einen Betreuer an oder klicke um fortzufahren.";
        if (mode.equals("Start")) return "Klicke, sobald du die Freigabe erhalten hast!";
        if (mode.equals("TimerEnded")) return "Die Zeit ist abgelaufen! Vielen Dank für deine Teilnahme.";
        return "Ende angefordert. Benachrichtige einen Betreuer und warte auf weitere Anweisungen.";
    }

    public String getTimerString() {
        return !timerLabel.getText().equals("") ? timerLabel.getText() : "00:00";
    }

    public void end() {
        ended = true;
    }

    public boolean isEnded() {
        return ended;
    }

    public void setTimerStartTime(long time) {
        stopWatch.setStartTime(time);
    }

    public ExperimentView getView() {
        return view;
    }

    public EditorGrammar getCurrentGrammar() {
        return view.getCurrentGrammar();
    }
}
