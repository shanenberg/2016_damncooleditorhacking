package prettyuncooleditor;

import damncooleditorhackers.EditorGrammar;
import prettyuncooleditor.view.TextEditor;

/**
 * Created by Niklas on 23.09.2016.
 */
public class Main {

    public static void main(String[] args) {
        new TextEditor().start(EditorGrammar.Website);
    }
}
