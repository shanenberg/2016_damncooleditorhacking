package prettyuncooleditor.controller;

import damncooleditorhackers.controller.StopWatch;
import prettyuncooleditor.view.TextEditorTimingPanel;

import javax.swing.*;
import java.awt.*;
import java.text.SimpleDateFormat;
import java.util.TimerTask;

/**
 * Created by Mark-Desktop on 29.09.2016.
 */
public class ExtendedStopWatch extends StopWatch {
    private TextAreaBackupHelper logger;
    private String username;
    public ExtendedStopWatch(TextEditorTimingPanel timingPanel) {
        super(timingPanel);
        logger = new TextAreaBackupHelper();
    }

    public void setUsername(String name) {
        this.username = name;
    }

    @Override
    public void start(final JLabel target) {
        started = true;
        startTime = System.currentTimeMillis();
        java.util.Timer timer = new java.util.Timer();
        timer.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                if (running) {
                    long elapsed = (System.currentTimeMillis() - startTime);
                    target.setText(new SimpleDateFormat("mm:ss").format(elapsed));
                    timingPanel.getParent().repaint();
                    if (!target.getText().equals("00:00") && new Integer(target.getText().split(":")[1]) % 5 == 0) {
                        logger.backup(timingPanel.getView().getCurrentInput(), target.getText(), username, timingPanel.getCurrentGrammar());
                    }
                    if (elapsed >= TOTAL_DURATION) target.setForeground(Color.red);
                }
            }
        }, 0, 1000);
        this.running = true;
    }
}
