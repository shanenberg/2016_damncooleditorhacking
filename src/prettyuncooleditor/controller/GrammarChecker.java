package prettyuncooleditor.controller;

import damncooleditorhackers.EditorGrammar;
import damncooleditorhackers.model.slopeGrammars.GrammarKassenzettel;
import damncooleditorhackers.model.slopeGrammars.GrammarTestDACH;
import damncooleditorhackers.model.slopeGrammars.GrammarTestText;
import damncooleditorhackers.model.slopeGrammars.GrammarWebsite;
import slope.Parsing;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;

/**
 * Created by Niklas on 24.09.2016.
 */
public class GrammarChecker {
    private static long startTime;
    private final String userName;
    private EditorGrammar currentgrammar;

    public GrammarChecker(String userName) {
        GrammarChecker.startTime = System.currentTimeMillis();
        this.userName = userName;
    }

    public boolean check(EditorGrammar currentGrammar, String word) {
        this.currentgrammar = currentGrammar;
        switch (currentGrammar) {
            case Kassenzettel:
                GrammarKassenzettel tokens_kassenzettel = new GrammarKassenzettel();
                GrammarKassenzettel.Grammar01Parser grammarParser_kassenzettel = tokens_kassenzettel.new Grammar01Parser();
                try {
                    Parsing<GrammarKassenzettel> p = new Parsing<GrammarKassenzettel>(tokens_kassenzettel, word);
                    p.parse(grammarParser_kassenzettel.Grammar());
                } catch (Exception e) {
                    log(e);
                    return false;
                }
                break;
            case Website:
                GrammarWebsite tokens_website = new GrammarWebsite();
                GrammarWebsite.Grammar01Parser grammarParser_website = tokens_website.new Grammar01Parser();
                try {
                    Parsing<GrammarWebsite> p = new Parsing<GrammarWebsite>(tokens_website, word);
                    p.parse(grammarParser_website.Grammar());
                } catch (Exception e) {
                    log(e);
                    return false;
                }
                break;
            case Testgrammatik_DACH:
                GrammarTestDACH tokens_DACH = new GrammarTestDACH();
                GrammarTestDACH.Grammar01Parser grammarParser_DACH = tokens_DACH.new Grammar01Parser();
                try {
                    Parsing<GrammarTestDACH> p = new Parsing<GrammarTestDACH>(tokens_DACH, word);
                    p.parse(grammarParser_DACH.Grammar());
                } catch (Exception e) {
                    log(e);
                    return false;
                }
                break;
            case Testgrammatik_Text:
                GrammarTestText tokens_Text = new GrammarTestText();
                GrammarTestText.Grammar01Parser grammarParser_Text = tokens_Text.new Grammar01Parser();
                try {
                    Parsing<GrammarTestText> p = new Parsing<GrammarTestText>(tokens_Text, word);
                    p.parse(grammarParser_Text.Grammar());
                } catch (Exception e) {
                    log(e);
                    return false;
                }
                break;
            default:
                return false;
        }

        return true;
    }

    private void log(Exception e) {
        File dir = new File("logs");
        dir.mkdir();
        File user = new File("logs/" + userName);
        user.mkdir();
        File grammar = new File("logs/" + userName + "/" + currentgrammar.toString());
        grammar.mkdir();
        String filename = "logs/" + userName + "/" + currentgrammar.toString() + "/uncoolexceptionlog-" + userName + "-" + new SimpleDateFormat("yyyyMMddHHmmss").format(startTime) + ".txt";
        File file = new File(filename);
        try {
            FileWriter writer = new FileWriter(file, true);
            writer.write(new SimpleDateFormat("dd.MM.yyyy-HH:mm:ss").format(System.currentTimeMillis()) + ":\n" + e.getMessage() + "\n\n");
            writer.flush();
            writer.close();
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        String message = e.getMessage();
        System.out.println(message.substring(message.lastIndexOf(">") + 1));
    }
}
