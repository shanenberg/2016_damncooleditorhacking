package prettyuncooleditor.controller;

import damncooleditorhackers.EditorGrammar;

import java.io.File;
import java.io.FileWriter;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by Nick on 28.09.2016.
 */
public class TextAreaBackupHelper {
    private String timestamp;

    private void start(String username, EditorGrammar currentgrammar) {
        File dir = new File("logs");
        dir.mkdir();
        File user = new File("logs/" + username);
        user.mkdir();
        File grammar = new File("logs/" + username + "/" + currentgrammar.toString());
        grammar.mkdir();
        String timeStamp = new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss").format(new Date());
        String[] tmp = timeStamp.split("\\.");
        timestamp = "";
        for (String str : tmp) this.timestamp += str;
    }

    public void backup(String text, String time, String username, EditorGrammar currentgrammar) {
        if (timestamp == null) start(username, currentgrammar);
        String filename = "logs/" + username + "/" + currentgrammar + "/texteditor-log-" + username + "-" + timestamp + ".txt";
        File file = new File(filename);
        try {
            FileWriter writer = new FileWriter(file, true);
            String limiter = "=======================================================================";
            writer.write(new SimpleDateFormat("dd.MM.yyyy-HH:mm:ss - ").format(System.currentTimeMillis()) + time + ":\n" + text + "\n" + limiter + "\n");
            writer.flush();
            writer.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
