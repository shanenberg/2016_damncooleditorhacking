package prettyuncooleditor.controller;

import javax.swing.*;
import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.DocumentFilter;

/**
 * Created by Nick on 05.10.2016.
 */
public class TextAreaDocumentFilter extends DocumentFilter {
    private JTextArea textArea;
    private boolean enterAllowed = true;
    private int oldCaret;
    private String old;

    public TextAreaDocumentFilter(JTextArea textArea) {
        this.textArea = textArea;
        old = "";
        oldCaret = 0;
    }


    public void insertString(DocumentFilter.FilterBypass fb, int offset, String string, AttributeSet attr) throws BadLocationException {
        System.out.println("Insert");
    }

    public void remove(DocumentFilter.FilterBypass fb, int offset, int length) throws BadLocationException {

        String text = textArea.getText().substring(offset, offset + length);
        System.out.println("Remove Text \"" + text + "\"");
        if (text.startsWith("\n")) {
            offset--;
            length++;
        } else if (text.startsWith("\u00b6")) {
            length++;
        }
        super.remove(fb, offset, length);
        String full = textArea.getText();
        full = full.replace("\u00b6", "");
        full = full.replace("\n", "\u00b6\n");
        textArea.setText(full);
        textArea.setCaretPosition(offset);
    }

    public void replace(DocumentFilter.FilterBypass fb, int offset, int length, String string, AttributeSet attrs) throws BadLocationException {
        System.out.println("Replace");
        if (enterAllowed) {
            enterAllowed = false;
            int caret = textArea.getCaretPosition();
            String text = textArea.getText();
            old = text;
            oldCaret = caret;
            //System.out.println("Old: " + old + "\n" +
            //        "Caret: " + caret+ "\n");
            text = text.substring(0, offset) + string + text.substring(offset + length, text.length());
            text = text.replace("\u00b6", "");
            text = text.replace("\n", "\u00b6\n");
            textArea.setText(text);
            //textArea.setCaretPosition((caret + length + 1 <= textArea.getText().length()) ? caret + length + 1 : caret);
            fixCaretOnEnter();
            enterAllowed = true;
        } else {
            super.replace(fb, offset, length, string, attrs);
            int temp2 = string.length() - old.length() + oldCaret;
            int temp = getCaret(old, string, oldCaret);
            temp = temp < 0 ? string.length() : temp;
            temp = temp > string.length() ? string.length() : temp;
            /*System.out.println(
                    "Alt: " + old + "["+ old.length() + "]\n" +
                    "Neu: " + string + "["+ string.length() + "]\n"+
                    "Pos: " + temp + "\n\n"
            );*/
            textArea.setCaretPosition(temp);
            //textArea.setCaretPosition((offset + length <= textArea.getText().length()) ? offset + length : offset);
        }
    }

    private void fixCaretOnEnter() {
        if (textArea.getCaretPosition() + 1 <= textArea.getText().length()) {
            int extraOffset = (textArea.getText().substring(textArea.getCaretPosition(), textArea.getCaretPosition() + 1).startsWith("\n")) ? 1 : 0;
            if (textArea.getCaretPosition() + extraOffset <= textArea.getText().length())
                textArea.setCaretPosition(textArea.getCaretPosition() + extraOffset);
        }
    }


    private int getCaret(String oldOne, String newOne, int oldCaret) {
        int kleinereLaenge = oldOne.length() > newOne.length() ? newOne.length() : oldOne.length();
        //Finde die Länge des geänderten Bereichs////////////////

        int ret = 0;
        boolean atEnd = true;
//        System.out.println("Old: \"" + oldOne + "\"\nNew: \"" + newOne + "\"\n");
        //if (!(oldOne.length() == newOne.length())) {
        //System.out.println("I'm in");T
        for (int i = 0; i < kleinereLaenge; i++) {
            //System.out.println("i ist: " + i + "[Vergleiche \'" + oldOne.charAt(i) + "\' mit \'" + newOne.charAt(i) + "\']");
                if (oldOne.charAt(i) == newOne.charAt(i)) {
                    ret = i + (newOne.length() - oldOne.length());
                } else {
                    atEnd = false;

                    if (i >= oldCaret) {
                        ret = oldCaret + 1;
                    } else {
                        ret = i + 1;
                        //ret = oldCaret+1;
                        //ret = i + (newOne.length() - oldOne.length());
                    }
                    if (newOne.charAt(i) == '\u00b6') {
                        System.out.println("JUHU");
                        ret -= 0;
                    }

                    break;
                }
                atEnd = true;
            }

            if (atEnd) {
                ret = newOne.length();
            }
        /*} else {
            ret = oldOne.length();
        }*/

        System.out.println("Ret: " + ret + "\n");
        //System.out.println("ErsteDiff: " + newOne.charAt(ret-1));


        return ret;
    }
}
