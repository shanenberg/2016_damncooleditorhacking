package prettyuncooleditor.view;

import damncooleditorhackers.EditorGrammar;
import damncooleditorhackers.controller.Logger;
import damncooleditorhackers.view.ExperimentView;
import damncooleditorhackers.view.listener.ExperimentListeners;
import damncooleditorhackers.view.view_elements.NameInputDialog;
import damncooleditorhackers.view.view_elements.OverlayPane;
import damncooleditorhackers.view.view_elements.TimingPanel;
import prettyuncooleditor.controller.TextAreaDocumentFilter;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import javax.swing.text.AbstractDocument;
import javax.swing.text.Document;
import java.awt.*;
import java.io.*;

/**
 * Created by Niklas on 23.09.2016.
 */
public class TextEditor implements ExperimentView {
    private JFrame frame;
    private EditorGrammar currentGrammar;
    private OverlayPane overlayPane;
    private TimingPanel timingPanel;
    private KeyboardFocusManager manager;
    private TextEditorListener textEditorListener;
    private JTextArea textArea;
    private String userName;
    private String targetInput;

    public void start(EditorGrammar currentGrammar) {
        this.currentGrammar = currentGrammar;
        setTargetInput();
        frame = new JFrame("TextEditor - " + currentGrammar.name());
        frame.setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);
        frame.setExtendedState(JFrame.MAXIMIZED_BOTH);
        frame.setMinimumSize(new Dimension(800, 600));
        overlayPane = new OverlayPane(this);
        textEditorListener = new TextEditorListener(this);
        manager = KeyboardFocusManager.getCurrentKeyboardFocusManager();
        manager.addKeyEventDispatcher(textEditorListener);
        JSplitPane contentPane;
        contentPane = getMainPanel();
        frame.setContentPane(contentPane);
        frame.setGlassPane(overlayPane);
        frame.addWindowListener(textEditorListener.getCloseListener(frame));
        showOverlayPane("Start", timingPanel.getDescriptionFor("Start"));
        requestUserName();
        frame.setVisible(true);
    }

    private void setTargetInput() {
        InputStream inputStream;
        switch (currentGrammar) {
            case Kassenzettel:
                inputStream = getClass().getResourceAsStream("/damncooleditorhackers/view/view_elements/res/target_kassenzettel.txt");
                break;
            case Website:
                inputStream = getClass().getResourceAsStream("/damncooleditorhackers/view/view_elements/res/target_website.txt");
                break;
            case Testgrammatik_DACH:
                inputStream = getClass().getResourceAsStream("/damncooleditorhackers/view/view_elements/res/target_test_DACH.txt");
                break;
            case Testgrammatik_Text:
                inputStream = getClass().getResourceAsStream("/damncooleditorhackers/view/view_elements/res/target_test_Text.txt");
                break;
            default:
                JOptionPane.showMessageDialog(frame, "Fehler, keine Grammatik angegeben!");
                return;
        }
        try {
            InputStreamReader fileInput = new InputStreamReader(inputStream, "UTF-8");
            BufferedReader reader = new BufferedReader(fileInput);
            String line;
            StringBuilder result = new StringBuilder();
            try {
                while ((line = reader.readLine()) != null) {
                    result.append(line).append("\n");
                }
                this.targetInput = result.toString().substring(0, result.lastIndexOf("\n"));
            } catch (IOException e) {
                e.printStackTrace();
            }
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
    }

    private void requestUserName() {
        NameInputDialog nameInputDialog = new NameInputDialog(this);
        nameInputDialog.show();
        setUserName(nameInputDialog.getName());
    }

    private JSplitPane getMainPanel() {
        JSplitPane contentPanel = new JSplitPane(JSplitPane.VERTICAL_SPLIT);
        contentPanel.setDividerSize(0);
        timingPanel = new TextEditorTimingPanel(new FlowLayout(FlowLayout.RIGHT), new ExperimentListeners(), this);
        contentPanel.setTopComponent(timingPanel);
        JTabbedPane tabPane = new JTabbedPane(SwingConstants.TOP);
        tabPane.addChangeListener(textEditorListener.getTabChangedListener());
        tabPane.addTab("Editor", getEditorPanel());
        tabPane.addTab("Grammatik", getGrammarPanel());
        contentPanel.setBottomComponent(tabPane);
        return contentPanel;
    }

    private JPanel getEditorPanel() {
        final JPanel editorPanel = new JPanel();
        editorPanel.setLayout(new BorderLayout());
        editorPanel.setBorder(new EmptyBorder(4, 4, 4, 4));
        textArea = new JTextArea(25, 50);
        textArea.setFont(new Font(textArea.getFont().getName(), textArea.getFont().getStyle(), 18));
        Document textAreaDocument = textArea.getDocument();
        TextAreaDocumentFilter textAreaFilter = new TextAreaDocumentFilter(textArea);
        ((AbstractDocument) textAreaDocument).setDocumentFilter(textAreaFilter);
        JScrollPane scrollPane = new JScrollPane(textArea, ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED, ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED);
        editorPanel.add(scrollPane);
        editorPanel.setAlignmentX(Component.LEFT_ALIGNMENT);
        return editorPanel;
    }

    private JPanel getGrammarPanel() {
        JPanel grammarPanel = new JPanel(new BorderLayout());
        grammarPanel.setBackground(Color.WHITE);
        JLabel grammarImage = new JLabel(new ImageIcon(getClass().getResource("res/grammar-" + currentGrammar.name().toLowerCase() + ".png")), SwingConstants.LEFT);
        grammarImage.setVerticalAlignment(JLabel.TOP);
        grammarPanel.add(grammarImage);
        grammarPanel.setAlignmentX(Component.LEFT_ALIGNMENT);
        grammarPanel.setAlignmentY(Component.TOP_ALIGNMENT);
        return grammarPanel;
    }

    public void checkUserInput() {
        overlayPane.setVisible(false);
        Logger.log("OVERLAY ausgeblendet", timingPanel.getTimerString(), userName, currentGrammar);
        frame.requestFocusInWindow();
        manager.addKeyEventDispatcher(textEditorListener);
    }


    public void showOverlayPane(String mode, String text) {
        if (mode.equals("Stop")) textEditorListener.endRequested();
        manager.removeKeyEventDispatcher(textEditorListener);
        overlayPane.setVisible(true);
        overlayPane.showMode(mode, text);
        overlayPane.requestFocusInWindow();
    }

    public void hideOverlayPane() {
        overlayPane.setVisible(false);
        frame.requestFocusInWindow();
        manager.addKeyEventDispatcher(textEditorListener);
        timingPanel.start();
    }

    public void restartTimer() {
        Logger.log("ENDE abgelehnt", timingPanel.getTimerString(), userName, currentGrammar);
        timingPanel.start();
    }

    public void end() {
        timingPanel.end();
        Logger.log("ENDE bestätigt", timingPanel.getTimerString(), userName, currentGrammar);
    }

    public void abort() {
        timingPanel.end();
        Logger.log("EDITOR geschlossen", timingPanel.getTimerString(), userName, currentGrammar);
    }

    public TimingPanel getTimingPanel() {
        return timingPanel;
    }

    public JFrame getFrame() {
        return frame;
    }

    public void setUserName(String name) {
        userName = name;
        frame.setTitle(frame.getTitle() + " - " + name);
    }

    public String getCurrentInput() {
        return textArea.getText() != null ? textArea.getText().replace("\u00b6\n", "\n") : "";
    }

    public void showInvalidDialog(String message, String title) {
        JOptionPane.showMessageDialog(frame, message, title, JOptionPane.ERROR_MESSAGE);
    }

    public EditorGrammar getCurrentGrammar() {
        return currentGrammar;
    }

    public String getUserName() {
        return userName;
    }

    public String getTarget() {
        return targetInput;
    }
}
