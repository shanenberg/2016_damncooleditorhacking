package prettyuncooleditor.view;

import damncooleditorhackers.controller.Logger;
import damncooleditorhackers.view.view_elements.TimingDialog;

import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import java.awt.*;
import java.awt.event.KeyEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;

/**
 * Created by Niklas on 23.09.2016.
 */
public class TextEditorListener implements KeyEventDispatcher {
    private final TextEditor textEditor;
    private boolean endRequested;

    public TextEditorListener(TextEditor textEditor) {
        this.textEditor = textEditor;
    }

    public boolean dispatchKeyEvent(KeyEvent e) {
        if (!e.isConsumed() && e.getID() == KeyEvent.KEY_PRESSED) {
            if (endRequested && e.isControlDown() && e.isAltDown() && e.getKeyCode() == KeyEvent.VK_P) {
                textEditor.showOverlayPane("Stop", "Vielen Dank für ihre Teilnahme! Das Experiment ist nun beendet.");
                textEditor.end();
            } else if (endRequested && e.isControlDown() && e.isAltDown() && e.getKeyCode() == KeyEvent.VK_O) {
                textEditor.restartTimer();
                endRequested = false;
            } else if (e.isControlDown() && e.isAltDown() && e.getKeyCode() == KeyEvent.VK_T) {
                textEditor.getTimingPanel().pause();
                TimingDialog timingDialog = new TimingDialog(textEditor);
                timingDialog.show();
                textEditor.getTimingPanel().setTimerStartTime(timingDialog.getNewStartTime());
            }
        }
        return false;
    }

    public ChangeListener getTabChangedListener() {
        return new ChangeListener() {
            public void stateChanged(ChangeEvent e) {
                if (textEditor.getUserName() != null) {
                    JTabbedPane tabbedPane = (JTabbedPane) e.getSource();
                    Logger.log("WECHSEL zu " + tabbedPane.getTitleAt(tabbedPane.getSelectedIndex()) + "-Tab", textEditor.getTimingPanel().getTimerString(), textEditor.getUserName(), textEditor.getCurrentGrammar());
                }
            }
        };
    }

    public WindowListener getCloseListener(final JFrame frame) {
        return new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent windowEvent) {
                if (JOptionPane.showConfirmDialog(frame,
                        "Sind Sie sicher, dass Sie den Editor schließen möchten?", "Editor schließen?",
                        JOptionPane.YES_NO_OPTION,
                        JOptionPane.QUESTION_MESSAGE) == JOptionPane.YES_OPTION) {
                    textEditor.abort();
                    System.exit(0);
                }
            }
        };
    }

    public void endRequested() {
        endRequested = true;
    }
}
