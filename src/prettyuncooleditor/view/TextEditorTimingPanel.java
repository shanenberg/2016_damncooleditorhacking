package prettyuncooleditor.view;

import damncooleditorhackers.controller.Logger;
import damncooleditorhackers.view.ExperimentView;
import damncooleditorhackers.view.listener.ExperimentListeners;
import damncooleditorhackers.view.view_elements.TimingPanel;
import prettyuncooleditor.controller.ExtendedStopWatch;
import prettyuncooleditor.controller.GrammarChecker;

import java.awt.*;

/**
 * Created by Mark-Desktop on 29.09.2016.
 */
public class TextEditorTimingPanel extends TimingPanel {

    public TextEditorTimingPanel(FlowLayout flowLayout, ExperimentListeners listeners, ExperimentView view) {
        super(flowLayout, listeners, view);
        stopWatch = new ExtendedStopWatch((this));
        setFocusTraversalKeysEnabled(false);
    }

    @Override
    public void start() {
        ((ExtendedStopWatch) stopWatch).setUsername(view.getUserName());
        super.start();
    }

    @Override
    public void checkInput(String input) {
        if (input.equals("") || !new GrammarChecker(view.getUserName()).check(view.getCurrentGrammar(), view.getCurrentInput())) {
            Logger.log("EINGABE WAR UNGÜLTIG", timerLabel.getText(), view.getUserName(), view.getCurrentGrammar());
            view.showInvalidDialog("Das von Ihnen angegebene Dokument ist kein gültiger Satz der Grammatik!", "Wort kann nicht geparst werden");
        } else if (!input.equals(view.getTarget())) {
            Logger.log("EINGABE IST INKORREKT", timerLabel.getText(), view.getUserName(), view.getCurrentGrammar());
            System.out.println(view.getTarget() + "\n\n\n" + input);

            view.showInvalidDialog("Das von Ihnen angegebene Dokument entspricht zwar der Grammatik, ist aber nicht das Wort, welches unseren Vorgaben entspricht.\nBitte schauen Sie sich die Aufgabenstellung nochmal an.", "Wort entspricht nicht den Vorgaben");
        } else {
            Logger.log("EINGABE WAR GÜLTIG", timerLabel.getText(), view.getUserName(), view.getCurrentGrammar());
            view.showOverlayPane("Stop", "Ihre Eingabe ist korrekt! Vielen Dank für ihre Teilnahme! Das Experiment ist nun beendet.");
            view.end();
        }
    }
}
