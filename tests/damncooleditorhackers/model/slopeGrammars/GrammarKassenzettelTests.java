package damncooleditorhackers.model.slopeGrammars;

import junit.framework.TestCase;
import slope.Parsing;

/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2015 Stefan Hanenberg (stefan.hanenberg@gmail.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this
 * software and associated documentation files (the "Software"), to deal in the Software
 * without restriction, including without limitation the rights to use, copy, modify,
 * merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:

 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.

 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
 * INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
 * PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
 * FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 **/

public class GrammarKassenzettelTests extends TestCase {

	public void test01() {

		String myGrammar = "Ihr Supermarkt\n" +
				"45257 \"Essen\"\n" +
				"\"Universitätsstraße 2\"\n" +
				"Tel.: 0123012345\n" +
				"\n" +
				"\n" +
				"\"Brot\"   0,99 (Erm)\n" +
				"\"Smartphone\"   499,00 (Voll)\n" +
				"R \"Altes Smartphone\"  -199,00 (Voll)\n" +
				"\"Marmelade\"   1,99 (Erm)\n" +
				"\n" +
				"EC-Karte EUR =sum(Preis)\n" +
				"Steuerart    MwSt    ohne MwSt    mit MwSt\n" +
				"Voll 19,0%     57,00     243,00     =sum(sumMwSt + sumOhneMwSt)\n" +
				"Erm 7,0%     0,21     2,77     =sum(sumMwSt + sumOhneMwSt)\n" +
				"\n" +
				"\n" +
				"01.02.2013\n" +
				"Uhrzeit: 15:01\n" +
				"USt-IdNr.: DE 01234512345\n" +
				"\"Danke für Ihren Einkauf\"";
		;
//System.out.println(myGrammar.substring(0, 73));
		GrammarKassenzettel grammarKassenZettel = new GrammarKassenzettel();
		Parsing p = new Parsing(grammarKassenZettel, myGrammar);
		p.parse(grammarKassenZettel.new Grammar01Parser().Grammar());

	}
}
/*
				"Ihr Supermarkt\n"
        		+ "45701 Herten\n"
        		+ "Birkenstr.16a\n"
        		+ "Tel.: 018054646\n\n"
        		+ "\n"
        		+ " Schokolade   1,99Q\n"
				+ " AK47   0,99V\n\n"
        		+ "Bar EUR =sum(Preis)\n" +
				"Steuerart    MwSt    ohne MwST    mit MwST" +
				"\nG 19,0%     0,99     0,18     =sum(sumMwST + sumOhneMwST)" +
				"\nZ 7,0%     1,99     0,14     =sum(sumMwST + sumOhneMwST)\n" +
				"\n" +
				"\n" +
				"24.09.2016\n" +
				"Uhrzeit: 15:51\n" +
				"USt-IdNr.: DE 0000000000000\n" +
				"TRUMPFTW"
 */