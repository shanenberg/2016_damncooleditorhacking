package damncooleditorhackers.model.slopeGrammars;

import org.junit.Test;
import slope.Parsing;

/**
 * Created by Niklas on 01.10.2016.
 */
public class GrammarTestDACHTest {

    @Test
    public void test01() {
        String myGrammar = "Viel Erfolg beim Testen\n" +
                "\"Test\"\n" +
                "\n" +
                "A\n" +
                "\n" +
                "getrennte\n" +
                "Zeile\n" +
                "\n" +
                "tatütatatatütatatatütata\n" +
                "\n" +
                "Lass mich bitte da\n" +
                "\n" +
                "1234    12345012345\n" +
                "\n" +
                "Dies ist die 5. Zeile\n" +
                "Dies ist die 4. Zeile\n" +
                "Dies ist die 3. Zeile\n" +
                "Dies ist die 2. Zeile\n" +
                "\n" +
                "\n" +
                "34";

        GrammarTestDACH grammarTestDACH = new GrammarTestDACH();
        Parsing p = new Parsing(grammarTestDACH, myGrammar);
        p.parse(grammarTestDACH.new Grammar01Parser().Grammar());
    }

}