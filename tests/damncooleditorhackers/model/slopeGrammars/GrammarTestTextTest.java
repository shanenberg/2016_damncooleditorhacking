package damncooleditorhackers.model.slopeGrammars;

import org.junit.Test;
import slope.Parsing;

/**
 * Created by Niklas on 01.10.2016.
 */
public class GrammarTestTextTest {

    @Test
    public void test01() {
        String myGrammar = "\"Test\"\n" +
                "BC\n" +
                "AC\n" +
                "Getrennter\n" +
                "Satz\n" +
                "tatütatatatütatatatütata\n" +
                "Lass mich da";

        GrammarTestText grammarTestText = new GrammarTestText();
        Parsing p = new Parsing(grammarTestText, myGrammar);
        p.parse(grammarTestText.new Grammar01Parser().Grammar());
    }

}