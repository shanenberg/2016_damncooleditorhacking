package damncooleditorhackers.model.slopeGrammars;

import junit.framework.TestCase;
import slope.Parsing;


/**
 * Created by Nick on 27.09.2016.
 */
public class GrammarWebsiteTest extends TestCase {

    public void test01() {

        String myGrammar = "Persönliche Website von\n" +
                "89123 \"The Nick\"\n" +
                "\"Niklas H.\"\n" +
                "Tel.: 304120431231233\n" +
                "\n" +
                "\n" +
                "D \"WorldChampionship\"   0,10 (3T)\n" +
                "\"EuropeanChampionship\"   20,00 (5T)\n" +
                "\n" +
                "gesamt EUR =sum(Preisgeld)\n" +
                "Teamart    Teamanteil    Sponsorengeld    Gesamt\n" +
                "5er-Teams     20,00     5,00     =sum(TeamAnteile + Sponsorengeld)\n" +
                "3er-Teams     0,10     0,90     =sum(TeamAnteile + Sponsorengeld)\n" +
                "\n" +
                "\n" +
                "27.09.2016\n" +
                "Uhrzeit: 20:01\n" +
                "eSp-IdNr.: DE 91028472385123\n" +
                "\"Hooray\"";
        ;

        GrammarWebsite grammarWebsite = new GrammarWebsite();
        Parsing p = new Parsing(grammarWebsite, myGrammar);
        p.parse(grammarWebsite.new Grammar01Parser().Grammar());

    }

}