package model.elements;

import damncooleditorhackers.model.grammarElements.ANDElement;
import damncooleditorhackers.model.grammarElements.Op;
import damncooleditorhackers.model.grammarElements.TokenElement;
import junit.framework.TestCase;
import slope.PC;
import slope.ParserConstructor_N;
import slope.lang.TOKEN.TokenParserConstructor;
import slowscane.scanners.SingleCharScanner;

/**
 * Created by Nick on 15.06.2016.
 */
public class ANDElementTest extends TestCase {

    public void testTraverseChildren() throws Exception {
        TokenParserConstructor<Character> characterTokenParserConstructor;
        characterTokenParserConstructor = new TokenParserConstructor<Character>(new SingleCharScanner('\"'));
        ANDElement and = new ANDElement(new ParserConstructor_N("AND_PARSER", TokenParserConstructor.class, new PC[]{characterTokenParserConstructor}), Op.AND, null);

        assertTrue(TokenElement.class.isInstance(and.getChildren().get(0)));
    }

}