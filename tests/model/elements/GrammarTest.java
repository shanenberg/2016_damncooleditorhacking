package model.elements;

import junit.framework.TestCase;
import slope.Parsing;
import slope.SetCmd;
import slope.lib.grammar.GrammarParsing;
import slope.lib.grammar.parsetree.Grammar;

/**
 * Created by Nick on 22.06.2016.
 */
public class GrammarTest extends TestCase {

    public void testGrammar() {
        String myGrammar = "EinHaus\n" +
                "Start: Start\n" +
                "Rules:\n" +
                "Start -> \"Außenwand\" \"Außenwand\" \"Außenwand\" \"Außenwand\"  \"Tür\" Dach (\"Innenwand\" \"Fenster\" Garten)*.\n" +
                "Dach -> \"Dachziegel\"+ \"Dachfenster\"* \"Schornsstein\"?.\n" +
                "Garten -> (Blume | Kräuter)*.";

        GrammarParsing tokens = new GrammarParsing();
        SetCmd<Grammar> grammarCmd = new SetCmd<Grammar>() {
            public void set(Grammar value) {
            }
        };
        Parsing p = new Parsing(tokens, myGrammar);
        p.parse(tokens.new Grammar01Parser().Grammar(grammarCmd));
    }
}
